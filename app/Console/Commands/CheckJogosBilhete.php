<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\DetalheBilheteModel;
use App\Models\ResultadoJogosModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use App\Console\Commands\ArrayObject;
use App\Models\BilheteModel;

class CheckJogosBilhete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:jogosBilhete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar todos os jogos cadastrados no detalhe bilhete para que possa ser consultado o resultado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    private $api_url = 'https://api.b365api.com/';
    private $token = '84127-4nhc241d2Wr7wK';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->atualizarResultadoJogos();
        $this->verificarDetalheBilhete();
        $this->validarBilhete();
    }
    
    private function atualizarResultadoJogos() {

        $array['data_atual'] = date( 'Y-m-d H:i:s' , strtotime( now() ) );
        $resultadoJogos = ResultadoJogosModel::store($array);
        $duracao = '02:00:00';
        
        
        foreach($resultadoJogos as $rj) {
            
             $v = explode(':', $duracao);
            if($rj->jogo_atualizado == 0) {
                
                // caso o jogo já tenha terminado, buscar o resultado do jogo
                if( strtotime(now() ) > strtotime("{$rj->data_hora_jogo} + {$v[0]} hours {$v[1]} minutes {$v[2]} seconds") ){
                    
                    $resultConsulta =  json_decode (Http::get($this->api_url . 'v1/event/view?', [
                                'token' => $this->token,
                                'event_id'=> $rj->id_jogo_bet_api
                            ] ) ) ;

                    //dd( $resultConsulta );
                    if ( isset ( $resultConsulta->results ) ) {

                        foreach( $resultConsulta->results as $rc  ) {
                            $array = [];
                            // jogo terminado
                            if ( $rc->time_status == 3 ) {
                                
                                if(isset ( $rc->scores ) ) {
                                    foreach ( $rc->scores as $key => $s ) {
    
                                        if( $key == 1) {
                                            $array['id']                  = $rj->id;
                                            $array['id_jogo_bet_api']     = $rj->id_jogo_bet_api;
                                            $array['gols_casa_1_tempo']   = $s->home;
                                            $array['gols_fora_1_tempo']   = $s->away;
                                        }
    
                                        if( $key == 2) {
                                            $array['gols_casa_2_tempo']   = $s->home;
                                            $array['gols_fora_2_tempo']   = $s->away;
                                        }
                                    }
                                }
    
                                if(isset ( $rc->stats ) ) {
                                    foreach ( $rc->stats as $key => $s ) {
                                        if( $key == 'corners') {
                                            $array['escanteios'] = $s[0] + $s[1];
                                        }
                                    }
                                }
    
                                // gravar os dados pesquisados
                                if(isset ( $rc->scores ) ) {
                                    if( isset ( $array['id'] ) &&
                                        isset ( $array['id_jogo_bet_api']   ) &&
                                        isset ( $array['gols_casa_1_tempo'] ) &&
                                        isset ( $array['gols_fora_1_tempo'] ) &&
                                        isset ( $array['gols_casa_2_tempo'] ) &&
                                        isset ( $array['gols_fora_2_tempo'] )   ) {
    
                                        
                                        if( ( $array['gols_casa_2_tempo'] - $array['gols_casa_1_tempo'] ) > 0 ) {
                                            $array['gols_casa_2_tempo'] = $array['gols_casa_2_tempo'] - $array['gols_casa_1_tempo'];
                                        }else{
                                            $array['gols_casa_2_tempo'] = 0;
                                        }
                                        
                                        if( ( $array['gols_fora_2_tempo'] - $array['gols_fora_1_tempo'] ) > 0 ) {
                                            $array['gols_fora_2_tempo'] = $array['gols_fora_2_tempo'] - $array['gols_fora_1_tempo'];
                                        }else{
                                            $array['gols_fora_2_tempo'] = 0;
                                        }
    
                                        ResultadoJogosModel::getUpdate($array);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
        }
        
    }

    private function verificarDetalheBilhete() {
        
        $detalheBilhete =  DetalheBilheteModel::store();

        foreach( $detalheBilhete as $db) {

            if($db->resultado_jogo == null) {

                $array = [
                    'time_casa'      => $db->time_casa_bilhete_detalhe,
                    'time_fora'      => $db->time_visitante_bilhete_detalhe,
                    'data_hora_jogo' => $db->data_hora_jogo_bilhete_detalhe,
                ];

                $resultadoJogos = ResultadoJogosModel::store($array);

                if ( sizeof ( $resultadoJogos ) > 0 ) {

                    if( $resultadoJogos[0]->jogo_atualizado == true ){
                        $array_insert['id_detalhe_bilhete'] = $db->id_detalhe_bilhete;
                        $array_insert['resultado_jogo'] = $this->validarResultadosJogosSP( $resultadoJogos[0] , strtolower ( $db->descricao_odd_bilhete_detalhe ) );

                        DetalheBilheteModel::getUpdate($array_insert);
                    }

                }
            }
        }
    }

    private function validarBilhete() {
    
        $bilheteModel = BilheteModel::store();

        foreach ( $bilheteModel as $bm) {

            if($bm->bilhete_premiado == null) {
                
                $validarBilhete     = false;
                $recalcularBilhete  = false;
                $faltandoJogos      = false;

                foreach ( DetalheBilheteModel::store( $bm->id_bilhete ) as $dbm) {
                
                    // jogo perdido
                    if( isset($dbm->resultado_jogo) && $dbm->resultado_jogo == 0 ) {
                        $validarBilhete = true;
                        break;
                        // recalcular o valor do blihete
                    }else if( $dbm->resultado_jogo == 2 || $dbm->resultado_jogo == 3 ) {
                        $recalcularBilhete = true;
                    }else if($dbm->resultado_jogo == null) {
                        $faltandoJogos = true;
                    }
                    
                }

                // caso tenha perdido algum jogo "marca como perdedor"
                if( $validarBilhete == true ) {
                    $array['id_bilhete'] = $bm->id_bilhete;
                    $array['bilhete_premiado'] = false;
                    
                    BilheteModel::getUpdate($array);
                    
                }else if( $faltandoJogos ) {
                    
                }else if( $recalcularBilhete ) {

                    // recalcula odds
                    $valorOddAtualizada = 1;
                    foreach ( DetalheBilheteModel::store( $bm->id_bilhete ) as $dbm) {
    
                        if( $dbm->resultado_jogo == 1  ) {
                            $valorOddAtualizada = $valorOddAtualizada * $dbm->taxa_odd_bilhete_detalhe;
                        }
                    }
    
                    // altera valor da odd 
                    $array['id_bilhete']                    = $bm->id_bilhete;
                    $array['valor_cotacao_aposta_bilhete']  = $this->maskMoney( $valorOddAtualizada );
                    $array['valor_premio_aposta_bilhete']   = $this->maskMoney( floatval( $bm->valor_aposta_bilhete ) * $valorOddAtualizada );
                    $array['bilhete_premiado'] = true;
    
                    BilheteModel::getUpdate($array);
                    

                } else {

                    $array['id_bilhete'] = $bm->id_bilhete;
                    $array['bilhete_premiado'] = true;
                    
                    BilheteModel::getUpdate($array);
                }
            }
        }
    }

    function validarResultadosJogosSP( $resultadoJogos , $TipoOddBilhete ) {
       
        $PTCasa         = $resultadoJogos->gols_casa_1_tempo;
        $PTFora         = $resultadoJogos->gols_fora_1_tempo;
        $STCasa         = $resultadoJogos->gols_casa_2_tempo;
        $STFora         = $resultadoJogos->gols_fora_2_tempo;

        $TotalGolsCasa  = $PTCasa + $STCasa;
        $TotalGolsFora  = $PTFora + $STFora;

        $escanteio  = $resultadoJogos->escanteios;

        switch($TipoOddBilhete) {

            // ** VENCEDOR DO ENCONTRO ***

            case 'casa':
                if( $TotalGolsCasa > $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'empate':
                if( $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break;
                
            case 'fora':
                if( $TotalGolsCasa < $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break;
                
            
            // *** DUPLA CHANCE ***

            case '1-2':
                if( $TotalGolsCasa != $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'casa ou fora':
                if( $TotalGolsCasa != $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break; 

            case '1x':
                if( $TotalGolsCasa > $TotalGolsFora || $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'casa ou empate':
                if( $TotalGolsCasa > $TotalGolsFora || $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'x2':
                if( $TotalGolsCasa < $TotalGolsFora || $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora ou empate':
                if( $TotalGolsCasa < $TotalGolsFora || $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else    
                    return '0';
                break; 
                
            //  *** TOTAL DE GOLS EM CASA ***

            case 'casa - acima 1.5':
                if( $TotalGolsCasa > 1.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'casa - acima 2.5':
                if( $TotalGolsCasa > 2.5 ) 
                    return '1';
                else    
                    return '0';
                break; 
            
            case 'casa - acima 3.5':
                if( $TotalGolsCasa > 3.5 ) 
                    return '1';
                else    
                    return '0';
                break; 
            
            case 'casa - acima 4.5':
                if( $TotalGolsCasa > 4.5 ) 
                    return '1';
                else    
                    return '0';
                break; 
            
            case 'casa - abaixo 1.5':
                if( $TotalGolsCasa < 1.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'casa - abaixo 2.5':
                if( $TotalGolsCasa < 2.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'casa - abaixo 3.5':
                if( $TotalGolsCasa < 3.5 ) 
                    return '1';
                else    
                    return '0';
                break;
            
            case 'casa - abaixo 4.5':
                if( $TotalGolsCasa < 4.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'casa - abaixo 5.5':
                if( $TotalGolsCasa < 5.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            // *** TOTAL DE GOLS DO VISITANTE ***

            case 'fora - acima 1.5':
                if( $TotalGolsFora > 1.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 2.5':
                if( $TotalGolsFora > 2.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 3.5':
                if( $TotalGolsFora > 3.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 4.5':
                if( $TotalGolsFora > 4.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 5.5':
                if( $TotalGolsFora > 5.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - abaixo 1.5':
                if( $TotalGolsFora < 1.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 2.5':
                if( $TotalGolsFora < 2.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 3.5':
                if( $TotalGolsFora < 3.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 4.5':
                if( $TotalGolsFora < 4.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'fora - acima 5.5':
                if( $TotalGolsFora < 5.5 ) 
                    return '1';
                else    
                    return '0';
                break; 
            
            // *** TOTAL DE GOLS NO JOGO ***

            case 'jogo - acima 1.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 1.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - acima 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 2.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case '+2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 2.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - acima 3.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 3.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - acima 4.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 4.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - acima 5.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 5.5 ) 
                    return '1';
                else    
                    return '0';
                break;
                
            case 'jogo - acima 6.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 6.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'jogo - abaixo 0.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 0.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - abaixo 1.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 1.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - abaixo 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 2.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case '-2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 2.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'jogo - abaixo 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 3.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case 'jogo - abaixo 3.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 3.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - abaixo 4.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 4.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'jogo - abaixo 5.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 5.5 ) 
                    return '1';
                else    
                    return '0';
                break; 

            // *** AMBAS EQUIPES *** 

            case 'ambas marcam : sim':
                if( $TotalGolsCasa > 0 && $TotalGolsFora > 0) 
                    return '1';
                else    
                    return '0';
                break; 

            case 'ambas marcam : não':
                if( $TotalGolsCasa == 0 || $TotalGolsFora == 0) 
                    return '1';
                else    
                    return '0';
                break; 

                // *** VENCEDOR DO ENCONTRO no 1º tempo ou 2º tempo ***

            case '1º tempo - casa':
                if( $PTCasa > $PTFora ) 
                    return '1';
                else    
                    return '0';
                break; 

            case '1º tempo - empate':
                if( $PTCasa == $PTFora ) 
                    return '1';
                else    
                    return '0';
                break;
                
            case '1º tempo - fora':
                if( $PTCasa < $PTFora ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - casa':
                if( $STCasa > $STFora ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - empate':
                if( $STCasa == $STFora ) 
                    return '1';
                else    
                    return '0';
                    break;

            case '2º tempo - fora':
                if( $STCasa < $STFora ) 
                    return '1';
                else    
                    return '0';
                break;
                
            // *** TOTAL DE GOLS NO PRIMEIRO TEMPO ***
                    
            case '1º tempo - jogo acima 0.5':
                if( ($PTCasa + $PTFora) > 0.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo acima 1.5':
                if( ($PTCasa + $PTFora) > 1.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo acima 2.5':
                if( ($PTCasa + $PTFora) > 2.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo acima 3.5':
                if( ($PTCasa + $PTFora) > 3.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo acima 4.5':
                if( ($PTCasa + $PTFora) > 4.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo acima 5.5':
                if( ($PTCasa + $PTFora) > 5.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo acima 6.5':
                if( ($PTCasa + $PTFora) > 6.5 ) 
                    return '1';
                else    
                    return '0';
                break;
            
            case '1º tempo - jogo abaixo 0.5':
                if( ($PTCasa + $PTFora) < 0.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo abaixo 1.5':
                if( ($PTCasa + $PTFora) < 1.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo abaixo 2.5':
                if( ($PTCasa + $PTFora) < 2.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo abaixo 3.5':
                if( ($PTCasa + $PTFora) < 3.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo abaixo 4.5':
                if( ($PTCasa + $PTFora) < 4.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo abaixo 5.5':
                if( ($PTCasa + $PTFora) < 5.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - jogo abaixo 6.5':
                if( ($PTCasa + $PTFora) < 6.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            // *** TOTAL DE JOGOS NO SEGUNDO TEMPO ***

            case '2º tempo - jogo acima 0.5':
                if( ($STCasa + $STFora) > 0.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo acima 1.5':
                if( ($STCasa + $STFora) > 1.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo acima 2.5':
                if( ($STCasa + $STFora) > 2.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo acima 3.5':
                if( ($STCasa + $STFora) > 3.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo acima 4.5':
                if( ($STCasa + $STFora) > 4.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo acima 5.5':
                if( ($STCasa + $STFora) > 5.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo abaixo 0.5':
                if( ($STCasa + $STFora) > 0.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo abaixo 1.5':
                if( ($STCasa + $STFora) < 1.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo abaixo 2.5':
                if( ($STCasa + $STFora) < 2.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo abaixo 3.5':
                if( ($STCasa + $STFora) < 3.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo abaixo 4.5':
                if( ($STCasa + $STFora) < 4.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - jogo abaixo 5.5':
                if( ($STCasa + $STFora) < 5.5 ) 
                    return '1';
                else    
                    return '0';
                break;

            // *** TOTAL DE GOLS PAR OU IMPAR ***

            case 'total de gols ímpar':
                if( ( $TotalGolsCasa + $TotalGolsFora ) % 2 == 1)
                    return '1';
                else    
                    return '0';
                break;

            case 'total de gols par':
                if( ( $TotalGolsCasa + $TotalGolsFora ) % 2 == 0)
                    return '1';
                else    
                    return '0';
                break;

            // *** TOTAL DE GOLS PAR OU IMPAR 1º TEMPO ***

            case '1º tempo - total de gols ímpar':
                if( ( $PTCasa + $PTFora ) % 2 == 1)
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - total de gols par':
                if( ( $PTCasa + $PTFora ) % 2 == 0)
                    return '1';
                else    
                    return '0';
                break;

            // *** TOTAL DE GOLS PAR OU IMPAR 2º TEMPO ***

            case '2º tempo - total de gols ímpar':
                if( ( $STCasa + $STFora ) % 2 == 1)
                    return '1';
                else    
                    return '0';
                break;

            case '2º tempo - total de gols par':
                if( ( $STCasa + $STFora ) % 2 == 0)
                    return '1';
                else    
                    return '0';
                break;
            
            // *** DUPLA CHANCE 1º TEMPO ***

            case '1º tempo - casa ou fora':
                if( $PTCasa != $PTFora )
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - casa ou empate':
                if( $PTCasa > $PTFora || $PTCasa == $PTFora )
                    return '1';
                else    
                    return '0';
                break;

            case '1º tempo - fora ou empate':
                if( $PTCasa < $PTFora || $PTCasa == $PTFora )
                    return '1';
                else    
                    return '0';
                break;

            // *** INTERVALO / FINAL DE JOGO ***

            case 'intervalo | final (casa | casa)':
                if( $PTCasa > $PTFora && $STCasa > $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (casa | empate)':
                if( $PTCasa > $PTFora && $STCasa == $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (casa | fora)':
                if( $PTCasa > $PTFora && $STCasa < $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (empate | casa)':
                if( $PTCasa == $PTFora && $STCasa > $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (empate | empate)':
                if( $PTCasa == $PTFora && $STCasa == $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (empate | fora)':
                if( $PTCasa == $PTFora && $STCasa < $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (fora | casa)':
                if( $PTCasa < $PTFora && $STCasa > $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (fora | empate)':
                if( $PTCasa < $PTFora && $STCasa == $STFora )
                    return '1';
                else    
                    return '0';
                break;

            case 'intervalo | final (fora | fora)':
                if( $PTCasa < $PTFora && $STCasa < $STFora )
                    return '1';
                else    
                    return '0';
                break;

            // *** EMPATE ANULA A APOSTA ***
                
            case 'empate não tem aposta - casa':
                if( $TotalGolsCasa > $TotalGolsFora )
                    return '1';
                else if( $TotalGolsCasa == $TotalGolsFora )    
                    return 2;
                else 
                    return '0';
                break;

            case 'empate não tem aposta - fora':
                if( $TotalGolsCasa < $TotalGolsFora )
                    return '1';
                else if( $TotalGolsCasa == $TotalGolsFora )    
                    return 2;
                else 
                    return '0';
                break;

            // *** AMBAS AS EQUIPES 1º TEMPO ***
            
            case '1º tempo - ambas marcam - sim':
                if( $PTCasa > 0 && $PTFora > 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - ambas marcam - não':
                if( $PTCasa == 0 || $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            // *** AMBAS AS EQUIPES 2º TEMPO ***
            
            case '2º tempo - ambas marcam - sim':
                if( $STCasa > 0 && $STFora > 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '2º tempo - ambas marcam - não':
                if( $STCasa == 0 || $STFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            // *** HANDICAP DE GOL

            case 'handicap casa (-1)':
                if( ( $TotalGolsCasa - 1 ) > $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap casa (-2)':
                if( ( $TotalGolsCasa - 2 ) > $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap casa (-3)':
                if( ( $TotalGolsCasa - 3 ) > $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap casa (+1)':
                if( ( $TotalGolsCasa + 1 ) > $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;

            // case 'handicap empate (-1)':
            //     if( ( $TotalGolsCasa - 1 ) == $TotalGolsFora )
            //         return '1';
            //     else 
            //         return '0';
            //     break;

            // case 'handicap empate (-2)':
            //     if( ( $TotalGolsCasa - 2 ) == $TotalGolsFora )
            //         return '1';
            //     else 
            //         return '0';
            //     break;

            // case 'handicap empate (+1)':
            //     if( ( $TotalGolsCasa - 2 ) == $TotalGolsFora )
            //         return '1';
            //     else 
            //         return '0';
            //     break;

            // case 'handicap empate (+2)':
            //     if( ( $TotalGolsCasa - 2 ) == $TotalGolsFora )
            //         return '1';
            //     else 
            //         return '0';
            //     break;

            // case 'handicap empate (+3)':
            //     if( ( $TotalGolsCasa - 2 ) == $TotalGolsFora )
            //         return '1';
            //     else 
            //         return '0';
            //     break;

            case 'handicap fora (-1)':
                if( ( $TotalGolsFora - 1 ) > $TotalGolsCasa )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap fora (-2)':
                if( ( $TotalGolsFora - 2 ) > $TotalGolsCasa )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap fora (+1)':
                if( ( $TotalGolsFora + 1 ) > $TotalGolsCasa )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap fora (+2)':
                if( ( $TotalGolsFora + 2 ) > $TotalGolsCasa )
                    return '1';
                else 
                    return '0';
                break;

            case 'handicap fora (+3)':
                if( ( $TotalGolsFora + 3 ) > $TotalGolsCasa )
                    return '1';
                else 
                    return '0';
                break;

            // *** RESULTADO EXATO ***

            case 'resultado exato (0 : 0)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 1)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 2)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 3)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 4)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 5)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 6)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 7)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 8)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (0 : 9)':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 0)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 1)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 2)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 3)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 4)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 5)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;
                
            case 'resultado exato (1 : 6)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 7)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 8)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (1 : 9)':
                if( $TotalGolsCasa == 1 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 0)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 1)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 2)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 3)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 4)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 5)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 6)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 7)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (2 : 8)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;
    
            case 'resultado exato (2 : 9)':
                if( $TotalGolsCasa == 2 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 0)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 1)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 2)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 3)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 4)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 5)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 6)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 7)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 8)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (3 : 9)':
                if( $TotalGolsCasa == 3 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 0)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 1)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 2)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 3)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 4)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 5)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 6)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 7)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 8)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (4 : 9)':
                if( $TotalGolsCasa == 4 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 0)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 1)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 2)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 3)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 4)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 5)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 6)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 7)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 8)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (5 : 9)':
                if( $TotalGolsCasa == 5 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 0)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 1)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 2)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 3)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 4)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 5)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 6)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 7)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 8)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (6 : 9)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 0)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 1)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 2)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 3)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 4)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 5)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 6)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 7)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 8)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (7 : 9)':
                if( $TotalGolsCasa == 7 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 0)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 1)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 2)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 3)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 4)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 5)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 6)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 7)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 8)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (8 : 9)':
                if( $TotalGolsCasa == 8 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 0)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 1)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 2)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 3)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 4)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 5)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 6)':
                if( $TotalGolsCasa == 6 && $TotalGolsFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 7)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 8)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado exato (9 : 9)':
                if( $TotalGolsCasa == 9 && $TotalGolsFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            // *** RESULTADO EXATO 1º TEMPO ***

            case '1º tempo - resultado exato (0 : 0)':
                if( $PTCasa == 0 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 1)':
                if( $PTCasa == 0 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 2)':
                if( $PTCasa == 0 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 3)':
                if( $PTCasa == 0 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 4)':
                if( $PTCasa == 0 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 5)':
                if( $PTCasa == 0 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 6)':
                if( $PTCasa == 0 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 7)':
                if( $PTCasa == 0 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 8)':
                if( $PTCasa == 0 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (0 : 9)':
                if( $PTCasa == 0 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 0)':
                if( $PTCasa == 1 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 1)':
                if( $PTCasa == 1 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 2)':
                if( $PTCasa == 1 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 3)':
                if( $PTCasa == 1 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 4)':
                if( $PTCasa == 1 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 5)':
                if( $PTCasa == 1 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;
                
            case '1º tempo - resultado exato (1 : 6)':
                if( $PTCasa == 1 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 7)':
                if( $PTCasa == 1 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 8)':
                if( $PTCasa == 1 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (1 : 9)':
                if( $PTCasa == 1 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 0)':
                if( $PTCasa == 2 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 1)':
                if( $PTCasa == 2 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 2)':
                if( $PTCasa == 2 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 3)':
                if( $PTCasa == 2 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 4)':
                if( $PTCasa == 2 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 5)':
                if( $PTCasa == 2 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 6)':
                if( $PTCasa == 2 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 7)':
                if( $PTCasa == 2 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (2 : 8)':
                if( $PTCasa == 2 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;
    
            case '1º tempo - resultado exato (2 : 9)':
                if( $PTCasa == 2 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 0)':
                if( $PTCasa == 3 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 1)':
                if( $PTCasa == 3 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 2)':
                if( $PTCasa == 3 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 3)':
                if( $PTCasa == 3 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 4)':
                if( $PTCasa == 3 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 5)':
                if( $PTCasa == 3 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 6)':
                if( $PTCasa == 3 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 7)':
                if( $PTCasa == 3 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 8)':
                if( $PTCasa == 3 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (3 : 9)':
                if( $PTCasa == 3 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 0)':
                if( $PTCasa == 4 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 1)':
                if( $PTCasa == 4 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 2)':
                if( $PTCasa == 4 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 3)':
                if( $PTCasa == 4 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 4)':
                if( $PTCasa == 4 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 5)':
                if( $PTCasa == 4 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 6)':
                if( $PTCasa == 4 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 7)':
                if( $PTCasa == 4 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 8)':
                if( $PTCasa == 4 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (4 : 9)':
                if( $PTCasa == 4 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 0)':
                if( $PTCasa == 5 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 1)':
                if( $PTCasa == 5 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 2)':
                if( $PTCasa == 5 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 3)':
                if( $PTCasa == 5 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 4)':
                if( $PTCasa == 5 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 5)':
                if( $PTCasa == 5 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 6)':
                if( $PTCasa == 5 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 7)':
                if( $PTCasa == 5 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 8)':
                if( $PTCasa == 5 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (5 : 9)':
                if( $PTCasa == 5 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 0)':
                if( $PTCasa == 6 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 1)':
                if( $PTCasa == 6 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 2)':
                if( $PTCasa == 6 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 3)':
                if( $PTCasa == 6 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 4)':
                if( $PTCasa == 6 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 5)':
                if( $PTCasa == 6 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 6)':
                if( $PTCasa == 6 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 7)':
                if( $PTCasa == 6 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 8)':
                if( $PTCasa == 6 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (6 : 9)':
                if( $PTCasa == 6 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 0)':
                if( $PTCasa == 7 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 1)':
                if( $PTCasa == 7 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 2)':
                if( $PTCasa == 7 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 3)':
                if( $PTCasa == 7 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 4)':
                if( $PTCasa == 7 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 5)':
                if( $PTCasa == 7 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 6)':
                if( $PTCasa == 6 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 7)':
                if( $PTCasa == 7 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 8)':
                if( $PTCasa == 7 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (7 : 9)':
                if( $PTCasa == 7 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 0)':
                if( $PTCasa == 8 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 1)':
                if( $PTCasa == 8 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 2)':
                if( $PTCasa == 8 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 3)':
                if( $PTCasa == 8 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 4)':
                if( $PTCasa == 8 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 5)':
                if( $PTCasa == 8 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 6)':
                if( $PTCasa == 6 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 7)':
                if( $PTCasa == 8 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 8)':
                if( $PTCasa == 8 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (8 : 9)':
                if( $PTCasa == 8 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 0)':
                if( $PTCasa == 9 && $PTFora == 0 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 1)':
                if( $PTCasa == 9 && $PTFora == 1 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 2)':
                if( $PTCasa == 9 && $PTFora == 2 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 3)':
                if( $PTCasa == 9 && $PTFora == 3 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 4)':
                if( $PTCasa == 9 && $PTFora == 4 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 5)':
                if( $PTCasa == 9 && $PTFora == 5 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 6)':
                if( $PTCasa == 6 && $PTFora == 6 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 7)':
                if( $PTCasa == 9 && $PTFora == 7 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 8)':
                if( $PTCasa == 9 && $PTFora == 8 )
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo - resultado exato (9 : 9)':
                if( $PTCasa == 9 && $PTFora == 9 )
                    return '1';
                else 
                    return '0';
                break;

             // Ambas equipes
             case 'ambas marcam: sim':
                if( $TotalGolsCasa > 0 && $TotalGolsFora > 0 )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambas marcam: nao':
                if( $TotalGolsCasa == 0 || $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambos m':
                if( $TotalGolsCasa > 0 && $TotalGolsFora > 0 )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambos m não':
                if( $TotalGolsCasa == 0 || $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;
             // Ambas com Resultado

            case 'ambas marcam: nao / casa':
                if( $TotalGolsCasa > 0 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambas marcam: sim / casa':
                if( $TotalGolsCasa > 0 && $TotalGolsFora > 0 && $TotalGolsCasa > $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambas marcam: nao / empate':
                if( $TotalGolsCasa == 0 && $TotalGolsFora == 0 )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambas marcam: sim / empate':
                if( $TotalGolsCasa > 0 && $TotalGolsFora > 0 && $TotalGolsCasa == $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;   

            case 'ambas marcam: nao / fora':
                if( $TotalGolsCasa == 0 && $TotalGolsCasa < $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;

            case 'ambas marcam: sim / fora':
                if( $TotalGolsCasa > 0 && $TotalGolsFora > 0 && $TotalGolsCasa < $TotalGolsFora )
                    return '1';
                else 
                    return '0';
                break;

            // ***TOTAL EXATO DE GOLS***

            case 'número de gols exato (9)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 9 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (8)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 8 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (7)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 7 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (6)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 6 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (5)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 5 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (4)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 4 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (3)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 3 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (2)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 2 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (1)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 1 ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'número de gols exato (0)':
                if( ( $TotalGolsCasa + $TotalGolsFora ) == 0 ) 
                    return '1';
                else 
                    return '0';
                break;

            // ***TOTAL EXATO DE GOLS 1º TEMPO ***
                    
            case '1º tempo número de gols exato (0)':
                if( ( $PTCasa + $PTFora ) == 0 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (1)':
                if( ( $PTCasa + $PTFora ) == 1 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (2)':
                if( ( $PTCasa + $PTFora ) == 2 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (3)':
                if( ( $PTCasa + $PTFora ) == 3 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (4)':
                if( ( $PTCasa + $PTFora ) == 4 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (5)':
                if( ( $PTCasa + $PTFora ) == 5 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (6)':
                if( ( $PTCasa + $PTFora ) == 6 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (7)':
                if( ( $PTCasa + $PTFora ) == 7 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (8)':
                if( ( $PTCasa + $PTFora ) == 8 ) 
                    return '1';
                else 
                    return '0';
                break;

            case '1º tempo número de gols exato (9)':
                if( ( $PTCasa + $PTFora ) == 9 ) 
                    return '1';
                else 
                    return '0';
                break;

            // **** Resultado / Total de Gols ****

            case 'resultado casa / total de gols acima de 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 2.5 &&  $TotalGolsCasa > $TotalGolsFora ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado casa / total de gols abaixo de 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 2.5 &&  $TotalGolsCasa > $TotalGolsFora ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado fora / total de gols acima de 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 2.5 &&  $TotalGolsCasa < $TotalGolsFora ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado fora / total de gols abaixo de 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 2.5 &&  $TotalGolsCasa < $TotalGolsFora ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado empate / total de gols acima de 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) > 2.5 &&  $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else 
                    return '0';
                break;

            case 'resultado empate / total de gols abaixo de 2.5':
                if( ( $TotalGolsCasa + $TotalGolsFora ) < 2.5 &&  $TotalGolsCasa == $TotalGolsFora ) 
                    return '1';
                else 
                    return '0';
                break;

            /********** cantos escanteios *******/

            case 'cantos exatamente (1)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (2)':
                if($escanteio == 2)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (3)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (4)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (5)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (6)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (7)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (8)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos exatamente (9)':
                if($escanteio == 1)
                    return '1';
                else 
                    return '0';
                break;

            /********** cantos escanteios acima *******/
            case 'cantos acima de (6)':
                if( $escanteio > 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos acima de (7)':
                if( $escanteio > 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos acima de (8)':
                if( $escanteio > 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos acima de (9)':
                if( $escanteio > 9 )
                    return '1';
                else 
                    return '0';
                break;

            /********** cantos escanteios abaixo *******/
            case 'cantos abaixo de (3)':
                if( $escanteio < 9 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos abaixo de (4)':
                if( $escanteio < 4 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos abaixo de (5)':
                if( $escanteio < 5 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos abaixo de (6)':
                if( $escanteio < 6 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos abaixo de (7)':
                if( $escanteio < 7 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos abaixo de (8)':
                if( $escanteio < 8 )
                    return '1';
                else 
                    return '0';
                break;

            case 'cantos abaixo de (9)':
                if( $escanteio < 9 )
                    return '1';
                else 
                    return '0';
                break;

                
                    // // *** HANDICAP ASIATICO

                    // case 380: // Handicap Asiático Casa (-0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 381: // Handicap Asiático Casa (-1.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 382: // Handicap Asiático Casa (-2.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 386: // Handicap Asiático Casa (+0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 416: // Handicap Asiático Fora (-0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 422: // Handicap Asiático Fora (+0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 423: // Handicap Asiático Fora (+1.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 424: // Handicap Asiático Fora (+2.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
            default:
                return null;
                break;
        }
    }


    // formartar valor dinheiro
    private function maskMoney($value){
        $val = explode("." , str_replace(",", ".", $value) );
        
        if(!isset($val[1]))
            return $val[0].',00';
        else if(strlen($val[1]) == 1)
            return $val[0].','.$val[1].'0';
        else if(strlen($val[1]) >= 2)
            return $val[0] . ',' . substr( $val[1], 0, 2 );
        else
            return $value;
        
    }
}
