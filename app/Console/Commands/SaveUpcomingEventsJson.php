<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use App\Models\ResultadoJogosModel;

class SaveUpcomingEventsJson extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save:getUpcomingEvents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Este comando fará uma consulta na API, trazendo todos os eventos de um dia para frente';

    private $api_url = 'https://api.b365api.com/';
    private $token = '84127-4nhc241d2Wr7wK';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->getData(); // consultar os jogos na API
        // $this->limparJogos(); // apagar jogos de uma semana atrás
        $this->carregarJogos(); // carregar os dados para a tabela
    }

    private function getData()
    {
        $dataAll = [];

        $date = date('Ymd', strtotime( '1 days' ) );
        $data = $this->getEvents( $date , null );

        $page       = $data['pager']['page'];
        $per_page   = $data['pager']['per_page'];
        $total      = $data['pager']['total'];
        $qtdPages  = ( $total / $per_page );

        echo $total;

        // se tiver apenas uma pagina
        if( $qtdPages < 1 ) {
            echo $qtdPages . '<br>';
            if ( isset ( $data['results'] ) ) {
                $dataAll = array_merge( $dataAll , $data['results'] );
            }

        } else {

            do {
                echo $page . '<br>';
                if($page == 1) {
                    if ( isset ( $data['results'] ) ) {
                        $dataAll = array_merge( $dataAll , $data['results'] );
                    }
                } else {
                    if ( isset ( $this->getEvents( $date , $page )['results'] ) ) {
                        $dataAll = array_merge( $dataAll , $this->getEvents( $date , $page )['results'] ) ;
                    }
                }

                $page++;

            } while ( $page  <= $qtdPages );
        }

        Storage::disk('public')->put('All_jogos.json', json_encode( $dataAll ) );
        
    }


    // faz a consulta na API seguindo os parametros informados
    private function getEvents($date = null , $page = null )
    {
        if($date == null) {
            $date = date('Ymd', strtotime(now()));
        } else {
            $date = date('Ymd', strtotime($date));
        }
        
        if( $page == null ) {
            return Http::get($this->api_url . '/v2/events/upcoming?', [
                'token' => $this->token,
                'sport_id'  => '1',
                'LNG_ID'    => 22,
                'skip_esports' => 'Esoccer',
                // 'day'       => $date,
            ]);

        } else {

            return Http::get($this->api_url . '/v2/events/upcoming?', [
                'token' => $this->token,
                'sport_id'  => '1',
                'LNG_ID'    => 22,
                'skip_esports' => 'Esoccer',
                // 'day'       => $date,
                'page'      => $page,
            ]);
        }
    }

    private function carregarJogos() 
    {
        $contents = Storage::disk('public')->get('All_jogos.json');
        
        foreach ( json_decode( $contents ) as $c ) {
            
            $arrayRJ = [
                'id_jogo_bet_api'   => $c->id,
                'data_hora_jogo'    => date('Y-m-d H:i:s ', $c->time ),
                'time_casa'         => $c->home->name,
                'time_fora'         => $c->away->name,
            ];

            ResultadoJogosModel::getInsert( $arrayRJ );
        }
    }

    private function limparJogos()
    {
        ResultadoJogosModel::getDelete();
    }
}
