<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\CheckJogosBilhete;
use App\Console\Commands\SaveUpcomingEventsJson;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // 
        CheckJogosBilhete::class,
        // SaveUpcomingEventsJson::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $schedule->command('inspire')->hourly();
        
        // executa a cada 30 minutos
        $schedule->command('check:jogosBilhete')->everyMinute();

        // executa as 23:50, todos os dias
        $schedule->command('check:save:getUpcomingEvents')->dailyAt('23:50');
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
