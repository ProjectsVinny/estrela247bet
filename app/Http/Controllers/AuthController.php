<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ConsultarSporteBetNet;

class AuthController extends Controller
{
    public function dashboard(Request $request)
    {
        $consulta = new ConsultarSporteBetNet;

        if(Auth::check() === true) {
            return view('admin.dashboard', [
                'data' => $consulta->getAdmin($request)
            ]);
        }
        return redirect('/');
    }

    public function getLogin(Request $request)
    {
        $credentials = [
            'login'     => $request->login,
            'password'  => $request->password,
            'conta_ativa' => true
        ];

        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            return redirect('admin');
        }

        return redirect()->back()->withInput()->withErrors(['Dados informados estão incorretos!']);
    }

    public function getLogout()
    {
        Auth::logout();
        
        return redirect('/');
    }
}
