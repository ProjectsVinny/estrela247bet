<?php

namespace App\Http\Controllers;
use App\Models\BilheteModel;
use App\Models\DetalheBilheteModel;
use App\Models\UsuarioModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class BilheteController extends Controller

{
    //
    public function get() 
    {
        if ( Auth::check() === true ) {

            if( Auth::user()->id_perfil === 1) {
                $listaBilhetes = BilheteModel::store();
                $usuarios = UsuarioModel::getStore();
            } else {
                $listaBilhetes = BilheteModel::store( null , null , Auth::user()->id );
                $usuarios = UsuarioModel::getStore(Auth::user()->id, true);
            }

            return view('admin.bilhetes',[
                'listaBilhetes' => $listaBilhetes,
                'usuarios' => $usuarios
            ]);
        }

        return redirect("/");
    }


    public function getfiltroBilhetes(Request $request)
    {
        if ( Auth::check() === true ) {

             if( Auth::user()->id_perfil === 1) {

                 $data_inicio       = $request->data_inicio != '' ? $request->data_inicio : null;
                 $dataFim           = $request->data_fim != '' ? $request->data_fim : null;
                 $codigoBilhete     = $request->codigo_bilhete != '' ? $request->codigo_bilhete : null ;
                 $id_usuario        = $request->usuario != '' ? $request->usuario : null;
                 
                 // consultar pela data
                 if ( $data_inicio != null && $dataFim != null ) {
                    
                    $array['data_inicio']   = $data_inicio ;
                    $array['data_fim']      = $dataFim;

                    if( $id_usuario != null ) {
                        $listaBilhetes = BilheteModel::store(null , null , $id_usuario , $array );
                        $array['usuario'] = UsuarioModel::getStore($id_usuario, true )[0]->login;
                    } else {
                        $listaBilhetes = BilheteModel::store(null , null , null , $array );
                    }
                    
                         return view('admin.bilhetes',[
                            'listaBilhetes' => $listaBilhetes,
                            'usuarios' => UsuarioModel::getStore(),
                            'periodo_relatório' => $array
                        ]);
                }else if( $id_usuario != null ) {

                    $listaBilhetes = BilheteModel::store(null , null , $id_usuario , null );
                    $array['usuario'] = UsuarioModel::getStore($id_usuario, true )[0]->login;

                    return view('admin.bilhetes',[
                        'listaBilhetes' => $listaBilhetes,
                        'usuarios' => UsuarioModel::getStore(),
                        'periodo_relatório' => $array
                    ]);

                }else if( $codigoBilhete != null ) {
                    $bilheteModel = new BilheteModel;
                    $detalheBilheteModel = new DetalheBilheteModel;

                    $bilhete = $bilheteModel->store($codigoBilhete, null);
                    $detalheBilhete = [];
                    $message = null;
                    if ( $bilhete != null ) {
                        if ( $bilhete != '[]' ) {
                            foreach ( $bilhete as $b ) {
                                $detalheBilhete = $detalheBilheteModel->store($b->id_bilhete);
                                break;
                            }
                        } else {
                            $bilhete = null;
                            $message = 'Código do bilhete não encontrado no sistema';
                        }
                    } else {
                        $bilhete = null;
                        $message = 'Código do bilhete não encontrado no sistema';
                    }
                    
                    return view('admin.bilhetes' , [
                        'bilhete' => $bilhete ,
                        'detalheBilhete' => $detalheBilhete , 
                        'message' =>  $message,
                    ]);
                }
             }
        }

    }

    public function downloadPDF($cod_validacao_bilhete) {

        $bilhete = BilheteModel::store($cod_validacao_bilhete);

        
        if( $bilhete != [] ) { 
            $detalhe_bilhete = DetalheBilheteModel::store($bilhete[0]->id_bilhete);
            $cambista = UsuarioModel::getStore( $bilhete[0]->id_usuario , true);

            return view('admin.print_pdf' , [
                'detalhe_bilhete'   => $detalhe_bilhete,
                'bilhete'           => $bilhete,
                'cambista'          => $cambista,
            ]);

        } else {
            // bilhete não encontrado
        }

        
        // $pdf = PDF::loadView('' , [
        //     'bilhete' => $bilhete,
        //     'detalhe_bilhete' => $detalhe_bilhete,
        // ]);
        // return $pdf->downloload('bilhete_'.$codigo.'.pdf');

    }

    public function getConsultarBilheteExterno(Request $request) 
    {
        $bilheteModel = new BilheteModel;
        $detalheBilheteModel = new DetalheBilheteModel;
        
        $request->validate([
            'codigo_bilhete' => 'required'
        ]);

        $bilhete = $bilheteModel->store($request->codigo_bilhete, null);
        $detalheBilhete = [];
        $message = null;
        
        if ( $bilhete != null ) {
            if ( $bilhete != '[]' ) {
                foreach ( $bilhete as $b ) {
                    $detalheBilhete = $detalheBilheteModel->store($b->id_bilhete);
                    break;
                }
            } else {
                $bilhete = null;
                $message = 'Código do bilhete não encontrado no sistema';
            }
        } else {
            $bilhete = null;
            $message = 'Código do bilhete não encontrado no sistema';
        }
        
        return view('templates.bilhetes' , [
            'bilhete' => $bilhete ,
            'detalheBilhete' => $detalheBilhete , 
            'message' =>  $message,
        ]);
    }

    public function consultarBilheteExterno() {
        return view('templates.bilhetes');
    }

    public function getStore(Request $request)
    {
        $bilheteModel = new BilheteModel;
        $detalheBilheteModel = new DetalheBilheteModel;
        
        $request->validate([
            'codigo_bilhete' => 'required'
        ]);

        $bilhete = $bilheteModel->store($request->codigo_bilhete, null);
        $detalheBilhete = [];
        $message = null;
        if ( $bilhete != null ) {
            if ( $bilhete != '[]' ) {
                foreach ( $bilhete as $b ) {
                    $detalheBilhete = $detalheBilheteModel->store($b->id_bilhete);
                    break;
                }
            } else {
                $bilhete = null;
                $message = 'Código do bilhete não encontrado no sistema';
            }
        } else {
            $bilhete = null;
            $message = 'Código do bilhete não encontrado no sistema';
        }
        
        return view('admin.bilhetes' , [
            'bilhete' => $bilhete ,
            'detalheBilhete' => $detalheBilhete , 
            'message' =>  $message,
        ]);
    }

    public function getStoreDetail($codigoBilhete)
    {
        $bilheteModel = new BilheteModel;
        $detalheBilheteModel = new DetalheBilheteModel;

        $bilhete = $bilheteModel->store($codigoBilhete, null);
        $detalheBilhete = [];
        $message = null;
        if ( $bilhete != null ) {
            if ( $bilhete != '[]' ) {
                foreach ( $bilhete as $b ) {
                    $detalheBilhete = $detalheBilheteModel->store($b->id_bilhete);
                    break;
                }
            } else {
                $bilhete = null;
                $message = 'Código do bilhete não encontrado no sistema';
            }
        } else {
            $bilhete = null;
            $message = 'Código do bilhete não encontrado no sistema';
        }
        
        return view('admin.bilhetes' , [
            'bilhete' => $bilhete ,
            'detalheBilhete' => $detalheBilhete , 
            'message' =>  $message,
        ]);
    }

    public function getConfirm( Request $request )
    {
        $bilheteModel = new BilheteModel;
        $detalheBilheteModel = new DetalheBilheteModel;
        $request->validate([
            'nome_cliente' => 'required',
        ]);

        $nomeCliente = $request->nome_cliente;
        $codigoBilhete   = $request->validacao_bilhete;
        $bilhete = $bilheteModel->store($request->validacao_bilhete);
        $detalheBilhete = $detalheBilheteModel->store($bilhete[0]->id_bilhete);
        $valorAposta = 0;

        // tirar o valor do saldo
        if( Auth::check() ) {
            
            if( ( floatval ( Auth::user()->saldo_apostas ) - floatval ( $bilhete[0]->valor_aposta_bilhete ) ) < 0) {
                 return view('admin.bilhetes' , [
                    'message' => 'Seu usuário não possui saldo suficiente para efetivar jogos, favor entrar em contato com Administador e solicitar que seja inserido mais saldo para sua conta!',
                    'bilhete' => $bilheteModel->store( $request->validacao_bilhete ) ,
                    'detalheBilhete' => $detalheBilhete
                ]);
            } 
            
            if( isset ( Auth::user()->odd_minima ) ){
                // odd_minima_2_jogos
                if( count($detalheBilhete) == 1 ) {

                    if( intval( Auth::user()->odd_minima * 100 ) > intval( floatval( $bilhete[0]->valor_cotacao_aposta_bilhete ) * 100 ) ) {
                        return view('admin.bilhetes' , [
                            'bilhete' => $bilheteModel->store( $request->validacao_bilhete ) ,
                            'message' => 'O bilhete não pode ser validado pois a cotação da ODD é menor do que o permitido.' . PHP_EOL . 'Cotação mínima ' . Auth::user()->odd_minima
                        ]);
                    }

                }else if( count($detalheBilhete) > 1 ) {

                    if( isset ( Auth::user()->odd_minima_2_jogos ) ){

                        if( intval( Auth::user()->odd_minima_2_jogos * 100 ) > intval( floatval( $bilhete[0]->valor_cotacao_aposta_bilhete ) * 100 ) ) {
                            return view('admin.bilhetes' , [
                                'bilhete' => $bilheteModel->store( $request->validacao_bilhete ) ,
                                'message' => 'O bilhete não pode ser validado pois a cotação da ODD é menor do que o permitido.' . PHP_EOL . 'Cotação mínima ' . Auth::user()->odd_minima_2_jogos
                            ]);
                        }
                    }

                } else{
                    
                }
                
            } else {

                $arrayUsers['id'] = Auth::user()->id;
                $arrayUsers['saldo_apostas'] = ( floatval ( Auth::user()->saldo_apostas ) - floatval ( $valorAposta ) ) ;
                UsuarioModel::getUpdate($arrayUsers);
            }

            // verificar se existe jogos que já começaram
            
            foreach($detalheBilhete as $detalhe) {

                if( date('Y-m-d H:i:s', strtotime( $detalhe->data_hora_jogo_bilhete_detalhe ) )  < date('Y-m-d H:i:s', strtotime( now () ) ) ) {
                    return view('admin.bilhetes' , [
                        'message' => 'O bilhete não pode ser validado pois contém um ou mais jogos em andamento!',
                        'bilhete' => $bilheteModel->store( $request->validacao_bilhete ) ,
                        'detalheBilhete' => $detalheBilhete
                    ]);
                }
                
            }
            
            $array = [
                'nome_cliente_bilhete' => $nomeCliente,
                'codigo_validacao_bilhete' => $codigoBilhete,
                'bilhete_valido' => true,
                'id_usuario' => Auth::user()->id,
            ];
            
            $bilheteModel->getUpdate(  $array );
            
            $bilhete = $bilheteModel->store($request->validacao_bilhete, null);
            $detalheBilhete = [];
            
            
            foreach ( $bilhete as $b ) {
                $detalheBilhete = $detalheBilheteModel->store($b->id_bilhete);
                $valorAposta = $b->valor_aposta_bilhete;
                break;
            }
            
            
            
            return view('admin.bilhetes' , [
                'bilhete' => $bilheteModel->store( $request->validacao_bilhete ) ,
                'detalheBilhete' => $detalheBilhete
            ]);
        } else {
             return redirect("/");
        }
    }
        
    public function cancelarBilhete( $codigoBilhete ) 
    {
        if( Auth::check() === true && Auth::user()->id === 1) {

            // consultar o codigo do bilhete
            $bilhete =  BilheteModel::store($codigoBilhete);

            // consultar jogos detalhados do bilhete
            $detalheBilhete =  DetalheBilheteModel::store($bilhete[0]->id_bilhete);

            // verificar se já iniciou algum jogo
            foreach( $detalheBilhete as $b) {
                if($b->data_hora_jogo_bilhete_detalhe < date('Y-m-d H:i:s', strtotime( now () ) )) {
                    return view('admin.bilhetes' , [
                        'message' => 'O bilhete não pode ser cancelado pois contém um ou mais jogos em andamento!',
                        'bilhete' =>$bilhete ,
                        'detalheBilhete' => $detalheBilhete
                    ]);
                }
            }

            // inativar jogos do bilhete
            $detalheBilhete =  DetalheBilheteModel::removeAllItens($bilhete[0]->id_bilhete);

            // excluir o bilhete

            $array = [
                'codigo_validacao_bilhete' => $codigoBilhete,
                'bilhete_valido' => false,
                'id_usuario' => null,
                'bilhete_excluido' => true,
                'id_usuario_cancelou' => Auth::user()->id
            ];

            BilheteModel::getUpdate( $array );

            return view('admin.bilhetes' , [
                'message' => 'O bilhete foi excluido com sucesso!',
            ]);
        }
        return redirect("/");
    }
}

