<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UsuarioModel;
use App\Models\CaixaModel;
use App\Models\BilheteModel;

class CaixaController extends Controller
{
    public function get() 
    {
        if(Auth::check() === true) {

            if(Auth::user()->id_perfil == 1) {
                $usuarios = UsuarioModel::getStore();
            } else {
                 $usuarios = UsuarioModel::getStore(Auth::user()->id, true);
            }
        
            return view('admin.caixa', [
                'usuarios' => $usuarios
            ]);
        }
    }

    public function getStore(Request $request){
        
        if(Auth::check() == true) {

            if(Auth::user()->id_perfil == 1) {
                $usuarios = UsuarioModel::getStore();
            } else {
                 $usuarios = UsuarioModel::getStore(Auth::user()->id, true);
            }
            
            $usuario        = (isset($request->usuario)) ? $request->usuario : '';
            $dataInicio     = (isset($request->data_inicio)) ? $request->data_inicio : '';
            $dataFim        = (isset($request->data_fim)) ? $request->data_fim : '';
            $cliente        = (isset($request->cliente)) ? $request->cliente : '';
            $tipoRelatorio  = (isset($request->tipo_relatorio)) ? $request->tipo_relatorio : '';
            $parametros     = '';

            if($tipoRelatorio == ''){
                return view('admin.caixa', [
                    'usuarios' => $usuarios,
                    'message' => 'Selecione um tipo de relatório!',
                ]);
            }

            $array['usuario']        = $usuario;
            $array['data_inicio']    = $dataInicio;
            $array['data_fim']       = $dataFim;
            $array['cliente']        = $cliente;
            $array['tipo_relatorio'] = $tipoRelatorio;
            $dados = [];

            if($tipoRelatorio == 1) {
                $user_id = '';
                
                $users_dados = UsuarioModel::getStore( $usuario , true );

                foreach($users_dados as $key => $value) {

                    $dados[$value->id]['id']            = $value->id;
                    $dados[$value->id]['name']          = $value->name;
                    $dados[$value->id]['login']         = $value->login;
                    $dados[$value->id]['saldo_aposta']  = $value->saldo_apostas;
                    $dados[$value->id]['total_bilhete'] = BilheteModel::getTotalValores('total_bilhete', $value->id, $array);
                    $dados[$value->id]['total_premio']  = BilheteModel::getTotalValores('total_premio', $value->id , $array);
                    
                    $dados[$value->id]['data_inicio']   = ($dataInicio != '') ? date('d/m/Y',strtotime($dataInicio)) : date('d/m/Y',strtotime(BilheteModel::getDatas('data_inicio', $value->id , $array)[0]->created_at ) );
                    $dados[$value->id]['data_fim']      = ($dataFim != '') ? date('d/m/Y',strtotime($dataFim)) : date('d/m/Y',strtotime(BilheteModel::getDatas('data_fim'   , $value->id , $array)[0]->created_at ) );
                }   
            } else if($tipoRelatorio == 2) {
                
                if($usuario == '') {
                    return view('admin.caixa', [
                        'usuarios' => $usuarios,
                        'message' => 'Selecione um usuário para emitir o relatório detalhado!',
                    ]);
                }
                
                $arr_bilhetes = BilheteModel::store(null , null, $usuario , $array);

                if($arr_bilhetes == false) {
                    if($usuario == '') {
                        return view('admin.caixa', [
                            'usuarios' => $usuarios,
                            'message' => 'Não é permitido emitir relatório desta data selecionada!',
                        ]);
                    }   
                }
                // fazer a contagem dos valores
                $numeroApostasPagas  = 0;
                $numeroApostasPerdeu = 0;

                $totalBrutoPago   = 0;
                $totalBrutoPerdeu = 0;


                $totalPago        = 0;
                
                foreach($arr_bilhetes as $arr_b) {

                    // somar os valores de apostas perdidas
                    if($arr_b->bilhete_premiado == 0) {
                        $numeroApostasPerdeu++;
                        $totalBrutoPerdeu = $totalBrutoPerdeu + floatval($arr_b->valor_aposta_bilhete);
                    }

                    // somar os valores do bilhete premiado
                    if($arr_b->bilhete_premiado == 1) {
                        $numeroApostasPagas++;
                        $totalBrutoPago = $totalBrutoPago + floatval($arr_b->valor_aposta_bilhete);
                        $totalPago = $totalPago + floatval($arr_b->valor_premio_aposta_bilhete);
                    }
                    
                    
                }

                $dados['login_usuario']          = UsuarioModel::getStore($usuario , true)[0]->login;
                $dados['n_apostas_pagas']        = $numeroApostasPagas;
                $dados['n_apostas_perdidas']     = $numeroApostasPerdeu;

                $dados['total_bruto_pago']       = $totalBrutoPago;
                $dados['total_bruto_perdeu']       = $totalBrutoPerdeu;

                $dados['total_pago']             = ( $totalPago );
                $dados['total_perdeu']           = 0;

                $dados['total_comissao_pago']    = $totalBrutoPago * env('PORCENTAGEM_COMISSAO');
                $dados['total_comissao_perdeu']  = $totalBrutoPerdeu * env('PORCENTAGEM_COMISSAO');

                $dados['total_liquido_pago']     = ( $totalBrutoPago + ( $totalBrutoPago * env('PORCENTAGEM_COMISSAO') ) ) - $totalPago;
                $dados['total_liquido_perdeu']   = $totalBrutoPerdeu - ( $totalBrutoPerdeu * env('PORCENTAGEM_COMISSAO') );
            }
            
            return view('admin.caixa', [
                'usuarios' => $usuarios,
                'dados' => $dados,
                'tipo_relatorio' => $tipoRelatorio,
            ]);           
        }
    }
}


