<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ConsultarApiBet365 extends Controller
{
    /********************************************************
     *                  dados de acesso a API               *
     * ------------------------------------------------------
     * link: https://betsapi.com/                           *
     * token: 84184-PGqIMvGpNKmkRy                          *
     *******************************************************/

    private $api_key = '84184-PGqIMvGpNKmkRy';
    // private $api_site = 'https://api.b365api.com/v1/events/inplay';
    private $api_site = "https://api.b365api.com/v1/bet365/upcoming";


    // retorna os dados da API para o blade
    public function get ()
    {
        $response = json_decode($this->getDataEvents());
        
        if($response->success != '1') {
            $message = "Erro ao carregar os dados, favor tente novamente mais tarde";
            return view( 'index' , compact('message') ); 
        }

        $this->monstarTabelaJogos($response);
        // linha onde mostra todos os dados retornados da API
        //dd($response->results );
        return view( 'index', [
            'menu' => $this->mountLeftMenu($response) ,
            'dados_total' => $response->results 
        ]); 
        
    }
     

    // carrega os dados da API
    private function getDataEvents() 
    {
        return Http::get($this->api_site , [
            'sport_id' => 1,
            'token' => $this->api_key,
            'LNG_ID' => 22,
        ]);
    }

    /* montar menu lateral */
    private function mountLeftMenu($data) 
    {

        $result = $data->results;
        $league = '';
        $arr = [];
    
        foreach ($result as $r) {
            $arr[] = $r->league; 
        }

        sort($arr);
        // tira as ligas duplicadas
        foreach($arr as $r) {
            if($league !=  $r->name) {
                $return[] = $r; 
                $league = $r->name;
            }
        }
        
        return $return;
    }

    private function monstarTabelaJogos ($data , $filter = null)
    {
        $result = $data->results;
        $arr = [];

        foreach ($result as $r) {
            $arr['timeCasa'] = $r->home;
            $arr['timeFora'] = $r->away;
        }
    }

}
