<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use app\config\session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ConsultarSporteBetNet extends Controller
{
    /********************************************************
     *                  dados de acesso a API               *
     * ------------------------------------------------------
     * link: https://betsapi.com/                           *
     * token: 84184-PGqIMvGpNKmkRy                          *
     *******************************************************/

    // private $api_key = '84184-PGqIMvGpNKmkRy';
    // private $api_site = 'https://api.b365api.com/v1/events/inplay';

    // private $api_site = "https://masterbets.com.br";
    // private $api_site = "https://esportenet.bet";
    private $api_site = "https://www.esportenetsp.com.br";
    // private $api_site = "https://esportenetvip.com.br/";
    
    // retorna os dados da API para o blade
    public function get (Request $request)
    {
        // dd( bcrypt('desativado') ) ;
        Auth::logout();
        
        if( Auth::check() === true ) {
            $request->session()->forget('dadosTratados');
            $request->session()->forget('bilheteAtual');
            $request->session()->forget('dadosTratadosAdmin');
        }else {
            $request->session()->flush();
        }

        $dadosTratados = $this-> tratarJson( json_decode( $this->getDataEvents() ) );
        $request->session()->put('dadosTratados', $dadosTratados);
        return view( 'index', [
            'data'          => $dadosTratados
        ]); 
    }

    public function getOddsDetail($campCode) {
        return json_decode(Http::get($this->api_site.'/futebolapi/api/CampJogo/getOdds/'.$campCode));
        // dd(json_decode(Http::get($this->api_site.'/futebolapi/api/CampJogo/getOdds/'.$campCode)) );
    }

    public function getAdmin(Request $request) {
        $dadosTratados = $this-> tratarJson( json_decode( $this->getDataEvents() ) );
        $request->session()->put('dadosTratadosAdmin', $dadosTratados);
        return $dadosTratados;
    }

    // carrega os dados da API
    private function getDataEvents() 
    {
        return Http::get($this->api_site."/futebolapi/api/CampJogo/getEvents/1");
    }

    public function loadMenuDataFilter($data_filter , Request $request)
    {
        $request->session()->forget('dadosTratados');

        $dadosTratados = $this-> tratarJsonDetalhe( json_decode( $this->getDataEvents() ) , $data_filter );
        $request->session()->put('dadosTratados', $dadosTratados);
        if(@Auth::check() === true){
            return view( 'admin.dashboard', [
                'data'          => $dadosTratados
            ]); 
        } else {
            return view( 'index', [
                'data'          => $dadosTratados
            ]); 
        }
        
        return view('templates.bilhetes');
    }

    // tratar json retornado pela aplicação
    private function tratarJson($data) 
    {
        $arr = [];
        foreach ($data as $d) {
            $arr[$d->Name] = json_decode($d->Value);
        }
        
        $campeonatos = [];
        $campeonato = '';
        $cont = 0;

        foreach( $arr as $a) {
            
            if(strcmp($campeonato , $a->camp_nome)) {
                $campeonatos[$a->camp_nome][$cont] = $a;
                $cont++;
            }else{
                $campeonatos[$a->camp_nome][$cont] = $a;
                $cont = 0;
            }
            $campeonato = $a->camp_nome;
        }

        ksort($campeonatos);
        return($campeonatos);
    }

    // tratar json retornado pela aplicação
    private function tratarJsonDetalhe($data , $data_filter = null) 
    {
           $arr = [];
        foreach ($data as $d) {
            $arr[$d->Name] = json_decode($d->Value);
        }
        
        $campeonatos = [];
        $campeonato = '';
        $cont = 0;

        foreach( $arr as $a) {
            if( $data_filter != null ) {

                if ( substr( $data_filter , 0 , 10 ) == substr( $a->dt_hr_ini  , 0 , 10)  ) {
                    
                    if(strcmp($campeonato , $a->camp_nome)) {
                        // se já existir a posição setada no vetor, procura uma nova posição
                        if(isset($campeonatos[$a->camp_nome][$cont])) {
                            $i=0;
                            while(isset($campeonatos[$a->camp_nome][$cont + $i]) ) {
                                $i++;
                            }
                            $campeonatos[$a->camp_nome][$cont + $i] = $a;    
                        }else{
                            $campeonatos[$a->camp_nome][$cont] = $a;
                        }

                        $cont++;
                    }else{
                        $campeonatos[$a->camp_nome][$cont] = $a;
                        $cont = 0;
                    }
                    $campeonato = $a->camp_nome;
                }
            } else {
                if(strcmp($campeonato , $a->camp_nome)) {
                    $campeonatos[$a->camp_nome][$cont] = $a;
                    $cont++;
                }else{
                    $campeonatos[$a->camp_nome][$cont] = $a;
                    $cont = 0;
                }
                $campeonato = $a->camp_nome;
            }
        }

        ksort($campeonatos);
        return($campeonatos);
    }

}
