<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DetalheBilheteModel;
use App\Models\BilheteModel;
use Illuminate\Support\Facades\Auth;

class DetalheBilheteController extends Controller
{
    public function getRemoveItem($value, Request $request) {

        if( Auth::check()) {
            $BilheteModel = new BilheteModel;
            $detalheBilheteModel = new DetalheBilheteModel;

            $detalheBilheteModel->removeItem($value);
            $valorCotacao = 0;
            $idBilhete;
            $codBilhete;
            
            // recalcular bilhete
            foreach ( $detalheBilheteModel->store() as $db ) {

                $idBilhete = $db->id_bilhete;

                if($valorCotacao == 0) {
                    $valorCotacao = $db->taxa_odd_bilhete_detalhe;
                } else {
                    $valorCotacao = $valorCotacao * $db->taxa_odd_bilhete_detalhe;
                }

            }

            // atualiza premio bilhete
            $bilhete = $BilheteModel->store(null , $idBilhete);
            
            foreach($bilhete as $b) {
                $valorPremioAtt = str_replace(",", ".", $b->valor_aposta_bilhete) * $valorCotacao;
                $array = [
                    'id_bilhete' => $idBilhete,
                    'valor_cotacao_aposta_bilhete' => $valorCotacao,
                    'valor_premio_aposta_bilhete' => $valorPremioAtt,
                ];
                $BilheteModel->getUpdate( $array ); 
                $codeBilhete = $b->codigo_validacao_bilhete;
            }
        }

        $bilhete = $BilheteModel->store(null , $idBilhete);;
        $detalheBilhete = [];

        foreach($bilhete as $b){
            $detalheBilhete = $detalheBilheteModel->store($b->id_bilhete);
            break;
        }
        
        return view('admin.bilhetes',[
            'bilhete' => $bilhete,
            'detalheBilhete' => $detalheBilhete
        ]);
    }
}
