<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MapeamentoOddsController extends Controller
{
    //
    public function returnMapeamentoOdds( $data , $codeJogo ) {

        if (env('SITE_CONSULTA_ODDS') == 'master_bets') {
            return $this->montarTableModalMasterBets($data , $codeJogo);
        } else if(env('SITE_CONSULTA_ODDS') == 'esportenetsp') {
            return $this->montarTableModalEsporteNetSP($data , $codeJogo);
        }
    }


    // mapeamento das ODDS MASTER BETS
    private function montarTableModalMasterBets($data , $codeJogo) {

        $vencedorEncontro           = []; // Vencedor do Encontro
        $duplaChance                = []; // Dupla Chance
        $totalGolsCasa              = []; // Total de Gols da Casa
        $totalGolsVisitante         = []; // Total de Gols do Visitante
        $totalGolsJogo              = []; // Total de Gols no Jogo
        $ambasEquipes               = []; // Ambas as Equipes
        $retultadoExato             = []; // Resultado Exato
        $handicapGol                = []; // Handicap de Gol
        $handicapAsiatico           = []; // Handicap Asiático
        $intervFinalJogo            = []; // Intervalo / Final de Jogo
        $empAnulaAposta             = []; // Empate Anula a Aposta
        $resultadoTotalGols         = []; // Resultado / Total de Gols
        $ambasComResultado          = []; // Ambas com Resultado
        $ambasMarcamAmbosTempos     = []; // Ambas marcam em ambos os tempos
        $escanteiosCantos           = []; // Cantos
        $metadeComMaisGols          = []; // Metados com mais gols
        $totalGolsAmbasMarcam       = []; // Total de gols com ambas marcam
        $totalExatoGols             = []; // Total exato de gols
        $duplaChance                = []; // Dupla Chance
        $totalGolsPrimTempo         = []; // Total de Gols no Jogo
        $ambasEquipesPriTemp        = []; // Ambas as Equipes
        $totalGolsParImpar          = []; // Total de Gols Par ou Ímpar
        $resultadoExato             = []; // Resultado Exato
        $resultadoExatoPrimTempo    = []; // Resultado Exato
        $ambasComResultadoPriTempo  = []; // Ambas com Resultado
        $totalExatoGolsPriTempo     = []; // Total exato de gols
        $vencedorEncontroPriTempo   = []; // Vencedor do Encontro
        $totalGolsSegTempo          = []; // Total de Gols no Jogo
        $ambasEquipesSegTemp        = []; // Ambas as Equipes
        $totalGolsParImparPriTempo  = []; // Total de Gols Par ou Ímpar
        $totalGolsParImparSegTempo  = []; // Total de Gols Par ou Ímpar
        $totalExatoGolsSegTempo     = []; // Total exato de gols
        $duplaChancePriTempo        = []; // Dupla Chance
        $resultado_total_gols_2_5   = []; // Resultado / Total de Gols

        foreach($data as $d) {
            $arr = json_decode($d->Value);
                switch($arr->odd_tipo) {

                    // ** VENCEDOR DO ENCONTRO ***

                    case 1: //casa
                        $vencedorEncontro[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 2: // empate
                            $vencedorEncontro[$arr->descricao] = [
                                'jog_odd_id' => $arr->jog_odd_id,
                                'descricao' => $arr->descricao,
                                'taxa' => $arr->taxa,
                                'codeJogo' => $codeJogo,
                            ];
                        break;
                    case 3: // fora
                            $vencedorEncontro[$arr->descricao] = [
                                'jog_odd_id' => $arr->jog_odd_id,
                                'descricao' => $arr->descricao,
                                'taxa' => $arr->taxa,
                                'codeJogo' => $codeJogo,
                            ];
                        break;

                    // *** DUPLA CHANCE ***

                    case 4: // casa ou fora
                        $duplaChance[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 5: // Casa ou Empate
                        $duplaChance[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 6: // fora ou empate
                        $duplaChance[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 7:
                        break;

                    //  *** TOTAL DE GOLS EM CASA ***

                    case 8: // Casa - Acima 1.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 9: // Casa - Acima 2.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 10: // Casa - Acima 3.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 11: // Casa - Acima 4.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18: // Casa - Abaixo 1.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 19: // Casa - Abaixo 2.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 20: // Casa - Abaixo 3.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    
                    // *** TOTAL DE GOLS DO VISITANTE ***

                    case 28: //Fora - Acima 1.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 29: //Fora - Acima 2.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 30: //Fora - Acima 3.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 31:
                        break;
                    case 32:
                        break;
                    case 33:
                        break;
                    case 34:
                        break;
                    case 35:
                        break;
                    case 36:
                        break;
                    case 37:
                        break;
                    case 38: // Fora - Abaixo 1.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 39: //Fora - Abaixo 2.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 40:
                        break;
                    case 41:
                        break;
                    case 42:
                        break;
                    case 43:
                        break;
                    case 44:
                        break;
                    case 45:
                        break;
                    case 46:
                        break;
                    case 47:
                        break;

                    // *** TOTAL DE GOLS NO JOGO ***

                    case 48: // Jogo - Acima 1.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 49: // Jogo - Acima 2.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 50: // Jogo - Acima 3.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 51: // Jogo - Acima 4.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 52: // Jogo - Acima 5.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 53: // Jogo - Acima 6.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 54:
                        break;
                    case 55:
                        break;
                    case 56:
                        break;
                    case 57: // Jogo - Abaixo 0.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 58: // Jogo - Abaixo 1.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 59: // Jogo - Abaixo 2.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 60: // Jogo - Abaixo 3.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 61: // Jogo - Abaixo 4.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 62:
                        break;
                    case 63:
                        break;
                    case 64:
                        break;
                    case 65:
                        break;
                    case 66:
                        break;
                    case 67:
                        break;
                    case 68:
                        break;

                    // *** AMBAS EQUIPES *** 

                    case 69: // ambas marcam : sim
                        $ambasEquipes[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 70: // ambas marcam : não
                        $ambasEquipes[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 71:
                        break;
                    case 72:
                        break;
                    case 73:
                        break;
                    case 74:
                        break;
                    case 75:
                        break;
                    case 76:
                        break;
                    case 77:
                        break;
                    case 78:
                        break;
                    case 79:
                        break;
                    case 80:
                        break;
                    case 81:
                        break;
                    case 82:
                        break;
                    case 83:
                        break;
                    case 84:
                        break;
                    case 85:
                        break;
                    case 86:
                        break;
                    case 87:
                        break;
                    case 88:
                        break;
                    case 89:
                        break;
                    case 90:
                        break;
                    case 91:
                        break;
                    case 92:
                        break;
                    case 93:
                        break;
                    case 94:
                        break;
                    case 95:
                        break;
                    case 96:
                        break;
                    case 97:
                        break;
                    case 98:
                        break;
                    case 99:
                        break;
                    case 100:
                        break;

                    // *** VENCEDOR DO ENCONTRO ***

                    case 101: // 1º Tempo - Casa
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 102: // 1º Tempo - Empate
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 103: // 1º Tempo - Fora
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 104: // 2º Tempo - Casa
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 105: // 2º Tempo - Empate
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 106: // 2º Tempo - Fora
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                        
                    
                    // *** TOTAL DE GOLS NO PRIMEIRO TEMPO ***
                    
                    case 107: // 1º Tempo - Jogo Acima 0.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 108: // 1º Tempo - Jogo Acima 1.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 109: // 1º Tempo - Jogo Acima 2.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 110: // 1º Tempo - Jogo Acima 3.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 111:
                        break;
                    case 112:
                        break;
                    case 113:
                        break;
                    case 114:
                        break;
                    case 115:
                        break;
                    case 116:
                        break;
                    case 117: // 1º Tempo - Jogo Abaixo 0.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 118: // 1º Tempo - Jogo Abaixo 1.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 119: // 1º Tempo - Jogo Abaixo 2.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 120:
                        break;
                    case 121:
                        break;
                    case 122:
                        break;
                    case 123:
                        break;
                    case 124:
                        break;
                    case 125:
                        break;
                    case 126:
                        break;

                    // *** TOTAL DE JOGOS NO SEGUNDO TEMPO ***

                    case 127: // 2º Tempo - Jogo Acima 0.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 128: // 2º Tempo - Jogo Acima 1.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 129: // 2º Tempo - Jogo Acima 2.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 130: // 2º Tempo - Jogo Acima 3.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 131: // 2º Tempo - Jogo Acima 3.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 132:
                        break;
                    case 133:
                        break;
                    case 134:
                        break;
                    case 135:
                        break;
                    case 136:
                        break;
                    case 137: // 2º Tempo - Jogo Abaixo 0.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 138: // 2º Tempo - Jogo Abaixo 1.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 139: // 2º Tempo - Jogo Abaixo 2.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 140: // 2º Tempo - Jogo Abaixo 3.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 141:
                        break;
                    case 142:
                        break;
                    case 143:
                        break;
                    case 144:
                        break;
                    case 145:
                        break;
                    case 146:
                        break;

                    // *** TOTAL DE GOLS PAR OU IMPAR ***

                    case 147: // Total de Gols Ímpar
                        $totalGolsParImpar[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 148: // Total de Gols Par
                        $totalGolsParImpar[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // *** TOTAL DE GOLS PAR OU IMPAR 1º TEMPO ***

                    case 149: // 1º tempo - Total de Gols Ímpar 
                        $totalGolsParImparPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 150: // 1º tempo - Total de Gols Par
                        $totalGolsParImparPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break; 
                        
                    // *** TOTAL DE GOLS PAR OU IMPAR 2º TEMPO ***

                    case 151: // 2º tempo - Total de Gols Ímpar 
                        $totalGolsParImparSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 152: // 2º tempo - Total de Gols Par
                        $totalGolsParImparSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 153:
                        break;
                    case 154:
                        break;
                    case 155:
                        break;
                    case 156:
                        break;
                    case 157:
                        break;
                    case 158:
                        break;
                    case 159:
                        break;
                    case 160:
                        break;
                    case 161:
                        break;
                    case 162:
                        break;
                    case 163:
                        break;
                    case 164:
                        break;
                    case 165:
                        break;
                    case 166:
                        break;
                    case 167:
                        break;
                    case 168:
                        break;
                    case 169:
                        break;
                    case 170:
                        break;
                    
                    // *** DUPLA CHANCE 1º TEMPO ***

                    case 171: // 1º Tempo - Casa ou Fora 
                        $duplaChancePriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 172: // 1º Tempo - Casa ou Empate
                        $duplaChancePriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 173: // 1º Tempo - Fora ou Empate
                        $duplaChancePriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 174:
                        break;
                    case 175:
                        break;
                    case 176:
                        break;
                    
                    // *** INTERVALO / FINAL DE JOGO ***

                    case 177: // Intervalo | Final (Casa | Casa)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 178: // Intervalo | Final (Casa | Empate)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 179: // Intervalo | Final (Casa | Fora)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 180: // Intervalo | Final (Empate | Casa)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 181: // Intervalo | Final (Empate | Empate)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 182: // Intervalo | Final (Empate | Fora)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 183: // Intervalo | Final (Fora | Casa)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 184: // Intervalo | Final (Fora | Empate)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 185: // Intervalo | Final (Fora | Fora)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  

                    // *** EMPATE ANULA A APOSTA ***

                    case 186: // Empate não tem aposta - Casa
                        $empAnulaAposta[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 187: // Empate não tem aposta - Fora
                        $empAnulaAposta[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 188:
                        break;
                    case 189:
                        break;
                    case 190:
                        break;
                    case 191:
                        break;

                        // *** AMBAS AS EQUIPES 1º TEMPO ***

                    case 192: // 1º Tempo - Ambas Marcam - sim
                        $ambasEquipesPriTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 193: // 1º Tempo - Ambas Marcam - não
                        $ambasEquipesPriTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;  

                    // *** AMBAS AS EQUIPES 2º TEMPO ***

                    case 194: // 2º Tempo - Ambas Marcam - sim
                        $ambasEquipesSegTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 195: // 2º Tempo - Ambas Marcam - não
                        $ambasEquipesSegTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                        
                    // *** HANDICAP DE GOL

                    case 250: // Handicap Casa (-1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 251: // Handicap Casa (-2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 252: // Handicap Casa (-3)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 255: // Handicap Casa (+1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 280: // Handicap Empate (-1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 281: // Handicap Empate (-2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 285: // Handicap Empate (+1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 286: // Handicap Empate (+2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 287: // Handicap Empate (+3)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 310: // Handicap Fora (-1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 311: // Handicap Fora (-2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 315: // Handicap Fora (+1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 316: // Handicap Fora (+2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 317: // Handicap Fora (+3)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // // *** HANDICAP ASIATICO

                    // case 380: // Handicap Asiático Casa (-0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 381: // Handicap Asiático Casa (-1.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 382: // Handicap Asiático Casa (-2.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 386: // Handicap Asiático Casa (+0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 416: // Handicap Asiático Fora (-0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 422: // Handicap Asiático Fora (+0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 423: // Handicap Asiático Fora (+1.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 424: // Handicap Asiático Fora (+2.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                       
                    // *** RESULTADO EXATO ***

                    case 510: // Resultado Exato (1 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 511: // Resultado Exato (1 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 512: // Resultado Exato (1 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 513: // Resultado Exato (1 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 514: // Resultado Exato (1 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 515: // Resultado Exato (1 : 5)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 520: // Resultado Exato (2 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 521: // Resultado Exato (2 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 522: // Resultado Exato (2 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 523: // Resultado Exato (2 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 524: // Resultado Exato (2 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 525: // Resultado Exato (2 : 5)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 530: // Resultado Exato (3 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 531: // Resultado Exato (3 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 532: // Resultado Exato (3 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 533: // Resultado Exato (3 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 534: // Resultado Exato (3 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 540: // Resultado Exato (4 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 541: // Resultado Exato (4 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 542: // Resultado Exato (4 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 543: // Resultado Exato (4 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 544: // Resultado Exato (4 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 550: // Resultado Exato (5 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 551: // Resultado Exato (5 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 552: // Resultado Exato (5 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 553: // Resultado Exato (5 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 560: // Resultado Exato (6 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 561: // Resultado Exato (6 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 562: // Resultado Exato (6 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 563: //Resultado Exato (6 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 570: // Resultado Exato (7 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 571: // Resultado Exato (7 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                        
                    // *** RESULTADO EXATO 1º TEMPO ***


                    case 600: // 1º Tempo - Resultado Exato (0 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 601: // 1º Tempo - Resultado Exato (0 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 602: // 1º Tempo - Resultado Exato (0 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 603: // 1º Tempo - Resultado Exato (0 : 3)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 610: // 1º Tempo - Resultado Exato (1 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 611: // 1º Tempo - Resultado Exato (1 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 612: // 1º Tempo - Resultado Exato (1 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 613: // 1º Tempo - Resultado Exato (1 : 3
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 620: // 1º Tempo - Resultado Exato (2 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 621: // 1º Tempo - Resultado Exato (2 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 622: // 1º Tempo - Resultado Exato (2 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 623: // 1º Tempo - Resultado Exato (2 : 3)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 630: // 1º Tempo - Resultado Exato (3 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 631: // 1º Tempo - Resultado Exato (3 : 1)"
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 632: // 1º Tempo - Resultado Exato (3 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 640: // 1º Tempo - Resultado Exato (4 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 641: // 1º Tempo - Resultado Exato (4 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 642: // 1º Tempo - Resultado Exato (4 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 650: // 1º Tempo - Resultado Exato (5 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    break;

                    // Ambas com Resultado
                    case 827: // Ambas marcam: nao / Casa
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 824: // Ambas marcam: sim / Casa
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 828: // Ambas marcam: nao / Empate
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 825: // Ambas marcam: sim / Empate
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 829: // Ambas marcam: nao / Fora
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;    
                    case 826: // Ambas marcam: sim / Fora
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    //

                    // ***TOTAL EXATO DE GOLS***

                    case 101125: // Número de gols exato (5)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101126: // Número de gols exato (4)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101127: // Número de gols exato (3)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101128: // Número de gols exato (2)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101129: // Número de gols exato (1)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101130: // Número de gols exato (0)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101138: // Número de gols exato (6)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // ***TOTAL EXATO DE GOLS 1º TEMPO ***
                    
                    case 101032: // 1º Tempo número de gols exato (0)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101033: // 1º Tempo número de gols exato (1)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101038: // 1º Tempo número de gols exato (2)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101065: // 1º Tempo número de gols exato (3)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101149: // 1º Tempo número de gols exato (4)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // **** Resultado / Total de Gols ****
                    case 101119: // Resultado casa / Total de gols acima de 2.5

                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        //$resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101120: // Resultado casa / Total de gols abaixo de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101136: // Resultado fora / Total de gols acima de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101135: // Resultado fora / Total de gols abaixo de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101131: // Resultado empate / Total de gols acima de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101137: //Resultado empate / Total de gols abaixo de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;
                        
                        /********** cantos escanteios *******/
                    case 101101: // Cantos exatamente (3)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101103: // Cantos exatamente (4)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101117: // Cantos exatamente (5)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101114: // Cantos exatamente (6)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101111: // Cantos exatamente (7)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101108: // Cantos exatamente (8)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101105: // Cantos exatamente (9)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101115: // Cantos acima de (6)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101112: // Cantos acima de (7)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101109: // Cantos acima de (8)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101098: // Cantos acima de (9)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101100: // Cantos abaixo de (3)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101113: // Cantos abaixo de (4)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101116: // Cantos abaixo de (5)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101107: // Cantos abaixo de (6)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101110: // Cantos abaixo de (7)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101106: // Cantos abaixo de (8)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101104: // Cantos abaixo de (9)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => $arr->taxa,
                            'codeJogo' => $codeJogo,
                        ];
                        break;
            }
            
        }

        // montar o menu com o cabeçalho
        $menu = '<table border=\'1\' class=\'table table-striped\'>';
        $result = '';

        if ( $vencedorEncontro != [] )
            $menu = $menu . $this->orderListOdds($vencedorEncontro , 'Vencedor do Encontro');
        if ( $duplaChance != [] )
            $menu = $menu . $this->orderListOdds($duplaChance , 'Dupla Chance');
        if ( $totalGolsCasa != [] )
            $menu = $menu . $this->orderListOdds($totalGolsCasa , 'Total de Gols da Casa');
        if ( $totalGolsVisitante != [] )
            $menu = $menu . $this->orderListOdds($totalGolsVisitante , 'Total de Gols do Visitante');
        if ( $totalGolsJogo != [] )
            $menu = $menu . $this->orderListOdds($totalGolsJogo , 'Total de Gols no Jogo');
        if ( $ambasEquipes != [] )
            $menu = $menu . $this->orderListOdds($ambasEquipes , 'Ambas as Equipes');
        if ( $retultadoExato != [] )
            $menu = $menu . $this->orderListOdds($retultadoExato , 'Resultado Exato');
        if ( $handicapGol != [] )
            $menu = $menu . $this->orderListOdds($handicapGol , 'Handicap de Gol');
        if ( $handicapAsiatico != [] )
            $menu = $menu . $this->orderListOdds($handicapAsiatico , 'Handicap Asiático');
        if ( $intervFinalJogo != [] )
            $menu = $menu . $this->orderListOdds($intervFinalJogo , 'Intervalo / Final de Jogo');
        if ( $empAnulaAposta != [] )
            $menu = $menu . $this->orderListOdds($empAnulaAposta , 'Empate Anula a Aposta');
        if ( $resultadoTotalGols != [] )
            $menu = $menu . $this->orderListOdds($resultadoTotalGols , 'Resultado / Total de Gols');
        if ( $ambasComResultado != [] )
            $menu = $menu . $this->orderListOdds($ambasComResultado , 'Ambas com Resultado');
        if ( $ambasMarcamAmbosTempos != [] )
            $menu = $menu . $this->orderListOdds($ambasMarcamAmbosTempos , 'Ambas marcam em ambos os tempos');
        if ( $escanteiosCantos != [] )
            $menu = $menu . $this->orderListOdds($escanteiosCantos , 'Escanteios');
        if ( $metadeComMaisGols != [] )
            $menu = $menu . $this->orderListOdds($metadeComMaisGols , 'Metade com mais gols');
        if ( $totalGolsAmbasMarcam != [] )
            $menu = $menu . $this->orderListOdds($totalGolsAmbasMarcam , 'Total de gols com ambas marcam');
        if ( $totalExatoGols != [] )
            $menu = $menu . $this->orderListOdds($totalExatoGols , 'Total exato de gols');
        if ( $totalGolsPrimTempo != [] )
            $menu = $menu . $this->orderListOdds($totalGolsPrimTempo , 'Total de Gols no Jogo');
        if ( $ambasEquipesPriTemp != [])
            $menu = $menu . $this->orderListOdds($ambasEquipesPriTemp , 'Ambas as Equipes');
        if ( $totalGolsParImpar != [] )
            $menu = $menu . $this->orderListOdds($totalGolsParImpar , 'Total de Gols Par ou Ímpar');
        if ( $resultadoExato != [] )
            $menu = $menu . $this->orderListOdds($resultadoExato , 'Resultado Exato');
        if ( $resultadoExatoPrimTempo  != [] )
            $menu = $menu . $this->orderListOdds($resultadoExatoPrimTempo , 'Resultado Exato 1º tempo');
        if ( $ambasComResultadoPriTempo != [] )
            $menu = $menu . $this->orderListOdds($ambasComResultadoPriTempo , 'Ambas com Resultado');
        if ( $totalExatoGolsPriTempo != [] )
            $menu = $menu . $this->orderListOdds($totalExatoGolsPriTempo , 'Total exato de gols');
        if ( $vencedorEncontroPriTempo != [] )
            $menu = $menu . $this->orderListOdds($vencedorEncontroPriTempo , 'Vencedor do Encontro');
        if ( $totalGolsSegTempo != [] )
            $menu = $menu . $this->orderListOdds($totalGolsSegTempo , 'Total de Gols no Jogo');
            if ( $ambasEquipesSegTemp != [] )    
            $menu = $menu . $this->orderListOdds($ambasEquipesSegTemp , 'Ambas as Equipes');
        if ( $totalGolsParImparPriTempo != [] )    
            $menu = $menu . $this->orderListOdds($totalGolsParImparPriTempo , 'Total de Gols Par ou Ímpar');
        if ( $totalGolsParImparSegTempo != [] )    
            $menu = $menu . $this->orderListOdds($totalGolsParImparSegTempo , 'Total de Gols Par ou Ímpar');
        if ( $totalExatoGolsSegTempo != [] )    
            $menu = $menu . $this->orderListOdds($totalExatoGolsSegTempo , 'Total exato de gols');
        if ( $duplaChancePriTempo != [] )    
            $menu = $menu . $this->orderListOdds($duplaChancePriTempo , 'Dupla Chance');
        if ( $resultado_total_gols_2_5 != [] ) { 
            $menu = $menu . $this->orderListOdds($resultado_total_gols_2_5 , 'Resultado / Total de Gols');   
        }
            

        return ($menu . '</table>');
        
    }


    // mapeamento das ODDS da ESPORTENETSP
    // mapeamento das ODDS MASTER BETS
    private function montarTableModalEsporteNetSP($data , $codeJogo) {

        $vencedorEncontro           = []; // Vencedor do Encontro
        $duplaChance                = []; // Dupla Chance
        $totalGolsCasa              = []; // Total de Gols da Casa
        $totalGolsVisitante         = []; // Total de Gols do Visitante
        $totalGolsJogo              = []; // Total de Gols no Jogo
        $ambasEquipes               = []; // Ambas as Equipes
        $retultadoExato             = []; // Resultado Exato
        $handicapGol                = []; // Handicap de Gol
        $handicapAsiatico           = []; // Handicap Asiático
        $intervFinalJogo            = []; // Intervalo / Final de Jogo
        $empAnulaAposta             = []; // Empate Anula a Aposta
        $resultadoTotalGols         = []; // Resultado / Total de Gols
        $ambasComResultado          = []; // Ambas com Resultado
        $ambasMarcamAmbosTempos     = []; // Ambas marcam em ambos os tempos
        $escanteiosCantos           = []; // Cantos
        $metadeComMaisGols          = []; // Metados com mais gols
        $totalGolsAmbasMarcam       = []; // Total de gols com ambas marcam
        $totalExatoGols             = []; // Total exato de gols
        $duplaChance                = []; // Dupla Chance
        $totalGolsPrimTempo         = []; // Total de Gols no Jogo
        $ambasEquipesPriTemp        = []; // Ambas as Equipes
        $totalGolsParImpar          = []; // Total de Gols Par ou Ímpar
        $resultadoExato             = []; // Resultado Exato
        $resultadoExatoPrimTempo    = []; // Resultado Exato
        $ambasComResultadoPriTempo  = []; // Ambas com Resultado
        $totalExatoGolsPriTempo     = []; // Total exato de gols
        $vencedorEncontroPriTempo   = []; // Vencedor do Encontro
        $totalGolsSegTempo          = []; // Total de Gols no Jogo
        $ambasEquipesSegTemp        = []; // Ambas as Equipes
        $totalGolsParImparPriTempo  = []; // Total de Gols Par ou Ímpar
        $totalGolsParImparSegTempo  = []; // Total de Gols Par ou Ímpar
        $totalExatoGolsSegTempo     = []; // Total exato de gols
        $duplaChancePriTempo        = []; // Dupla Chance
        $resultado_total_gols_2_5   = []; // Resultado / Total de Gols

        foreach($data as $d) {
            $arr = json_decode($d->Value);
                switch($arr->odd_tipo) {

                    // ** VENCEDOR DO ENCONTRO ***

                    case 1: //casa
                        $vencedorEncontro[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 2: // empate
                            $vencedorEncontro[$arr->descricao] = [
                                'jog_odd_id' => $arr->jog_odd_id,
                                'descricao' => $arr->descricao,
                                'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_PORCENTAGEM') ) + $arr->taxa ),
                                'codeJogo' => $codeJogo,
                            ];
                        break;
                    case 3: // fora
                            $vencedorEncontro[$arr->descricao] = [
                                'jog_odd_id' => $arr->jog_odd_id,
                                'descricao' => $arr->descricao,
                                'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_PORCENTAGEM') ) + $arr->taxa ),
                                'codeJogo' => $codeJogo,
                            ];
                        break;

                    // *** DUPLA CHANCE ***

                    case 4: // casa ou fora
                        $duplaChance[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 5: // Casa ou Empate
                        $duplaChance[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 6: // fora ou empate
                        $duplaChance[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 7:
                        break;

                    //  *** TOTAL DE GOLS EM CASA ***

                    case 8: // Casa - Acima 1.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 9: // Casa - Acima 2.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 10: // Casa - Acima 3.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 11: // Casa - Acima 4.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 12:
                        break;
                    case 13:
                        break;
                    case 14:
                        break;
                    case 15:
                        break;
                    case 16:
                        break;
                    case 17:
                        break;
                    case 18: // Casa - Abaixo 1.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 19: // Casa - Abaixo 2.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 20: // Casa - Abaixo 3.5
                        $totalGolsCasa[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 21:
                        break;
                    case 22:
                        break;
                    case 23:
                        break;
                    case 24:
                        break;
                    case 25:
                        break;
                    case 26:
                        break;
                    case 27:
                        break;
                    
                    // *** TOTAL DE GOLS DO VISITANTE ***

                    case 28: //Fora - Acima 1.5
                        $totalGolsVisitante[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 29: //Fora - Acima 2.5
                        $totalGolsVisitante[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 30: //Fora - Acima 3.5
                        $totalGolsVisitante[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 31:
                        break;
                    case 32:
                        break;
                    case 33:
                        break;
                    case 34:
                        break;
                    case 35:
                        break;
                    case 36:
                        break;
                    case 37:
                        break;
                    case 38: // Fora - Abaixo 1.5
                        $totalGolsVisitante[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 39: //Fora - Abaixo 2.5
                        $totalGolsVisitante[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 40:
                        break;
                    case 41:
                        break;
                    case 42:
                        break;
                    case 43:
                        break;
                    case 44:
                        break;
                    case 45:
                        break;
                    case 46:
                        break;
                    case 47:
                        break;

                    // *** TOTAL DE GOLS NO JOGO ***

                    case 48: // Jogo - Acima 1.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 49: // Jogo - Acima 2.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 50: // Jogo - Acima 3.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 51: // Jogo - Acima 4.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 52: // Jogo - Acima 5.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 53: // Jogo - Acima 6.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 54:
                        break;
                    case 55:
                        break;
                    case 56:
                        break;
                    case 57: // Jogo - Abaixo 0.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 58: // Jogo - Abaixo 1.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 59: // Jogo - Abaixo 2.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 60: // Jogo - Abaixo 3.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 61: // Jogo - Abaixo 4.5
                        $totalGolsJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    break;
                    case 62:
                        break;
                    case 63:
                        break;
                    case 64:
                        break;
                    case 65:
                        break;
                    case 66:
                        break;
                    case 67:
                        break;
                    case 68:
                        break;

                    // *** AMBAS EQUIPES *** 

                    case 69: // ambas marcam : sim
                        $ambasEquipes[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 70: // ambas marcam : não
                        $ambasEquipes[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 71:
                        break;
                    case 72:
                        break;
                    case 73:
                        break;
                    case 74:
                        break;
                    case 75:
                        break;
                    case 76:
                        break;
                    case 77:
                        break;
                    case 78:
                        break;
                    case 79:
                        break;
                    case 80:
                        break;
                    case 81:
                        break;
                    case 82:
                        break;
                    case 83:
                        break;
                    case 84:
                        break;
                    case 85:
                        break;
                    case 86:
                        break;
                    case 87:
                        break;
                    case 88:
                        break;
                    case 89:
                        break;
                    case 90:
                        break;
                    case 91:
                        break;
                    case 92:
                        break;
                    case 93:
                        break;
                    case 94:
                        break;
                    case 95:
                        break;
                    case 96:
                        break;
                    case 97:
                        break;
                    case 98:
                        break;
                    case 99:
                        break;
                    case 100:
                        break;

                    // *** VENCEDOR DO ENCONTRO ***

                    case 101: // 1º Tempo - Casa
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 102: // 1º Tempo - Empate
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 103: // 1º Tempo - Fora
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 104: // 2º Tempo - Casa
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 105: // 2º Tempo - Empate
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 106: // 2º Tempo - Fora
                        $vencedorEncontroPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                        
                    
                    // *** TOTAL DE GOLS NO PRIMEIRO TEMPO ***
                    
                    case 107: // 1º Tempo - Jogo Acima 0.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 108: // 1º Tempo - Jogo Acima 1.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 109: // 1º Tempo - Jogo Acima 2.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 110: // 1º Tempo - Jogo Acima 3.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 111:
                        break;
                    case 112:
                        break;
                    case 113:
                        break;
                    case 114:
                        break;
                    case 115:
                        break;
                    case 116:
                        break;
                    case 117: // 1º Tempo - Jogo Abaixo 0.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 118: // 1º Tempo - Jogo Abaixo 1.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 119: // 1º Tempo - Jogo Abaixo 2.5
                        $totalGolsPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 120:
                        break;
                    case 121:
                        break;
                    case 122:
                        break;
                    case 123:
                        break;
                    case 124:
                        break;
                    case 125:
                        break;
                    case 126:
                        break;

                    // *** TOTAL DE JOGOS NO SEGUNDO TEMPO ***

                    case 127: // 2º Tempo - Jogo Acima 0.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 128: // 2º Tempo - Jogo Acima 1.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 129: // 2º Tempo - Jogo Acima 2.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 130: // 2º Tempo - Jogo Acima 3.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;                   
                    case 131: // 2º Tempo - Jogo Acima 3.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 132:
                        break;
                    case 133:
                        break;
                    case 134:
                        break;
                    case 135:
                        break;
                    case 136:
                        break;
                    case 137: // 2º Tempo - Jogo Abaixo 0.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 138: // 2º Tempo - Jogo Abaixo 1.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 139: // 2º Tempo - Jogo Abaixo 2.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 140: // 2º Tempo - Jogo Abaixo 3.5
                        $totalGolsSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 141:
                        break;
                    case 142:
                        break;
                    case 143:
                        break;
                    case 144:
                        break;
                    case 145:
                        break;
                    case 146:
                        break;

                    // *** TOTAL DE GOLS PAR OU IMPAR ***

                    case 147: // Total de Gols Ímpar
                        $totalGolsParImpar[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 148: // Total de Gols Par
                        $totalGolsParImpar[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // *** TOTAL DE GOLS PAR OU IMPAR 1º TEMPO ***

                    case 149: // 1º tempo - Total de Gols Ímpar 
                        $totalGolsParImparPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 150: // 1º tempo - Total de Gols Par
                        $totalGolsParImparPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break; 
                        
                    // *** TOTAL DE GOLS PAR OU IMPAR 2º TEMPO ***

                    case 151: // 2º tempo - Total de Gols Ímpar 
                        $totalGolsParImparSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 152: // 2º tempo - Total de Gols Par
                        $totalGolsParImparSegTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 153:
                        break;
                    case 154:
                        break;
                    case 155:
                        break;
                    case 156:
                        break;
                    case 157:
                        break;
                    case 158:
                        break;
                    case 159:
                        break;
                    case 160:
                        break;
                    case 161:
                        break;
                    case 162:
                        break;
                    case 163:
                        break;
                    case 164:
                        break;
                    case 165:
                        break;
                    case 166:
                        break;
                    case 167:
                        break;
                    case 168:
                        break;
                    case 169:
                        break;
                    case 170:
                        break;
                    
                    // *** DUPLA CHANCE 1º TEMPO ***

                    case 171: // 1º Tempo - Casa ou Fora 
                        $duplaChancePriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 172: // 1º Tempo - Casa ou Empate
                        $duplaChancePriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 173: // 1º Tempo - Fora ou Empate
                        $duplaChancePriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 174:
                        break;
                    case 175:
                        break;
                    case 176:
                        break;
                    
                    // *** INTERVALO / FINAL DE JOGO ***

                    case 177: // Intervalo | Final (Casa | Casa)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 178: // Intervalo | Final (Casa | Empate)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 179: // Intervalo | Final (Casa | Fora)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 180: // Intervalo | Final (Empate | Casa)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 181: // Intervalo | Final (Empate | Empate)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 182: // Intervalo | Final (Empate | Fora)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 183: // Intervalo | Final (Fora | Casa)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 184: // Intervalo | Final (Fora | Empate)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 185: // Intervalo | Final (Fora | Fora)
                        $intervFinalJogo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  

                    // *** EMPATE ANULA A APOSTA ***

                    case 186: // Empate não tem aposta - Casa
                        $empAnulaAposta[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 187: // Empate não tem aposta - Fora
                        $empAnulaAposta[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  
                    case 188:
                        break;
                    case 189:
                        break;
                    case 190:
                        break;
                    case 191:
                        break;

                        // *** AMBAS AS EQUIPES 1º TEMPO ***

                    case 192: // 1º Tempo - Ambas Marcam - sim
                        $ambasEquipesPriTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 193: // 1º Tempo - Ambas Marcam - não
                        $ambasEquipesPriTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;  

                    // *** AMBAS AS EQUIPES 2º TEMPO ***

                    case 194: // 2º Tempo - Ambas Marcam - sim
                        $ambasEquipesSegTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 195: // 2º Tempo - Ambas Marcam - não
                        $ambasEquipesSegTemp[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                        
                    // *** HANDICAP DE GOL

                    case 250: // Handicap Casa (-1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 251: // Handicap Casa (-2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 252: // Handicap Casa (-3)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 255: // Handicap Casa (+1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 280: // Handicap Empate (-1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 281: // Handicap Empate (-2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 285: // Handicap Empate (+1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 286: // Handicap Empate (+2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 287: // Handicap Empate (+3)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 310: // Handicap Fora (-1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 311: // Handicap Fora (-2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 315: // Handicap Fora (+1)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 316: // Handicap Fora (+2)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 317: // Handicap Fora (+3)
                        $handicapGol[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // // *** HANDICAP ASIATICO

                    // case 380: // Handicap Asiático Casa (-0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 381: // Handicap Asiático Casa (-1.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 382: // Handicap Asiático Casa (-2.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 386: // Handicap Asiático Casa (+0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 416: // Handicap Asiático Fora (-0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 422: // Handicap Asiático Fora (+0.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 423: // Handicap Asiático Fora (+1.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                    // case 424: // Handicap Asiático Fora (+2.5)
                    //     $handicapAsiatico = $handicapAsiatico . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);  
                    //     break;
                       
                    // *** RESULTADO EXATO ***

                    case 500: // Resultado Exato (0 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 501: // Resultado Exato (0 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 502: // Resultado Exato (0 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 503: // Resultado Exato (0 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 504: // Resultado Exato (0 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 505: // Resultado Exato (0 : 5)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 506: // Resultado Exato (0 : 6)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 507: // Resultado Exato (0 : 7)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 508: // Resultado Exato (0 : 8)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 509: // Resultado Exato (0 : 9)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 510: // Resultado Exato (1 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 511: // Resultado Exato (1 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 512: // Resultado Exato (1 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 513: // Resultado Exato (1 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 514: // Resultado Exato (1 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 515: // Resultado Exato (1 : 5)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 516: // Resultado Exato (1 : 6)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 520: // Resultado Exato (2 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 521: // Resultado Exato (2 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 522: // Resultado Exato (2 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 523: // Resultado Exato (2 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 524: // Resultado Exato (2 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 525: // Resultado Exato (2 : 5)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 526: // Resultado Exato (2 : 6)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 530: // Resultado Exato (3 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 531: // Resultado Exato (3 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 532: // Resultado Exato (3 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 533: // Resultado Exato (3 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 534: // Resultado Exato (3 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 535: // Resultado Exato (3 : 5)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 540: // Resultado Exato (4 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 541: // Resultado Exato (4 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 542: // Resultado Exato (4 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 543: // Resultado Exato (4 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 544: // Resultado Exato (4 : 4)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 550: // Resultado Exato (5 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 551: // Resultado Exato (5 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 552: // Resultado Exato (5 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 553: // Resultado Exato (5 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 560: // Resultado Exato (6 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 561: // Resultado Exato (6 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 562: // Resultado Exato (6 : 2)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 563: //Resultado Exato (6 : 3)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 570: // Resultado Exato (7 : 0)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 571: // Resultado Exato (7 : 1)
                        $resultadoExato[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                        
                    // *** RESULTADO EXATO 1º TEMPO ***


                    case 600: // 1º Tempo - Resultado Exato (0 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 601: // 1º Tempo - Resultado Exato (0 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 602: // 1º Tempo - Resultado Exato (0 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 603: // 1º Tempo - Resultado Exato (0 : 3)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 610: // 1º Tempo - Resultado Exato (1 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 611: // 1º Tempo - Resultado Exato (1 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 612: // 1º Tempo - Resultado Exato (1 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 613: // 1º Tempo - Resultado Exato (1 : 3)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 620: // 1º Tempo - Resultado Exato (2 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 621: // 1º Tempo - Resultado Exato (2 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 622: // 1º Tempo - Resultado Exato (2 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 623: // 1º Tempo - Resultado Exato (2 : 3)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 630: // 1º Tempo - Resultado Exato (3 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 631: // 1º Tempo - Resultado Exato (3 : 1)"
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 632: // 1º Tempo - Resultado Exato (3 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 640: // 1º Tempo - Resultado Exato (4 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 641: // 1º Tempo - Resultado Exato (4 : 1)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 642: // 1º Tempo - Resultado Exato (4 : 2)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 650: // 1º Tempo - Resultado Exato (5 : 0)
                        $resultadoExatoPrimTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    break;

                    // Ambas com Resultado
                    case 827: // Ambas marcam: nao / Casa
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 824: // Ambas marcam: sim / Casa
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 828: // Ambas marcam: nao / Empate
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 825: // Ambas marcam: sim / Empate
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 829: // Ambas marcam: nao / Fora
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;    
                    case 826: // Ambas marcam: sim / Fora
                        $ambasComResultado[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    //

                    // ***TOTAL EXATO DE GOLS***

                    case 101125: // Número de gols exato (5)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101126: // Número de gols exato (4)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101127: // Número de gols exato (3)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101128: // Número de gols exato (2)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101129: // Número de gols exato (1)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101130: // Número de gols exato (0)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101138: // Número de gols exato (6)
                        $totalExatoGols[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // ***TOTAL EXATO DE GOLS 1º TEMPO ***
                    
                    case 101032: // 1º Tempo número de gols exato (0)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101033: // 1º Tempo número de gols exato (1)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101038: // 1º Tempo número de gols exato (2)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101065: // 1º Tempo número de gols exato (3)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
                    case 101149: // 1º Tempo número de gols exato (4)
                        $totalExatoGolsPriTempo[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    // **** Resultado / Total de Gols ****
                    case 101119: // Resultado casa / Total de gols acima de 2.5

                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        //$resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101120: // Resultado casa / Total de gols abaixo de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101136: // Resultado fora / Total de gols acima de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101135: // Resultado fora / Total de gols abaixo de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101131: // Resultado empate / Total de gols acima de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;

                    case 101137: //Resultado empate / Total de gols abaixo de 2.5
                        $resultado_total_gols_2_5[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                    //     $resultado_total_gols_2_5 = $resultado_total_gols_2_5 . $this->returnHtml( $arr->jog_odd_id , $arr->descricao, $arr->taxa , $codeJogo);
                        break;
                        
                        /********** cantos escanteios *******/
                    case 101116: // Cantos exatamente (3)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101002: // Cantos exatamente (4)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101005: // Cantos exatamente (5)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101008: // Cantos exatamente (6)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101011: // Cantos exatamente (7)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101037: // Cantos exatamente (8)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101040: // Cantos exatamente (9)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101007: // Cantos acima de (6)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101010: // Cantos acima de (7)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101035: // Cantos acima de (8)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101039: // Cantos acima de (9)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101115: // Cantos abaixo de (3)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101001: // Cantos abaixo de (4)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101006: // Cantos abaixo de (5)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101009: // Cantos abaixo de (6)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101012: // Cantos abaixo de (7)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101038: // Cantos abaixo de (8)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;

                    case 101041: // Cantos abaixo de (9)
                        $escanteiosCantos[$arr->descricao] = [
                            'jog_odd_id' => $arr->jog_odd_id,
                            'descricao' => $arr->descricao,
                            'taxa' => ( ( $arr->taxa * env('AUMENTAR_ODD_DD_PORCENTAGEM') ) + $arr->taxa ),
                            'codeJogo' => $codeJogo,
                        ];
                        break;
            }
            
        }

        // montar o menu com o cabeçalho
        $menu = '<table border=\'1\' class=\'table table-striped\'>';
        $result = '';

        if ( $vencedorEncontro != [] )
            $menu = $menu . $this->orderListOdds($vencedorEncontro , 'Vencedor do Encontro');
        if ( $duplaChance != [] )
            $menu = $menu . $this->orderListOdds($duplaChance , 'Dupla Chance');
        if ( $totalGolsCasa != [] )
            $menu = $menu . $this->orderListOdds($totalGolsCasa , 'Total de Gols da Casa');
        if ( $totalGolsVisitante != [] )
            $menu = $menu . $this->orderListOdds($totalGolsVisitante , 'Total de Gols do Visitante');
        if ( $totalGolsJogo != [] )
            $menu = $menu . $this->orderListOdds($totalGolsJogo , 'Total de Gols no Jogo');
        if ( $ambasEquipes != [] )
            $menu = $menu . $this->orderListOdds($ambasEquipes , 'Ambas as Equipes');
        if ( $totalGolsParImpar != [] )
            $menu = $menu . $this->orderListOdds($totalGolsParImpar , 'Total de Gols Par ou Ímpar');
        if ( $retultadoExato != [] )
            $menu = $menu . $this->orderListOdds($retultadoExato , 'Resultado Exato');
        if ( $handicapGol != [] )
            $menu = $menu . $this->orderListOdds($handicapGol , 'Handicap de Gol');
        if ( $handicapAsiatico != [] )
            $menu = $menu . $this->orderListOdds($handicapAsiatico , 'Handicap Asiático');
        if ( $intervFinalJogo != [] )
            $menu = $menu . $this->orderListOdds($intervFinalJogo , 'Intervalo / Final de Jogo');
        if ( $empAnulaAposta != [] )
            $menu = $menu . $this->orderListOdds($empAnulaAposta , 'Empate Anula a Aposta');
        if ( $resultadoTotalGols != [] )
            $menu = $menu . $this->orderListOdds($resultadoTotalGols , 'Resultado / Total de Gols');
        if ( $ambasComResultado != [] )
            $menu = $menu . $this->orderListOdds($ambasComResultado , 'Ambas com Resultado');
        if ( $ambasMarcamAmbosTempos != [] )
            $menu = $menu . $this->orderListOdds($ambasMarcamAmbosTempos , 'Ambas marcam em ambos os tempos');
        if ( $escanteiosCantos != [] )
            $menu = $menu . $this->orderListOdds($escanteiosCantos , 'Escanteios');
        if ( $metadeComMaisGols != [] )
            $menu = $menu . $this->orderListOdds($metadeComMaisGols , 'Metade com mais gols');
        if ( $totalGolsAmbasMarcam != [] )
            $menu = $menu . $this->orderListOdds($totalGolsAmbasMarcam , 'Total de gols com ambas marcam');
        if ( $totalExatoGols != [] )
            $menu = $menu . $this->orderListOdds($totalExatoGols , 'Total exato de gols');
        if ( $totalGolsPrimTempo != [] )
            $menu = $menu . $this->orderListOdds($totalGolsPrimTempo , 'Total de Gols no Jogo');
        if ( $ambasEquipesPriTemp != [])
            $menu = $menu . $this->orderListOdds($ambasEquipesPriTemp , 'Ambas as Equipes');
        if ( $resultadoExato != [] )
            $menu = $menu . $this->orderListOdds($resultadoExato , 'Resultado Exato');
        if ( $resultadoExatoPrimTempo  != [] )
            $menu = $menu . $this->orderListOdds($resultadoExatoPrimTempo , 'Resultado Exato 1º Tempo');
        if ( $ambasComResultadoPriTempo != [] )
            $menu = $menu . $this->orderListOdds($ambasComResultadoPriTempo , 'Ambas com Resultado');
        if ( $totalExatoGolsPriTempo != [] )
            $menu = $menu . $this->orderListOdds($totalExatoGolsPriTempo , 'Total exato de gols');
        if ( $vencedorEncontroPriTempo != [] )
            $menu = $menu . $this->orderListOdds($vencedorEncontroPriTempo , 'Vencedor do Encontro');
        if ( $totalGolsSegTempo != [] )
            $menu = $menu . $this->orderListOdds($totalGolsSegTempo , 'Total de Gols no Jogo');
            if ( $ambasEquipesSegTemp != [] )    
            $menu = $menu . $this->orderListOdds($ambasEquipesSegTemp , 'Ambas as Equipes');
        if ( $totalGolsParImparPriTempo != [] )    
            $menu = $menu . $this->orderListOdds($totalGolsParImparPriTempo , 'Total de Gols Par ou Ímpar');
        if ( $totalGolsParImparSegTempo != [] )    
            $menu = $menu . $this->orderListOdds($totalGolsParImparSegTempo , 'Total de Gols Par ou Ímpar');
        if ( $totalExatoGolsSegTempo != [] )    
            $menu = $menu . $this->orderListOdds($totalExatoGolsSegTempo , 'Total exato de gols');
        if ( $duplaChancePriTempo != [] )    
            $menu = $menu . $this->orderListOdds($duplaChancePriTempo , 'Dupla Chance');
        if ( $resultado_total_gols_2_5 != [] ) { 
            $menu = $menu . $this->orderListOdds($resultado_total_gols_2_5 , 'Resultado / Total de Gols');   
        }
            

        return ($menu . '</table>');
        
    }

    // retornar html com o modal de odds preenchido
    private function returnHtml( $idOdd , $descricaoOdd, $taxa , $codeJogo) {

        return '<tr><td><a href="javascript:checkDetailOdds('.$idOdd.', '.$codeJogo.');">'.$descricaoOdd.'</a></td>' .
                '<td><a href="javascript:checkDetailOdds('.$idOdd.', '.$codeJogo.');">' . $this->maskMoney($taxa) . '</a></td>' .
                '</tr>';  
    }

    // retorna o titulo da tabela
    private function returnTitleTable($value) {
        return '<tr><td colspan="2" align="center" class="title-odd-atual"><font color="white">' . $value . '</font></td></tr>';
    }

    private function orderListOdds($array , $tittle) {
        $result = '';
        ksort($array);
        foreach( $array as $key => $value ) {
            $result = $result . $this->returnHtml( $value['jog_odd_id'] , $key , $value['taxa'] , $value['codeJogo']);
        }
        return $this->returnTitleTable($tittle) . $result;
    }

    // formartar valor dinheiro
    private function maskMoney($value){
        $val = explode("." , str_replace(",", ".", $value) );
        
        if(!isset($val[1]))
            return $val[0].',00';
        else if(strlen($val[1]) == 1)
            return $val[0].','.$val[1].'0';
        else if(strlen($val[1]) >= 2)
            return $val[0] . ',' . substr( $val[1], 0, 2 );
        else
            return $value;
        
    }

}
