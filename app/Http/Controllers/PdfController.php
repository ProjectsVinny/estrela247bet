<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    //
    public function getGerarPDF()
    {
        $pdf = PDF::loadView('bilhete_pdf');
        return $pdf->setPaper('a4')->stram('pré-bilhete');
    }
}
