<?php

namespace App\Http\Controllers;
use App\Models\BilheteModel;
use App\Models\DetalheBilheteModel;
use App\Models\UsuarioModel;
use Illuminate\Http\Request;
use App\Http\Controllers\ConsultarSporteBetNet;
use App\Http\Controllers\MapeamentoOddsController;
use Illuminate\Support\Facades\Auth;

class PreBilhete extends Controller
{

    private $consultarSportBet;

    // 
    public function validarOddSelecionada( $idOdd, $idJogo ) 
    {
        $data = $request->session()->get('dadosTratados');
    }

    public function getOddDetailTitle($code , Request $request)
    {
        // buscar dados no array principal
        $dadosTratados = $request->session()->get('dadosTratados');
        foreach($dadosTratados as $data) {
            foreach ($data as $d){
                if($d->camp_jog_id == $code)
                    return($d->camp_nome);
            }
        }
    }

    public function getOddDetail($codeJogo , Request $request)
    {
        $consultarSportBet = new ConsultarSporteBetNet();
        $mapeamentoOdds = new MapeamentoOddsController();

        // buscar dados no array principal
        
        $dadosTratados = $request->session()->get('dadosTratados');
        foreach($dadosTratados as $data) {
            foreach ($data as $d){
                if($d->camp_jog_id == $codeJogo){
                    echo $mapeamentoOdds->returnMapeamentoOdds( $consultarSportBet->getOddsDetail($codeJogo) , $codeJogo );
                    //$this->montarTableModal($consultarSportBet->getOddsDetail($codeJogo), $codeJogo);
                }
            }
        }
    }

  public function getCheckOdds($codOdd , $codeJogo , $valorAposta , $tipoOdd = null , Request $request)
  {
        $data = [];
        $dadosBilheteAtual = [];
        // verifica se já exite algum jogo na sessão
        if ($request->session()->has('bilheteAtual')) {
            // verificar se não é duplicado, caso seja remove o que foi clicado
            $dadosBilheteAtual = $this->verificarJogoDuplicado( $codOdd , $codeJogo , $request->session()->get('bilheteAtual') );
        }

        $dadosTratados = $request->session()->get('dadosTratados');
        
        if($tipoOdd == '1') {
            $consultarSportBet = new ConsultarSporteBetNet();
            $detailOdd = $consultarSportBet->getOddsDetail($codeJogo);
        }
        if( $dadosTratados != null ) {

            foreach($dadosTratados as $data) {
                
                foreach ($data as $d){

                    if($d->camp_jog_id == $codeJogo){

                        if($tipoOdd == 1) {

                            foreach($detailOdd as $do) {

                                if(json_decode($do->Value)->jog_odd_id == $codOdd) {
                                
                                    $dadosBilheteAtual[$codeJogo]['code_jogo']      = $codeJogo;
                                    $dadosBilheteAtual[$codeJogo]['cod_odd']        = json_decode($do->Value)->jog_odd_id;
                                    $dadosBilheteAtual[$codeJogo]['valor_odd']      = (floatval(json_decode($do->Value)->taxa) + (floatval(json_decode($do->Value)->taxa) * env('AUMENTAR_ODD_DD_PORCENTAGEM')));
                                    $dadosBilheteAtual[$codeJogo]['casa_time']      = $d->casa_time;
                                    $dadosBilheteAtual[$codeJogo]['visit_time']     = $d->visit_time;
                                    $dadosBilheteAtual[$codeJogo]['dt_hr_ini']      = $d->dt_hr_ini;
                                    $dadosBilheteAtual[$codeJogo]['odd_descricao']  = json_decode($do->Value)->descricao; 
                                }
                            }

                        }else {

                            foreach($d->Odds as $odd){
                                
                                if($odd->jog_odd_id == $codOdd) {
                                    $dadosBilheteAtual[$codeJogo]['code_jogo']      = $codeJogo;
                                    $dadosBilheteAtual[$codeJogo]['cod_odd']        = $odd->jog_odd_id;
                                    $dadosBilheteAtual[$codeJogo]['valor_odd']      = ( floatval($odd->taxa) + (floatval($odd->taxa) * env('AUMENTAR_ODD_PORCENTAGEM')) );
                                    $dadosBilheteAtual[$codeJogo]['casa_time']      = $d->casa_time;
                                    $dadosBilheteAtual[$codeJogo]['visit_time']     = $d->visit_time;
                                    $dadosBilheteAtual[$codeJogo]['dt_hr_ini']      = $d->dt_hr_ini;
                                    $dadosBilheteAtual[$codeJogo]['odd_descricao']  = $odd->descricao ; 
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($request->session()->has('bilheteAtual')) {
            if($tipoOdd == 2) {
                $dadosBilheteAtual = $request->session()->get('bilheteAtual');
                unset($dadosBilheteAtual[$codeJogo]);
            }
        }

        $request->session()->put('bilheteAtual', $dadosBilheteAtual);
        echo( $this->montarBodyBilhete($dadosBilheteAtual) . $this->getResultFooterTable($valorAposta, $request) );
    }
    
    // montar tabela corpo bilhete 
    private function montarBodyBilhete($dadosBilheteAtual) 
    {
        $html = '<table class="table table-striped">';
        foreach($dadosBilheteAtual as $dados) 
        {
            $html = $html . '<tr>';
            $html = $html . '<td>'.$dados['casa_time'].' x '.$dados['visit_time'].'<br>' . date('d/m/Y H:i:s ', strtotime($dados['dt_hr_ini'] ) ) .'<br>Venc.: '. $dados['odd_descricao'] .'</td>';
            $html = $html . '<td><a href="javascript:deleteGame( ' . $dados['cod_odd'] . ' , ' . $dados['code_jogo'] .')">excluir</a></td>';
            $html = $html . '</tr>';
        }
        return $html . '</body>';
    }
    // verifica jogo duplicado
    private function verificarJogoDuplicado($codOdd , $codJogo, $dadosBilheteAtual )
    {
        if( isset( $dadosBilheteAtual[$codJogo] ) )
        {
            //remover odd da lista
            if ( $dadosBilheteAtual[$codJogo]['cod_odd'] == $codOdd ){
                // remove o jogo da tela de bilhete
                unset($dadosBilheteAtual[$codJogo]);
            }else {
                // atualiza odd
                return $dadosBilheteAtual;
            }
        }
        return $dadosBilheteAtual ;
    }

    // salvar pre bilhete
    public function getSavePreBilhete($valorAposta , $nome_cliente = null, Request $request) {
        if($request->session()->has('bilheteAtual')){
            $bilheteAtual =  $request->session()->get('bilheteAtual');
            $bilheteModel = new BilheteModel;
            $detalhebilheteModel = new DetalheBilheteModel;
            $pdf = new PdfController;

            // refazer os calculos das odds
            $valorCotacao = 0;
            $valorPremio = 0;
            $quantidadeJogos = 0;

            foreach($bilheteAtual as $bilhete) {
                
                if( date('Y-m-d H:i:s', strtotime( $bilhete['dt_hr_ini'] ) )  < date('Y-m-d H:i:s', strtotime( now () ) ) ) {
                    return 'jogo_iniciado';
                }
                if($valorCotacao == 0){
                    $valorCotacao = $bilhete['valor_odd'];
                }else{
                    $valorCotacao = $valorCotacao * $bilhete['valor_odd'];
                }
                $quantidadeJogos++;
            }

            

            if( Auth::user() !== null ){
                
                if($quantidadeJogos == 1 ){
                   
                    if( Auth::user()->odd_minima != null ) {
                        if( intval( Auth::user()->odd_minima * 100 ) > intval( $valorCotacao * 100 ) ) {
                            return 'odd_minima';
                        }
                    }

                }else if($quantidadeJogos > 1 ) {

                    if( Auth::user()->odd_minima_2_jogos != null ) {
                        if( intval( Auth::user()->odd_minima_2_jogos * 100 ) > intval( $valorCotacao * 100 ) ) {
                            return 'odd_minima_2_jogos';
                        }
                    }

                } else if($quantidadeJogos < 1 ) {
                    return 'sem_jogos';
                }

            }

            if( (int) $valorAposta < 20 ) {
                $valorFinalPremio = (int) $valorAposta . '000';
            } else {
                $valorFinalPremio = 20000;
            }

            if($valorCotacao * floatval ($valorAposta ) > $valorFinalPremio ){
                $valorPremio = $valorFinalPremio;
            }else{
                $valorPremio = floatval($valorAposta) * $valorCotacao;
            }
            
            if( Auth::check() === true ){

                if( ( floatval ( Auth::user()->saldo_apostas ) - floatval ( $valorAposta) ) < 0 ) {
                    return 'sem_saldo';
                    
                } else {
                    // retirar saldo do usuario                
                    $array['id'] = Auth::user()->id;
                    $array['saldo_apostas'] = ( floatval ( Auth::user()->saldo_apostas ) - floatval ( $valorAposta) ) ;
                    UsuarioModel::getUpdate($array);
                }
            }

            //gravar dados na tabela bilhete
            $idBilhete = $bilheteModel->saveBilhete($this->maskMoney($valorAposta) , $this->maskMoney($valorCotacao) , $this->maskMoney($valorPremio), $nome_cliente);

            foreach($bilheteAtual as $bilhete) {
                // grava dados na tabela detalhe bilhete

                $detalhebilheteModel->saveDetalheBilhete($bilhete , $idBilhete);
            }

            

            //recuperar codigo bilhete para imprimir pre bilhete
            $codValidacaoBilhete = $bilheteModel->getCodValidacaoBilhete($idBilhete);

            $var = 0;
            foreach($codValidacaoBilhete as $b){
                $var = $b->codigo_validacao_bilhete;
            }

            $request->session()->forget('bilheteAtual');
            
            // return codigo bilhete
            echo $var;
        }
    }

    /* limpar bilhete bilhetes preenchidos */
    public function getDeleteticket(Request $request) {
        $request->session()->forget('bilheteAtual');
        echo $this->getResultFooterTable(5 , $request);
    }

    /* montar footer do bilhete */
    public function getResultFooterTable( $valorAposta = 5, Request $request ) 
    {
        // recuperar valores da sessão onde encontra-se os bilhetes
        $qtd_games = 0;
        $valorCotacao;
        if($request->session()->has('bilheteAtual'))
        {
            $dadosBilheteAtual = $request->session()->get('bilheteAtual');
            if($dadosBilheteAtual != false)
                $qtd_games = count($dadosBilheteAtual);
        }
 
        if( $qtd_games > 0 )
        {
            foreach ($dadosBilheteAtual  as $bilhete) 
            {
                
                if ( isset( $valorCotacao ) ) 
                {
                    $valorCotacao = $valorCotacao * floatval ( $bilhete['valor_odd'] );
                } else {
                    $valorCotacao = $bilhete['valor_odd'];
                }
            }
        }
      
        if ( ! isset( $valorCotacao ) ) {
            $valorCotacao = 0;
        }
        
        if( (int) $valorAposta < 20 ) {
            $valorFinalPremio = (int) $valorAposta . '000';
        } else {
            $valorFinalPremio = 20000;
        }


        if($valorCotacao * floatval ($valorAposta ) > $valorFinalPremio ){
            $valorTotal = $valorFinalPremio;
        } else {
            $valorTotal = $valorCotacao * floatval ( $valorAposta );
        }

        if($valorAposta < 0) {
            $html = '<table class="table table-bordered table-striped">';
            $html = $html . '<tr><td colspan="2" class="title-odd-atual"></td></tr>';
            $html = $html . '<tr><td width="60%">N. de jogos:</td>';
            $html = $html . '<td width="40%">0</td>';
            $html = $html . '</tr><tr><td>Cotação:</td><td>0</td></tr>';
            
            if(Auth::check() === true){
                $html = $html . '</tr><tr><td>Cliente:</td><td><input class="inputBilhete" type="text" name="txt_nome_cliente" id="txt_nome_cliente" required></td></tr>';
            }
            $html = $html . '</tr><tr><td>Valor aposta:</td><td> R$:<input class="inputBilhete input_money only-number" id="valor_aposta" type="number" value="5,00" required data-accept-comma="1"></td></tr>';
            $html = $html . '<tr><td>Valor Prêmio:</td><td> 0 </td></tr>';
            
        }else {
            $html = '<table class="table table-bordered table-striped">';
            $html = $html . '<tr><td colspan="2" class="title-odd-atual"></td></tr>';
            $html = $html . '<tr><td width="60%">N. de jogos:</td>';
            $html = $html . '<td width="40%">'.$qtd_games.'</td>';
            $html = $html . '</tr><tr><td>Cotação:</td><td><span class="cotacao_atual">' . $this->maskMoney( $valorCotacao ) . '</span></td></tr>';
            if(Auth::check() === true){
                $html = $html . '</tr><tr><td>Cliente:</td><td><input class="inputBilhete" type="text" name="txt_nome_cliente" id="txt_nome_cliente" required></td></tr>';
            }
            $html = $html . '</tr><tr><td>Valor aposta:</td><td> <input class="inputBilhete input_money only-number" autocomplete="off" onkeyup="attVal()" id="valor_aposta" type="text" value="' . $this->maskMoney( $valorAposta ) . '" required data-accept-comma="1"></td></tr>';
            $html = $html . '<tr><td>Valor Prêmio:</td><td> R$: <span class="premio_aposta">' . $this->maskMoney( $valorTotal ) . '</span></td></tr>';
            
        }

        if(Auth::check()){
            $html = $html . '<tr><td colspan="2" align="center"><a href="javascript:savePreBilhete(0)" class="btn title-odd-atual">Gerar bilhete</a>&nbsp;<a href="javascript:cleanTicket()" class="btn title-odd-atual">Limpar</a></td></tr>';
        } else {
            $html = $html . '<tr><td colspan="2" align="center"><a href="javascript:savePreBilhete(1)" class="btn title-odd-atual">Pré-bilhete</a>&nbsp;<a href="javascript:cleanTicket()" class="btn title-odd-atual">Limpar</a></td></tr>';
        }
        return $html . '</table>';
    }
    // formartar valor dinheiro
    private function maskMoney($value){
        $val = explode("." , str_replace(",", ".", $value) );
        
        if(!isset($val[1]))
            return $val[0].',00';
        else if(strlen($val[1]) == 1)
            return $val[0].','.$val[1].'0';
        else if(strlen($val[1]) >= 2)
            return $val[0] . ',' . substr( $val[1], 0, 2 );
        else
            return $value;
        
    }
}

