<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TabelasController extends Controller
{
    //
    public function get() {

        if( Auth::check() === true ) {
            return view('tabelas');
        }
    }
}
