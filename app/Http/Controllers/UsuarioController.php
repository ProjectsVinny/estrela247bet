<?php

namespace App\Http\Controllers;

use App\Models\PerfilModel;
use App\Models\UsuarioModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsuarioController extends Controller
{

    public function getSalvarEditarUsuario(Request $request)
    {
        if( Auth::check() === true ) { 
            $request->validate([
                'name'      => 'required',
                'login'     => 'required',
                'id_perfil' => 'required',
            ]);

            $array = [
                'id'        => $request->id_user,
                'name'      => $request->name,
                'login'     => $request->login,
                'id_perfil' => $request->id_perfil, 
                'created_at'=> now(),
                'saldo_apostas' => str_replace(' ', '', ( str_replace (',' , '. ', str_replace ('.' , '' , ($request->saldo_aposta < 1) ? 0 : $request->saldo_aposta ) ) ) ),
            ];
            
            if ( UsuarioModel::getUpdate( $array ) ) {
                
                return view( 'admin.listar_usuario' , [
                    'message'   => 'Usuário editado com sucesso.',
                    'dados'     => UsuarioModel::getStore()
                ]);

            } else {

                return view( 'admin.listar_usuario' , [
                    'messageErro'   => 'Erro ao Editar Usuário.',
                    'dados'         => UsuarioModel::getStore()
                ]);

            }
            
        }

        return redirect("admin/logout");
    }

    public function getEditarUsuario($value) {
        if( Auth::check() === true ) { 
            return view('admin.cadastro' , [
                'dados'  => UsuarioModel::getStore($value , true ),
                'perfil' => PerfilModel::getStore()
            ]);
        }
        
        return redirect("admin/logout");
    }

    public function getTelaCadastro () 
    {
        if( Auth::check() === true ) {  
            return view('admin.cadastro' , [
                    'perfil' => PerfilModel::getStore()
                ]);
        }
        
        return redirect("admin/logout");
    }

    public function getDeleteUsuario($value) 
    {
        if( Auth::check() === true ) {  

            if( UsuarioModel::getDelete($value) ) {
                return view( 'admin.listar_usuario' , [
                    'message'   => 'Usuário excluido com sucesso.',
                    'perfil'    => PerfilModel::getStore()
                ]);
                
            }else{
                return view( 'admin.listar_usuario' , [
                    'messageErro'   => "Erro ao excluir usuário, tente novamente mais tarde",
                    'data'        => PerfilModel::getStore()
                ]);
            }
        }

        return redirect("admin/logout");
    }

    public function getListaUsuarios() 
    {
        if( Auth::check() === true ) {  
            return view('admin.listar_usuario' , [
                    'dados' => UsuarioModel::getStore()
                ]);
        }
        
        return redirect("admin/logout");
    }

    public function getCadastroUsuario(Request $request)
    {
        if( Auth::check() === true ) {
            $request->validate([
                'name'      => 'required',
                'login'     => 'required',
                'id_perfil' => 'required',
            ]); 

            $array= [
                'name'          => $request->name,
                'login'         => $request->login,
                'id_perfil'     => $request->id_perfil, 
                'password'      => bcrypt('123456'),
                'created_at'    => now(),
                'conta_ativa'   => true,
                'saldo_apostas' => str_replace(' ', '', ( str_replace (',' , '. ', str_replace ('.' , '' , ($request->saldo_aposta < 1) ? 0 : $request->saldo_aposta ) ) ) ),
            ];

            if(UsuarioModel::getCadastro($array)) {
                return view( 'admin/cadastro' , [
                    'message'   => 'Usuario cadastrado com Sucesso!! <br> Foi gerada uma senha provisória <b>123456</b>, quando efetuar o primeiro login favor solicitar que seja alterado a senha.',
                    'dados'     => UsuarioModel::getStore(),
                ]);
                
            }else{
                return view( 'admin/cadastro' , [
                    'messageErro'   => "Erro ao cadastrar usuário, tente novamente mais tarde",
                    'dados'         => UsuarioModel::getStore(),
                ]);
            }
        }

        return redirect("admin/logout");
    }

    public function getSaldo()
    {
        if( Auth::check() === true && Auth::user()->id_perfil == 1) {
            return view( 'admin/saldo' , [
                'messageErro'   => "Erro ao cadastrar usuário, tente novamente mais tarde",
                'usuarios'         => UsuarioModel::getStore(),
            ]);
        } else {
            return redirect("admin/logout");
        }
    }

    public function salvarSaldo(Request $request)
    {

        if( Auth::check() === true && Auth::user()->id_perfil == 1) {

            if(isset($request->saldo_usuario)){

                $array['id']            = $request->usuario;
                $array['saldo_apostas'] = $request->saldo_usuario;

                if(UsuarioModel::getUpdate($array) == 1) {
                    return view( 'admin/saldo' , [
                        'messageSucesso'   => "Saldo alterado com sucesso",
                        'usuarios'         => UsuarioModel::getStore(),
                    ]);
                }
            }
        } else {
            return redirect("admin/logout");
        }
    }

    public function getAlterarSenha()
    {
        if( Auth::check() === true && Auth::user()->id_perfil == 1) {
            return view( 'admin/alterar_senha' , [
                // 'messageErro'   => "Erro ao cadastrar usuário, tente novamente mais tarde",
                'usuarios'         => UsuarioModel::getStore(),
            ]);
        } else {
            return redirect("admin/logout");
        }
    }

    public function salvarSenhaNova(Request $request)
    {
        
        if( Auth::check() === true && Auth::user()->id_perfil == 1) {
            
            if(isset($request->senha_usuario)){
                $array['id']            = $request->usuario;
                $array['password'] = bcrypt($request->senha_usuario);

                if(UsuarioModel::getUpdate($array) == 1) {
                    return view( 'admin/alterar_senha' , [
                        'messageSucesso'   => "Senha alterada com sucesso!!",
                        'usuarios'         => UsuarioModel::getStore(),
                    ]);
                }
            }
        } else {
            return redirect("admin/logout");
        }
    }
}