<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

date_default_timezone_set('America/Sao_Paulo');
class BilheteModel extends Model
{
    public static function store( $codigoBilhete = null , $idBilhete = null , $idUsuario = null , $array = null)  
    {

        if(Auth::check() === true) {
            if(Auth::user()->id != 1){
                if( ! isset($array['data_inicio']) ) {
                    
                    if( date('Y-m-d' , strtotime('+7 days' , strtotime('tuesday last week') ) ) ==  date('Y-m-d' ,strtotime( now() ) ) ){
                        $array['data_inicio'] = date('Y-m-d' , strtotime('-1 days' , strtotime('tuesday last week') ) ) ;
                        $array['data_fim'] = date('Y-m-d' , strtotime( now() ) ) ;
                        
                    } else {
                        
                        $array['data_inicio'] = date('Y-m-d' , strtotime('-1 monday') ) ;
                        $array['data_fim'] = date('Y-m-d' , strtotime( now() ) ) ;
                    }

                } 
            }
        }
        
        $where  = '';
        if($array != null) {
            if( $array['data_inicio'] != '' && $array['data_fim'] != '' ){
                $where = "( created_at >= '".$array['data_inicio']." 00:00:00' AND created_at <= '".$array['data_fim']." 23:59:59')";
            }else if( $array['data_inicio'] != '' ) {
                $where = "( created_at >= '".$array['data_inicio']." 00:00:00')"; 
            }else if($array['data_fim'] != '') {
                $where = "(created_at <= '".$array['data_fim']." 23:59:59')";
            }
        }
        
        if ( $idBilhete != null ) {

            $result = DB::connection()
                            ->table('bilhete')
                            ->where('id_bilhete', '=', $idBilhete)
                            ->where('bilhete_excluido', '=', 0)
                            ->orderBy('created_at', 'DESC')
                            ->get();

        } else if ( $codigoBilhete != null ){

            $result = DB::connection()
                            ->table('bilhete')
                            ->where('codigo_validacao_bilhete', 'like', $codigoBilhete)
                            ->where('bilhete_excluido', '=', 0)
                            ->orderBy('created_at', 'DESC')
                            ->get();

        } else if ( $idUsuario != null ){

            if(Auth::user()->id_perfil === 4) {
                    $result = DB::connection()
                                ->table('bilhete')
                                ->where('bilhete_excluido', '=', 0)
                                ->whereRaw( ($where) ? $where : "created_at like '%'")
                                ->orderBy('created_at', 'desc')
                                ->get();
            }else {
                $result = DB::connection()
                                ->table('bilhete')
                                ->where('id_usuario', '=', $idUsuario)
                                ->where('bilhete_excluido', '=', 0)
                                ->whereRaw( ($where) ? $where : "created_at like '%'")
                                ->orderBy('created_at', 'desc')
                                ->get();
            }

        } else {
            if( $where != '' ) {
                
                $result = DB::connection()
                                ->table('bilhete')
                                ->where('bilhete_excluido', '=', 0)
                                ->whereRaw( ($where) ? $where : "created_at like '%'")
                                ->orderBy('created_at', 'desc')
                                ->get();

            } else {

                $result = DB::connection()
                                ->table('bilhete')
                                ->where('bilhete_excluido', '=', 0)
                                ->orderBy('created_at', 'desc')
                                ->get();
            }
        }

        return ($result);
    }

    public function saveBilhete($valorAposta , $valorCotacao , $valorPremio , $nomeCliente = null) {
        
        if(Auth::check()) {
            $return = DB::connection()->table('bilhete')->insert([
                'valor_aposta_bilhete'          => $valorAposta,
                'valor_cotacao_aposta_bilhete'  => $valorCotacao,
                'valor_premio_aposta_bilhete'   => $valorPremio,
                'codigo_validacao_bilhete'      => $this->gerarCodigoAleatorio(),
                'bilhete_valido'                => true,
                'id_usuario'                    => Auth::user()->id,
                'nome_cliente_bilhete'          =>$nomeCliente,
                'created_at'                    => now()
            ]);
        } else {
            $return = DB::connection()->table('bilhete')->insert([
                'valor_aposta_bilhete' => $valorAposta,
                'valor_cotacao_aposta_bilhete' => $valorCotacao,
                'valor_premio_aposta_bilhete' => $valorPremio,
                'codigo_validacao_bilhete' => $this->gerarCodigoAleatorio(),
                'created_at' => now()
            ]);
        }
        
        if($return) {
            return DB::connection()->table('bilhete')->select('id_bilhete')->orderBy('id_bilhete', 'desc')->limit(1)->get();
        }
        
    }

    public function getCodValidacaoBilhete($idBilhete)
    {
        $var = 0;
        foreach($idBilhete as $b){
            $var = $b->id_bilhete;
        }

        $data = DB::connection()->table('bilhete')->select('codigo_validacao_bilhete')->where('id_bilhete', '=' , $var)->get();

        return $data;
    }

    private function gerarCodigoAleatorio() {

        do{
            $restultado_bytes = random_bytes(4);
            $resultado_final = strtoupper(bin2hex($restultado_bytes)); 

            $data = DB::connection()->table('bilhete')->where('codigo_validacao_bilhete', 'LIKE' , $resultado_final)->get();

        }while(count($data) > 1);

        return $resultado_final;
    }

    public static function getUpdate ( $array )
    {
        if ( array_key_exists ( 'codigo_validacao_bilhete' , $array ) ) {
            DB::connection()->table('bilhete')->where( 'codigo_validacao_bilhete', 'like' , $array['codigo_validacao_bilhete'] ) -> update( $array );
        } else if ( array_key_exists ( 'id_bilhete' , $array ) ) {
            DB::connection()->table('bilhete')->where('id_bilhete', '=' , $array['id_bilhete'] ) -> update($array);
        }
    }

    public static function getTotalValores($value , $id_usuario , $array) {

        $where  = '';
        if( $array['data_inicio'] != '' && $array['data_fim'] != '' ){
            $where = "( created_at >= '".date('Y-m-d', strtotime( $array['data_inicio'] ) ) ." 00:00:00' AND created_at <= '".date('Y-m-d', strtotime($array['data_fim']))." 23:59:59')";
        }else if( $array['data_inicio'] != '' ) {
            $where = "( created_at >= '".date('Y-m-d', strtotime( $array['data_inicio'] ) ) ." 00:00:00')"; 
        }else if($array['data_fim'] != '') {
            $where = "(created_at <= '".date('Y-m-d', strtotime($array['data_fim']))." 23:59:59')";
        }
        
        if($value == 'total_premio'){
            return DB::connection()->table('bilhete')
                            ->where('id_usuario', '=', $id_usuario)
                            ->where('bilhete_premiado', '=', true)
                            ->whereRaw( ($where)? $where : "created_at like '%'")
                            ->sum('valor_premio_aposta_bilhete');
        }
        
        if($value == 'total_bilhete'){
            return DB::connection()->table('bilhete')
                            ->where('id_usuario', '=', $id_usuario)
                            ->whereRaw( ($where)? $where : "created_at like '%'")
                            ->sum('valor_aposta_bilhete');
        }
    }

    public static function getDatas( $value , $id_usuario , $array ) {

        
        if( $value == 'data_inicio' ) {
            if( $array['data_inicio'] != '' )
                return $array['data_inicio'];
            else
                return DB::connection()->table('bilhete')->select('created_at')->orderBy('created_at' , 'asc')->limit(1)->get();
        }

        if( $value == 'data_fim' ) {
            if( $array['data_fim'] != '' )
                return $array['data_fim'];
            else
                return DB::connection()->table('bilhete')->select('created_at')->orderBy('created_at' , 'desc')->limit(1)->get();
        }
    }
}
