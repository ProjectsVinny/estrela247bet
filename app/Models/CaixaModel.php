<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CaixaModel extends Model
{
    use HasFactory;

    public static function store($array) {
        $filtro = null;

        // relatório de porcentagem do usuario
        if($array['tipo_relatorio'] == 1) {
            return DB::connection()->table('users')
                            ->join('bilhete' , 'users.id' , 'bilhete.id_usuario')
                            ->orderBy('users.id')
                            ->get();
        }
    }
}
