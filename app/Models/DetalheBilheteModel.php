<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DetalheBilheteModel extends Model
{
    use HasFactory;

    public function saveDetalheBilhete( $dadosDetalheBilhete , $idBilhete ) 
    {
        $var = 0;
        foreach($idBilhete as $b){
            $var = $b->id_bilhete;
        }
        
        $return = DB::connection()->table('detalhe_bilhete')->insert([
            'id_jogo_bilhete'                  => $dadosDetalheBilhete['code_jogo'],
            'id_odd_bilhete_detalhe'           => $dadosDetalheBilhete['cod_odd'],
            'taxa_odd_bilhete_detalhe'         => $dadosDetalheBilhete['valor_odd'],  
            'time_casa_bilhete_detalhe'        => $dadosDetalheBilhete['casa_time'],
            'time_visitante_bilhete_detalhe'   => $dadosDetalheBilhete['visit_time'],
            'data_hora_jogo_bilhete_detalhe'   => date('Y-m-d H:i:s ', strtotime($dadosDetalheBilhete['dt_hr_ini'])),
            'descricao_odd_bilhete_detalhe'    => $dadosDetalheBilhete['odd_descricao'],
            'id_bilhete'                       => $var
        ]);
    }

    public static function store($codigoBilhete = null)
    {
        if( $codigoBilhete > 0 ){
            return DB::connection()
                        ->table('detalhe_bilhete')
                        ->where('id_bilhete','=', $codigoBilhete)
                        ->where('jogo_excluido_detalhe_bilhete','=', false)
                        ->orderBy('data_hora_jogo_bilhete_detalhe' , 'ASC')
                        ->get() ;
        } else {
            return DB::connection()
                        ->table('detalhe_bilhete')
                        ->where('jogo_excluido_detalhe_bilhete','=', false)
                        ->orderBy('data_hora_jogo_bilhete_detalhe' , 'ASC')
                        ->get();
        }
    }

    public function removeItem($value)
    {
        if($value > 0) {
            DB::connection()
                        ->table('detalhe_bilhete')
                        ->where('id_detalhe_bilhete' , '=' , $value)
                        ->update([
                            'jogo_excluido_detalhe_bilhete' => true
                        ]);
        }
    }

    public static function removeAllItens($value)
    {
        if($value > 0) {
            DB::connection()
                        ->table('detalhe_bilhete')
                        ->where('id_bilhete' , '=' , $value)
                        ->update([
                            'jogo_excluido_detalhe_bilhete' => true
                        ]);
        }
    }

    public static function getUpdate($array)
    {
        if( $array['resultado_jogo'] != null ) {
            DB::connection()->table('detalhe_bilhete')->where('id_detalhe_bilhete' , '=' , $array['id_detalhe_bilhete'])->update([
                'resultado_jogo' => $array['resultado_jogo']
            ]);
        }
    }
}
