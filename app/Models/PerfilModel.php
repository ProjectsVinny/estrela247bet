<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PerfilModel extends Model
{
    use HasFactory;

    public static function getStore() {
        return DB::connection()->table('perfil_usuario')->select([
                    'id_perfil',
                    'tipo_perfil'
                ])->where('perfil_ativo', true)->get(); 
    }
}
