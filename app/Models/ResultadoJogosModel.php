<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ResultadoJogosModel extends Model
{
    use HasFactory;

    public static function store($array = null , $id_bet_api = null , $id = null )
    {
        // buscar jogos com a data menor do que a atual
        if( isset ( $array['data_atual'] ) ) {
            return DB::connection()
                            ->table('resultado_jogos')
                            ->where( 'data_hora_jogo' , '<' , $array['data_atual'] )
                            ->get();
        } else {
            return DB::connection()
                            ->table('resultado_jogos')
                            ->where( 'time_casa' , 'like' , $array['time_casa'] )
                            ->where( 'time_fora' , 'like' , $array['time_fora'] )
                            ->where( 'data_hora_jogo' , $array['data_hora_jogo'] )
                            ->get();    
        }
        

    }

    public static function getUpdate($array)
    {
        return DB::connection()->table('resultado_jogos')
            ->where('id', $array['id'])
            ->where('id_jogo_bet_api' , $array['id_jogo_bet_api'])
            ->update([
                    'gols_casa_1_tempo' => $array['gols_casa_1_tempo'], 
                    'gols_casa_2_tempo' => $array['gols_casa_2_tempo'],
                    'gols_fora_1_tempo' => $array['gols_fora_1_tempo'],
                    'gols_fora_2_tempo' => $array['gols_fora_2_tempo'],
                    'escanteios'        => isset($array['escanteios']) ? $array['escanteios'] : null,
                    'jogo_atualizado'   => true,
            ]);
    }

    public static function getDelete()
    {

    }

    public static function getInsert($array)
    {
        DB::connection()->table('resultado_jogos')->insert($array);
    }
}
