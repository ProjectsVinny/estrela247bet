<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UsuarioModel extends Model
{
    use HasFactory;

    public static function getCadastro($array) 
    {
        return DB::connection()->table('users')->insert($array);
    }

    public static function getStore($filtro = null, $consulta = null) {
        
        if( $filtro != null && $consulta == true) {
            
            return DB::connection()->table('users')
                        ->join('perfil_usuario', 'perfil_usuario.id_perfil', 'users.id_perfil')
                        ->where('id', '=' , $filtro )
                        ->where('conta_ativa', true )
                        ->select('users.id' , 'users.name' , 'users.saldo_apostas' , 'users.login' , 'perfil_usuario.id_perfil' , 'perfil_usuario.tipo_perfil' )
                        ->orderby('users.login' , 'ASC')
                    ->get();

        } else if ( $filtro != null ) {

        } else {
            return DB::connection()->table('users')
                        ->join('perfil_usuario', 'perfil_usuario.id_perfil', 'users.id_perfil')
                        ->where('id', '<>','1')->where('conta_ativa', true )
                        ->select('users.id' , 'users.name' , 'users.login' , 'users.saldo_apostas' , 'perfil_usuario.id_perfil' , 'perfil_usuario.tipo_perfil' )
                        ->orderby('users.login' , 'ASC')
                    ->get();
        }

    }

    public static function getDelete($value)
    {
        return DB::connection()->table('users')->where('id' , '=' , $value)->update([
                    'conta_ativa' => false,
                    'updated_at' => now(),
                    'id_user_excluiu' => Auth::user()->id
                ]);
    }

    public static function getUpdate( $array )
    {
        return DB::connection()->table('users')->where('id' , '=' , $array['id'])->update( $array );
    }
}
