<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PerfilUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create table perfil usuario
        Schema::create('perfil_usuario', function (Blueprint $table) {
            $table->bigIncrements('id_perfil');
            $table->string('tipo_perfil' , 50);
            $table->boolean('perfil_ativo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('perfil_usuario');
    }
}
