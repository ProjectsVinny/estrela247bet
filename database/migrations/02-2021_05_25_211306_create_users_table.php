<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id()->unique();
            $table->string('name');
            $table->string('login');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('conta_ativa')->default(false);
            $table->bigInteger('id_user_excluiu')->nullable();
            $table->decimal('saldo_apostas', $precision = 8, $scale = 2);
            $table->rememberToken();
            $table->timestamps();

            // campo chave estrangeira
            $table->bigInteger('id_perfil')->unsigned();

            // relacionamento
            $table->foreign('id_perfil')->references('id_perfil')->on('perfil_usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
