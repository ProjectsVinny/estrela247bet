<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bilhete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bilhete', function (Blueprint $table) {
            $table->bigIncrements('id_bilhete');
            $table->string('nome_cliente_bilhete')->default('');
            $table->string('valor_aposta_bilhete');
            $table->string('valor_cotacao_aposta_bilhete');
            $table->string('valor_premio_aposta_bilhete');
            $table->boolean('bilhete_valido')->default(false);
            $table->boolean('bilhete_excluido')->default(false);
            $table->boolean('bilhete_premiado')->default(false);
            $table->string('codigo_validacao_bilhete')->unique();
            $table->bigInteger('id_usuario')->nullable();
            $table->bigInteger('id_usuario_cancelou')->nullable();
            $table->timestamps();
                        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bilhete');
    }
}
