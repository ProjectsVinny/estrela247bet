<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetalheBilhete extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        //
        Schema::create('detalhe_bilhete', function (Blueprint $table) {
            $table->bigIncrements('id_detalhe_bilhete');
            $table->bigInteger('id_jogo_bilhete');
            $table->integer('id_odd_bilhete_detalhe');
            $table->decimal('taxa_odd_bilhete_detalhe', $precision = 8, $scale = 2);
            $table->string('time_casa_bilhete_detalhe');
            $table->string('time_visitante_bilhete_detalhe');
            $table->datetime('data_hora_jogo_bilhete_detalhe');
            $table->string('descricao_odd_bilhete_detalhe');
            $table->boolean('jogo_excluido_detalhe_bilhete')->nullable();
            $table->boolean('resultado_jogo')->default(false);
            $table->timestamps();

            // campo chave estrangeira
            $table->bigInteger('id_bilhete')->unsigned();

            // relacionamento
            $table->foreign('id_bilhete')->references('id_bilhete')->on('bilhete');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('detalhe_bilhete');
    }
}
