<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ResultadosJogos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('resultado_jogos', function (Blueprint $table) {
            $table->id();
            $table->string('time_casa');
            $table->string('time_fora');
            $table->integer('gols_casa_1_tempo')->nullable();
            $table->integer('gols_casa_2_tempo')->nullable();
            $table->integer('gols_fora_1_tempo')->nullable();
            $table->integer('gols_fora_2_tempo')->nullable();
            $table->integer('escanteios')->nullable();
            $table->bigInteger('id_jogo_bet_api');
            $table->datetime('data_hora_jogo');
            $table->boolean('jogo_atualizado')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('resultado_jogos');
    }
}