<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class JogosAutorizados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('jogos_autorizados', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_usuario');
            $table->integer('quantidade_jogos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // apagar tabela
         Schema::dropIfExists('jogos_autorizados');
    }
}
