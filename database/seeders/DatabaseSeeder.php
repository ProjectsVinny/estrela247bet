<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        /* atualizar tabelas de perfil */
        DB::connection()->table('perfil_usuario')->insert([
            'id_perfil' => '1',
            'tipo_perfil' => 'Administrador',
            'perfil_ativo' => '1',
            ]);

        DB::connection()->table('perfil_usuario')->insert([
            'id_perfil' => '2',
            'tipo_perfil' => 'Cambista',
            'perfil_ativo' => '1',
            ]);

        DB::connection()->table('perfil_usuario')->insert([
            'id_perfil' => '3',
            'tipo_perfil' => 'Cliente',
            'perfil_ativo' => '0',
            ]);

        /* criar o usuário admin */
        User::create([
            'name' => 'Administrador',
            'login' => 'admin',
            'password' => bcrypt('peres2021'),
            'id_perfil' => '1',
            'saldo_apostas' => 0,
            'conta_ativa' => true,
        ]);
    }
}
