<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class PerfilSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection()->table('perfil_usuario')->insert([
            'id_perfil' => '1',
            'tipo_perfil' => 'Administrador',
            'perfil_ativo' => '1',
            ]);

        DB::connection()->table('perfil_usuario')->insert([
            'id_perfil' => '2',
            'tipo_perfil' => 'Cambista',
            'perfil_ativo' => '1',
            ]);

        DB::connection()->table('perfil_usuario')->insert([
            'id_perfil' => '3',
            'tipo_perfil' => 'Cliente',
            'perfil_ativo' => '1',
            ]);

    }
}
