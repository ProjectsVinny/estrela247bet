<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
class UsuarioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'login' => 'admin',
            'password' => bcrypt('peres2021'),
            'id_perfil' => '1',
            'conta_ativa' => true,
        ]);
    }
}
