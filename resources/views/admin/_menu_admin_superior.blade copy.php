
<div class="row">
    
    <div class="col-md-7">
        <img class="scala_logo" src="{{ asset('Images/logo_estrelabet247.png') }}" alt="">
    </div>
    <div class="col-md-2">
        <br/>
    </div>
    <div class="col-md-2">
        <br/>
    </div>
        <div class="col-md-1">
            <br/>
            <a class="btn btn-danger" href="{{ url('admin/logout')}}">Sair</a>
        </div>
</div>
<div class="row">
    <div class="col-md-12 menu_superior">
        <a href="{{ url('admin') }}" class="btn title-odd-atual">
            Apostas
        </a>

        <a href="{{ url('admin/consultar_bilhete') }}" class="btn title-odd-atual">
            Bilhetes
        </a>

        {{-- <a href="" class="btn title-odd-atual">
            Tabelas
        </a> --}}

        
            <a href="{{ url('admin/caixa') }}" class="btn title-odd-atual">
                Caixa
            </a>

        @if(Auth::user()->id_perfil == 1)
            <a href="{{ url('admin/usuarios') }}" class="btn title-odd-atual">
                Usuários
            </a>
        @endif

        @if(Auth::user()->id_perfil == 1)
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Dropdown
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item" href="#">Something else here</a></li>
            </ul>
            </li>
        @endif
        
        <div align="right" class="title-odd-atual">Saldo: {{ Auth::user()->saldo_apostas }}</div>
        <div class="col-md-12">&nbsp;</div>
            
    </div>
</div>

