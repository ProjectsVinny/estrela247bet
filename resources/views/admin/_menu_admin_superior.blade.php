
<div class="row">
    
    <div class="col-md-7">
        {{-- <img class="scala_logo" src="{{ asset('Images/logo_estrelabet247.png') }}" alt=""> --}}
    </div>

    <div class="col-md-2"></div>

    <div class="col-md-2"></div>

    <div class="col-md-1" align="right">
        <a class="btn btn-danger" href="{{ url('admin/logout')}}">Sair</a>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12 menu_superior">

        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="container-fluid">
                 
                <a href="{{ url('admin') }}" class="btn title-odd-atual">
                    Apostas
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">

                        <li class="nav-item">
                            <a href="{{ url('admin/consultar_bilhete') }}" class="btn title-odd-atual">
                                Bilhetes
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ url('admin/caixa') }}" class="btn title-odd-atual">
                                Caixa
                            </a>
                        </li>
                        @if( Auth::user()->id_perfil === 1 || Auth::user()->id_perfil === 4 )
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle title-odd-atual" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Usuários
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a href="{{ url('admin/usuarios') }}" class="dropdown-item">Usuário</a></li>
                                    
                                    <li><a class="dropdown-item" href="{{ url('admin/saldo') }}">Alterar Saldo</a></li>

                                    <li><a class="dropdown-item" href="{{ url('admin/pass') }}">Alterar Senha</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                    <div align="right" class="title-odd-atual">Saldo: {{ Auth::user()->saldo_apostas }}</div>
                </div>
            </div>
        </nav>
            
    </div>
</div>

