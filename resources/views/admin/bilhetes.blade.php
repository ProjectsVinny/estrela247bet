
@extends('templates.defaultTemplate');

@section('body')
    <div class="container">
        
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">

            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="mb-2"></div>
                <div class="mb-3 centerDiv" >
                    <h2 align="center"><b>Consultar Bilhetes</b></h2>
                    <br><br>

                    @if ( $errors->any() )
                    
                        <div class="alert alert-danger">
                            <ul>
                                <li>O campo codigo pré bilhete é obrigatório!</li>
                            </ul>
                        </div>
                    @endif
                        
                    @if ( isset($message) )
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{ $message }}</li>
                            </ul>
                        </div>
                    @endif

                    @if( ! isset($bilhete ) && ! isset($detalheBilhete) )
                        @if( Auth::user()->id_perfil === 1)
                            <form action="{{ url('admin/consultar_bilhete/do') }}" method="post">

                                <div class="row">
                                    <div class="col-md-12">
                                    <label for="inputEmail4" class="form-label">Usuário</label>
                                    <select name="usuario" id="usuario" class="form-select form-select-sm" >
                                        @if(Auth::user()->id_perfil == 1)
                                            <option selected value=''>Todos Usuários</option>
                                        @endif    
                                        @foreach ($usuarios as $u)
                                            <option value="{{ $u->id }}">{{ $u->login }}</option>                                        
                                        @endforeach
                                    </select>
                                </div>
                                </div>

                                <div class="row">
                                    <div class="col-3">
                                        <label for="inputAddress" class="form-label">De</label>
                                        @if(Auth::user()->id_perfil == 1)
                                            <input type="date" class="form-control" id="data_inicio" name="data_inicio" >
                                        @else
                                            @if( date('Y-m-d' , strtotime('+7 days' , strtotime('tuesday last week') ) ) > date('Y-m-d' , strtotime(now() ) ) )
                                                <input type="date" class="form-control" id="data_inicio" name="data_inicio" min="{{ date('Y-m-d' , strtotime('-1 days' , strtotime('tuesday last week') ) ) }}">
                                            @else
                                                <input type="date" class="form-control" id="data_inicio" name="data_inicio" min="{{ date('Y-m-d' , strtotime('+6 days' , strtotime('tuesday last week') ) ) }}">
                                            @endif
                                        @endif
                                    </div>

                                    <div class="col-3">
                                        <label for="inputAddress" class="form-label">até</label>
                                        @if(auth::user()->id_perfil == 1)
                                            <input type="date" class="form-control" id="data_fim" name="data_fim" >
                                        @else
                                            <input type="date" class="form-control" id="data_fim" name="data_fim" min="{{ date('Y-m-d' , strtotime('+5 days' , strtotime('tuesday last week') ) ) }}">
                                        @endif
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-3">
                                        <label for="codigo_bilhete" class="visually-hidden">Código Bilhete: </label>
                                        <input type="text" class="form-control form-control-sm inputCodBilhete" id="codigo_bilhete" name="codigo_bilhete" placeholder="Codigo do bilhete">
                                    </div>
                                </div>
                                <br/>
                                <div class="col-auto">
                                    <button type="submit" class="btn title-odd-atual mb-3">Pesquisar</button>
                                </div>
                            </form>
                        @else
                            <form class="row g-3" method="POST" action="{{ url('admin/find_bilhete') }}">
                                @csrf
                                <div class="col-auto " align="center">
                                    <label for="codigo_bilhete" class="visually-hidden">Código Bilhete: </label>
                                    <input type="text" class="form-control form-control-sm inputCodBilhete" id="codigo_bilhete" name="codigo_bilhete" placeholder="Codigo do bilhete" required>
                                </div>
                                
                                <div class="col-auto">
                                    <button type="submit" class="btn title-odd-atual mb-3">Pesquisar</button>
                                </div>
                            </form>
                        @endif
                    @endif
                </div>

                @if( ! isset($bilhete ) && ! isset($detalheBilhete) )
                    @if( isset ( $periodo_relatório ) )

                        <div class="alert alert-success">
                            <ul>
                                @if ( isset( $periodo_relatório['usuario'] )  )
                                    <li> Usuário : <b>{{ $periodo_relatório['usuario'] }} </b> </li>    
                                @else
                                    <li> Usuário : <b> Todos </b> </li>    
                                @endif

                                @if ( isset( $periodo_relatório['data_inicio'] ) && isset( $periodo_relatório['data_fim'] ) )
                                    <li>Bilhetes exibidos de <b>{{ date('d/m/Y H:i:s', strtotime( $periodo_relatório['data_inicio']) ) }}</b> até <b>{{ date('d/m/Y H:i:s', strtotime( $periodo_relatório['data_fim'] ) ) }} </b></li>
                                @endif
                            </ul>
                        </div>
        
                    @endif
                @endif

                @if( isset($bilhete ) && isset($detalheBilhete) )
                    <table id="example" class="table table-striped" border="0" cellpadding="1" cellspacing="0" width="100%">
                        <tbody>
                            <tr class="title-odd-atual">
                                <td>Jogos</td>
                                <td>&nbsp;</td>
                                <td align="center">Status</td>
                                <td align="center" class="hit" title="{tip}">Resultado</td>
                            </tr>
                            @php ($cont = 0)
                            @foreach ($detalheBilhete as $db)
                                <tr class="tabelaLinhaImpar4">
                                    <td>
                                        <br><span class="fonte12">
                                            {{ $db->time_casa_bilhete_detalhe }} x {{ $db->time_visitante_bilhete_detalhe }}
                                        </span>
                                        <br><span class="fonte12">Vencedor: {{ $db->descricao_odd_bilhete_detalhe}} - Taxa {{ $db->taxa_odd_bilhete_detalhe }}</span>
                                        <br><span class="fonte12">Data: {{ date('d/m/Y H:i:s', strtotime($db->data_hora_jogo_bilhete_detalhe) ) }}</span>
                                        <br>
                                    </td>
                                    <td>
                                        {{-- @if ($db->resultado_jogo == null)
                                            <button type="button" onclick="javascript: removerItem({{ $db->id_detalhe_bilhete }});" style="width: 120px;">Remover jogo</button>
                                        @endif --}}
                                    </td>
                                    @if ($db->resultado_jogo == '1')
                                        @php ( $cont++ )
                                        <td align="center">Vencedor</td>
                                    @elseif ($db->resultado_jogo == '0'))
                                        <td align="center">Perdedor</td>
                                    @elseif ($db->resultado_jogo == '3'))
                                        <td align="center">Cancelado</td>
                                    @elseif ($db->resultado_jogo == '2'))
                                        <td align="center">Não tem aposta</td>
                                    @else
                                        <td align="center">Aberto</td>
                                    @endif
                                    
                                    <td align="center" class="hit" title="">
                                        resultado
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    @foreach ($bilhete as $b)
                    <form action="{{url('admin/confirm_bilhete')}}" method="POST">
                        <input type="hidden" value="{{ $b->codigo_validacao_bilhete }}" name="validacao_bilhete" id="validacao_bilhete">
                            <table border="0" cellpadding="4" cellspacing="2" width="100%">
                                <tbody>
                                    <tr>
                                        <td align="right">Código:</td>
                                        <td align="left"><b>{{ $b->codigo_validacao_bilhete }}</b></td>
                                        <td align="right"></td>
                                        <td align="left"><b></b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Cliente: </td>
                                        <td align="left">
                                            <b>
                                                @if (! $b->bilhete_valido )
                                                    <input type="text" class="inputNomeCliente" id="nome_cliente" name="nome_cliente" required>
                                                @else
                                                    {{ $b->nome_cliente_bilhete }}
                                                @endif
                                            </b>
                                        </td>
                                        <td align="right">Usuário: </td>
                                        <td align="left">
                                            <b>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Valor da Aposta: </td>
                                        <td align="left"><b>{{ $b->valor_aposta_bilhete }}</b></td>
                                        <td align="right">N. de Jogos: </td>
                                        <td align="left"><b>{{ $detalheBilhete->count() }}</b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Data - Hora</td>
                                        <td align="left"><b> {{ date('d/m/Y H:i:s' , strtotime($b->created_at) ) }}</b></td>
                                        <td align="right">Valor do Prêmio: </td>
                                        <td align="left"><b>  <span class="formatMoney"> {{ $b->valor_premio_aposta_bilhete }} </span> </b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Qtd Acertos:</td>
                                        <td align="left"><b>{{ $cont }}</b></td>
                                        <td align="right">Status</td>
                                        <td align="left">
                                            <b>
                                                @if ($b->bilhete_excluido)
                                                    Excluido
                                                @else
                                                    @if (! $b->bilhete_valido )
                                                        Pré bilhete
                                                    @else
                                                        Validado
                                                    @endif
                                                @endif

                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Cancelado por:</td>
                                        <td align="left">
                                            <b>
                                            @if ($b->bilhete_excluido)
                                                teste
                                            @endif
                                            </b>
                                        </td>
                                        <td align="right">Vl. Pago: </td>
                                        <td align="left">
                                            <b>
                                                
                                                @if ( $b->bilhete_premiado )
                                                    <span class="formatMoney">
                                                        {{ $b->valor_premio_aposta_bilhete }}
                                                    </span>
                                                @endif
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"></td>
                                        <td align="center" colspan="2">
                                            @if (! $b->bilhete_valido )
                                                <button class="btn title-odd-atual" type="submit">Confirmar Bilhete</button>
                                            @endif

                                            @if ($b->bilhete_valido )
                                            
                                                @php ($url = "Acesse o link para verificar seu bilhete: ".url('print_bilhete/'.$b->codigo_validacao_bilhete ) )
                                                <a target="blank" href="https://wa.me/?text={{ $url }}" class="btn title-odd-atual">WhatsApp</a>        
                                                <a target="blank" href="{{ url('print_bilhete/'.$b->codigo_validacao_bilhete.'') }}" class="btn title-odd-atual">Imprimir Bilhete</a>
                                                @if( Auth::user()->id_perfil === 1)
                                                    <a href="javascript:confirmExBilhete('{{$b->codigo_validacao_bilhete}}')" class="btn title-odd-atual">Cancelar Bilhete</a>
                                                @endif
                                            @endif
                                        </td>
                                        <td align="right">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    @endforeach
                @endif


                @if (isset($listaBilhetes) )
                        
                <table class="table table-striped table-bordered" style="width:100%">
                    
                        <thead style="background-color: #BF130D">
                            <tr>
                                <td><font color="white">Codigo</font></td>
                                <td><font color="white">Nome</font></td>
                                <td><font color="white">Valor</font></td>
                                <td><font color="white">Prêmio</font></td>
                                <td><font color="white">Data de criação</font></td>
                                <td><font color="white">status</font></td>

                            </tr>
                        </thead>
                            @foreach ($listaBilhetes as $lb)
                                @if ($lb->bilhete_premiado === 1 && $lb->bilhete_valido === 1)
                                    <tr style="background-color: #5fbd86">
                                @elseif ($lb->bilhete_premiado === 0 && $lb->bilhete_valido === 1)
                                    <tr style="background-color: #d49292">
                                @else
                                    <tr>
                                @endif
                                        <td><a href="{{ url('admin/find_bilhete_detail/'.$lb->codigo_validacao_bilhete.'') }}">{{ $lb->codigo_validacao_bilhete }}</a></td>
                                        <td>{{ $lb->nome_cliente_bilhete }}</td>
                                        <td>{{ $lb->valor_aposta_bilhete }}</td>
                                        <td>{{ $lb->valor_premio_aposta_bilhete }}</td>
                                        <td>{{ date('d/m/Y', strtotime($lb->created_at) ) }}</td>
                                        <td>
                                            @if($lb->bilhete_valido === 0)
                                                Pré-bilhete
                                            @elseif ($lb->bilhete_valido === 1)
                                                @if ($lb->bilhete_premiado === 1)
                                                    Vencedor
                                                @elseif($lb->bilhete_premiado === 0)
                                                    Perdedor
                                                @else
                                                    Validado
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                
                            @endforeach
                    </table>
                @endif
            </div>
            <div class="col-md-2"></div>            
        </div>
        
        
        @include('templates._rodape')
    </div>
@endsection


