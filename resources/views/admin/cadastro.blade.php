@extends('templates.defaultTemplate')

@section('body')
    <div class="container">
        
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">

            <div class="col-md-2"></div>
            <div class="col-md-8">
                
                @if(isset($message) )
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! $message !!}</li>
                        </ul>
                    </div>
                @endif

                @if(isset($messageErro) )
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ $messageErro }}</li>
                        </ul>
                    </div>
                @endif

                @if ( isset( $dados ) )

                    @foreach ( $dados as $value )
                    
                        <form method="post" action="{{ url('admin/editar/do')}}" class="form">
                            <div class="mb-3"></div>
                            <div class="mb-3">
                                <h2 align="center"><b>Formulário de edição de cadastro</b></h2>
                            </div>
                            <div class="mb-3">
                                <label for="name" class="form-label">Nome completo</label>
                                <input type="text" class="form-control" id="name" name="name"  value="{{$value->name}}" placeholder="nome completo" required autocomplete="off">
                            </div>

                            <div class="mb-3">
                                <label for="login" class="form-label">Login</label>
                                <input type="text" class="form-control" id="login" name="login" placeholder="login" value="{{$value->login}}" required autocomplete="off">
                            </div>
                            
                            <div class="mb-3">
                                <label for="id_perfil" class="form-label">Perfil</label>
                                <select class="form-control form-select form-select-sm" aria-label=".form-select-sm example" name="id_perfil" id="id_perfil">
                                    <option value="2">Cambista</option>        
                                </select>
                            </div>

                            <input type="hidden" value="{{ $value->id }}" id="id_user" name='id_user'>
                            <div class="mb-3" align="center">

                                <button type="reset" class="btn title-odd-atual">Limpar</button> &nbsp;
                                <button type="submit" class="btn title-odd-atual">Editar</button>
                            </div>
                        </form>
                        @break
                    @endforeach

                @else

                    <form method="post" action="{{ url('admin/cadastro/do')}}" class="form">
                        <div class="mb-3"></div>
                        <div class="mb-3">
                            <h2 align="center"><b>Formulário de cadastro</b></h2>
                        </div>
                        <div class="mb-3">
                            <label for="name" class="form-label">Nome completo</label>
                            <input type="text" class="form-control" id="name" name="name"  placeholder="nome completo" required autocomplete="off">
                        </div>

                        <div class="mb-3">
                            <label for="login" class="form-label">Login</label>
                            <input type="text" class="form-control" id="login" name="login" placeholder="login" required autocomplete="off">
                        </div>

                        <div class="mb-3">
                            <label for="id_perfil" class="form-label">Perfil</label>
                            <select class="form-control form-select form-select-sm" aria-label=".form-select-sm example" name="id_perfil" id="id_perfil">    
                                <option value="2">Cambista</option>        
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="saldo_aposta" class="form-label">saldo para apostas</label>
                            <input type="text" class="form-control input_money" id="saldo_aposta" name="saldo_aposta" placeholder="saldo para apostas" required autocomplete="off">
                        </div>

                        <div class="mb-3" align="center">

                            <button type="reset" class="btn title-odd-atual">Limpar</button> &nbsp;
                            <button type="submit" class="btn title-odd-atual">Cadastrar</button>
                        </div>

                    </form>
                @endif
            </div>
            <div class="col-md-2"></div>            
        </div>

        @include('templates._rodape')
    </div>
@endsection

