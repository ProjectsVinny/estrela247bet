@extends('templates.defaultTemplate')

@section('body')
    <div class="list-group" id="list-tab" role="tablist">
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">

            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row">
                    <br />
                    <h2 align="center" size>
                            <font size="10">Caixa</font>
                    </h2>
                </div>
                @if( isset($message) )
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ $message }}</li>
                        </ul>
                    </div>
                @endif
                <div class="row list-group" id="list-tab" role="tablist">
                    <form class="row g-3" method="post" action="{{ url('admin/caixa/do') }}">

                        <div class="col-md-12">
                            <label for="inputEmail4" class="form-label">Usuário</label>
                            <select name="usuario" id="usuario" class="form-select form-select-sm" >
                                @if(Auth::user()->id_perfil == 1)
                                    <option disabled selected>Selecione o usuário</option>
                                @endif    
                                @foreach ($usuarios as $u)
                                    <option value="{{ $u->id }}">{{ $u->login }}</option>                                        
                                @endforeach
                            </select>
                        </div>

                        <div class="col-12">
                            <label for="inputAddress2" class="form-label">Tipo Relatório</label>
                            <select name="tipo_relatorio" id="tipo_relatorio" class="form-select form-select-sm">
                                <option selected disabled>Selecione um tipo de relatório</option>
                                <option value='1'>Comissão</option>
                                <option value='2'>Detalhado</option>
                                {{-- <option>Cancelado</option>
                                <option>Premiado</option> --}}
                            </select>
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">De</label>
                            {{-- @if(Auth::user()->id_perfil == 1) --}}
                                <input type="date" class="form-control" id="data_inicio" name="data_inicio" >
                            {{-- @else
                                @if( date('Y-m-d' , strtotime('+7 days' , strtotime('tuesday last week') ) ) > date('Y-m-d' , strtotime(now() ) ) )
                                    <input type="date" class="form-control" id="data_inicio" name="data_inicio" min="{{ date('Y-m-d' , strtotime('-1 days' , strtotime('tuesday last week') ) ) }}">
                                @else
                                    <input type="date" class="form-control" id="data_inicio" name="data_inicio" min="{{ date('Y-m-d' , strtotime('+6 days' , strtotime('tuesday last week') ) ) }}">
                                @endif
                            @endif --}}
                        </div>

                        <div class="col-6">
                            <label for="inputAddress" class="form-label">até</label>
                            {{-- @if(auth::user()->id_perfil == 1) --}}
                                <input type="date" class="form-control" id="data_fim" name="data_fim" >
                            {{-- @else
                                <input type="date" class="form-control" id="data_fim" name="data_fim" min="{{ date('Y-m-d' , strtotime('+5 days' , strtotime('tuesday last week') ) ) }}">
                            @endif --}}
                        </div>

                        <div class="col-12" id="div_status">
                            <label for="inputAddress2" class="form-label">Status</label>
                            <select name="" id="" class="form-select form-select-sm" >
                                <option>Aberto</option>
                                <option>Cancelado</option>
                                <option>Premiado</option>
                            </select>
                        </div>

                        <div class="col-12" id="div_cliente">
                            <label for="cliente" class="form-label">Cliente</label>
                            <input type="text" id="cliente" name="cliente" class="form-control" placeholder="Digite o nome do cliente">
                        </div>

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Pesquisar</button>
                        </div>
                    </form>
                    <div class="col-md-12">
                        <br><br>
                            @if( isset($dados))
                                @foreach ($dados as $d)
                                    @if( isset($d['data_inicio']) && $d['data_fim'])
                                        <div class="alert alert-success">
                                            <ul>
                                                <li>periodo selecionado de {{ $d['data_inicio'] }} até {{ $d['data_fim'] }}</li>
                                            </ul>
                                        </div>    
                                    @endif
                                    @break
                                @endforeach
                                @if(isset($dados['login_usuario']))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>Relatório do usuário : {{ $dados['login_usuario'] }}</li>
                                        </ul>
                                    </div>   
                                @endif
                            @endif
                        <br><br>
                    </div>

                    @if( isset($dados))
                        <div class="col-md-12">
                            @if($tipo_relatorio == 1)
                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                            @endif

                            @if($tipo_relatorio == 2)
                                <table class="table table-striped table-bordered" style="width:100%">
                            @endif
                                <thead  style="background-color: #BF130D">
                                    @if($tipo_relatorio == 1)
                                        <tr>
                                            <th><font color="white">Usuário</font></th>
                                            <th><font color="white">Total Apostas</font></th>
                                            <th><font color="white">Total em Prêmios</font></th>
                                            <th><font color="white">Comissão</font></th>
                                        </tr>
                                    @elseif ($tipo_relatorio == 2)
                                        <tr>
                                            <th><font color="white">Descrição</font></th>
                                            <th><font color="white">N. de apostas</font></th>
                                            <th><font color="white">Total Bruto</font></th>
                                            <th><font color="white">Total Pago</font></th>
                                            <th><font color="white">Total Comissão</font></th>
                                            <th><font color="white">Total liquido</font></th>
                                        </tr>
                                    @endif
                                </thead>
                                <tbody>
                                    @if($tipo_relatorio == 1)
                                        @foreach ($dados as $d)
                                            <tr>
                                                <td>{{ $d['login'] }}</td>
                                                <td><span>R$: {{ number_format($d['total_bilhete'] , 2 , ',' , '') }}</span></td>
                                                <td><span>R$: {{ number_format($d['total_premio'] , 2 , ',' , '') }}</span></td>
                                                <td><span>R$: {{ number_format(($d['total_bilhete'] * env('PORCENTAGEM_COMISSAO')) , 2 , ',' , '') }}</span></td> 
                                            </tr>
                                        @endforeach
                                    @elseif ($tipo_relatorio == 2)
                                        <tr>
                                            <td>Pago</td>
                                            <td><span>{{ $dados['n_apostas_pagas'] }}</span></td>
                                            <td><span>R$: {{ number_format($dados['total_bruto_pago'] , 2 , ',' , '') }}</span></td>
                                            <td><span>R$: {{ number_format($dados['total_pago'] , 2 , ',' , '') }}</span></td> 
                                            <td><span>R$: {{ number_format($dados['total_comissao_pago'] , 2 , ',' , '') }}</span></td> 
                                            <td><span></span></td> 
                                        </tr>

                                        <tr>
                                            <td>Perdeu</td>
                                            <td><span>{{ $dados['n_apostas_perdidas'] }}</span></td>
                                            <td><span>R$: {{ number_format($dados['total_bruto_perdeu'] , 2 , ',' , '') }}</span></td>
                                            <td><span>R$: {{ number_format($dados['total_perdeu'] , 2 , ',' , '') }}</span></td> 
                                            <td><span>R$: {{ number_format($dados['total_comissao_perdeu'] , 2 , ',' , '') }}</span></td> 
                                            <td><span></span></td> 
                                        </tr>
                                    @endif
                                </tbody>
                                <tfoot  style="background-color: #BF130D">
                                    <tr>
                                    @if($tipo_relatorio == 1)
                                        <th><font color="white">Usuário</font></th>
                                        <th><font color="white">Total Apostas</font></th>
                                        <th><font color="white">Total em Prêmios</font></th>
                                        <th><font color="white">Comissão</font></th>
                                    
                                    @elseif ($tipo_relatorio == 2)
                                        
                                        <th><font color="white"></font></th>
                                        <th><font color="white">{{ ($dados['n_apostas_pagas'] + $dados['n_apostas_perdidas']) }}</font></th>
                                        <th><font color="white">{{ number_format($dados['total_bruto_pago'] + $dados['total_bruto_perdeu'] , 2 , ',' , '') }}</font></th>
                                        <th><font color="white">{{ number_format($dados['total_pago'] + $dados['total_perdeu'] , 2 , ',' , '') }}</font></th>
                                        <th><font color="white">{{ number_format($dados['total_comissao_pago'] + $dados['total_comissao_perdeu'] , 2 , ',' , '') }}</font></th>

                                        <th><font color="white">{{ number_format( ( ( $dados['total_bruto_pago'] + $dados['total_bruto_perdeu'] ) - ( $dados['total_pago'] + $dados['total_perdeu']) ) - ($dados['total_comissao_pago'] + $dados['total_comissao_perdeu'])  , 2 , ',' , '') }}</font></th>
{{-- number_format(   ( ( $dados['total_pago'] + $dados['total_perdeu'] ) - $d['total_premio'] ) - $dados['total_comissao_pago'] + $dados['total_comissao_perdeu'] )  --}}
                                    @endif
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    @endif

                    <div class="col-md-12">
                        <br><br>
                        <br><br>
                    </div>

                </div>
            </div>
            <div class="col-md-1"></div>            
        </div>

        
        @include('templates._rodape')
    </div>
@endsection

