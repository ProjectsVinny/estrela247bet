@extends('templates.defaultTemplate')

@section('body')
    <div class="container">
        
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">
            
            <div class="col-md-3 overflow-auto" style="max-height: 750px; max-width: 100%">
                @include('templates.menu_lateral')
            </div>
            
            @include( 'templates.carroussel')

            <div class="col-md-3 overflow-auto" class="" style="max-height: 750px; max-width: 100%">
                @include('templates.jogos_selecionados')
            </div>
        </div>

        @include('templates._rodape')
    </div>
@endsection