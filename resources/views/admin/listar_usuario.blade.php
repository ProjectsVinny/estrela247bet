@extends('templates.defaultTemplate')

@section('body')
    <div class="container">
        
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">

            <div class="col-md-2"></div>
            <div class="col-md-8">
                <br><br>
                <a class="btn btn-danger" href="{{ url('admin/cadastro') }}">Incluir novo usuário</a>
                <br><br>
                @if(isset($message) )
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! $message !!}</li>
                        </ul>
                    </div>
                @endif

                @if(isset($messageErro) )
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ $messageErro }}</li>
                        </ul>
                    </div>
                @endif
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td class="title-odd-atual">Nome</td>
                            <td class="title-odd-atual">Login</td>
                            <td class="title-odd-atual" width="15%">Ações</td>
                        </tr>
                    </thead>
                    <tbody>
                        @if ( isset($dados) )
                            @foreach ($dados as $item)
                                <tr>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->login }}</td>
                                    <td>

                                        <a type="button" class="btn btn-primary"  href=" {{ url('admin/atualizar/'.$item->id.'') }} ">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"></path>
                                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"></path>
                                            </svg>
                                          </a>

                                        <a type="button" class="btn btn-outline-danger"  href=" {{ url('admin/delete/'.$item->id.'') }}">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-octagon-fill" viewBox="0 0 16 16">
                                                <path d="M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zm-6.106 4.5L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 1 1 .708-.708z"></path>
                                            </svg>
                                        </a>

                                    </td>
                                </tr>
                            @endforeach
                        @endif

                    </tbody>
                </table>
                
            </div>
            <div class="col-md-2"></div>            
        </div>

        @include('templates._rodape')
    </div>
@endsection

