<!DOCTYPE html>
<html lang="br">
<head>
    <title>Bilhete</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
        @media  screen {
            .print {
                display: none;
            }

            body {
                font-family: monospace;
            }

            .btn-search {
                padding: 9px 12px;
            }

            h4 {
                font-size: 14px;
                font-weight: bold;
                margin: 5px;
                text-align: center;
            }

            hr {
                width: 99%;
                border: 0;
                border-bottom: 1px dashed black;
                margin-block-start: 0.5em;
                margin-block-end: 0.5em;
            }

            .corpo-bilhete {
                border: 1px solid rgba(0, 0, 0, 0.4);
                box-shadow: rgba(31, 31, 31, 0.5) 0px 2px 7px 3px;
                margin: 0 auto;
                width: 400px;
                padding: 10px;
                background-color: rgb(248, 236, 194);
                color: black;
                box-sizing: border-box;
                border-radius: 3px;
            }

            .btn-imprimir {
                width: calc(100% - 20px);
                background-color: var(--btn-primary-color) !important;
                color: var(--primary-text-color) !important;
                border: none;
                text-align: center;
                box-sizing: border-box;
                padding: 15px;
                margin: 10px 10px 0;
            }

            .btn-situacao {
                text-decoration: none;
                text-align: center;
                padding: 15px;
                box-sizing: border-box;
                margin: 10px;
                text-transform: capitalize;
                width: 400px;
                margin: auto;
                margin-top: 10px;
                margin-bottom: 10px;
                border-radius: 3px;
                font-size: 15px;
                font-weight: bold;
                box-shadow: rgba(31, 31, 31, 0.5) 0px 2px 7px 3px;
            }

            .btn-situacao:focus {
                outline: none;
            }

            .btn-aguardando {
                color: white;
                background-color: rgb(58, 95, 205);
                padding-right: 5px;
                display: block;
            }

            .btn-perdedor {
                color: white;
                background-color: rgb(255, 64, 64);
                padding-right: 5px;
                display: block;
            }

            .btn-cancelado {
                color: white;
                background-color: rgb(255, 165, 0);
                padding-right: 5px;
                display: block;
            }

            .btn-vencedor {
                color: white;
                background-color: rgb(0, 106, 35);
                padding-right: 5px;
                display: block;
            }

            .botao-validar {
                width: 400px;
                min-height: 60px;
                margin: 20px auto;
            }

            .btn {
                width: 100%;
            }

            .footer{
                display: flex;
                flex-direction: column;
                align-items: center;
            }

            .footer button{
                margin-bottom: 5px !important;
                width: 100% !important;
            }

            .footer .btn-warning{
                left: 2px;
            }

            .modal-footer {
                padding: 5px 15px !important;
                width: calc(100% - 30px);
                margin: 15px 10px;
            }

            .modal-body {
                padding-bottom: 0;
            }

            .bi-side {
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                margin-right: 5px;
            }

            @media (max-width: 435px) {
                .btn-situacao {
                    width: 100%;
                }
                .corpo-bilhete {
                    width: 100%;
                }
            }
        }

        @media  print {
            .screen {
                display: none;
            }

            body {
                color: #000 !important;
                background-color: #fff;
                margin: 0;
                font-family: monospace;
                font-size: 11px;
                font-weight: normal !important;
                margin: 1.6mm;
            }
            hr {
                margin: 1mm 0;
                border-top: 1px dashed #000;
            }
            @page  {
                size: 88mm 134mm;
                margin: 0;
                font-size: 10px;
            }

            .aposta table td:nth-child(1) {
                width: auto !important;
            }

            .aposta table td:nth-child(2) {
                display: none;
            }
            p {
                margin: 0 0 1mm 0;
            }
        }
    </style>
</head>
<body>
    <div class="screen">
        <div class="modelo-bilhete">
            <button class="btn-cancelado btn-situacao"
                    style="border: none;
                        cursor: pointer;
                        "
                    onclick="window.print()"
                >
                    <i class="fa fa-remove"></i> Imprimir
            </button>

            @php ($cont = 0)

            @if ( isset( $bilhete ) )
                @foreach ($bilhete as $b)
                    <div class="corpo-bilhete">        
                        <h2 style="text-align: center;"> estrela247bet </h2> 
                        <div class="detalhes-bilhete">
                            <span><b>DATA: </b></span>
                            <span> {{ date('d/m/Y H:i:s', strtotime($b->created_at)) }} </span>
                            <br>
                            <span><b>VENDEDOR: </b></span>
                            @foreach ($cambista as $c)
                                <span> {{ $c->login }}  </span>
                                @break
                            @endforeach
                            <br>
                            <span><b>CLIENTE: </b></span>
                            <span> {{ $b->nome_cliente_bilhete }} </span>
                            <br>
                        </div>

                        <hr align="center">

                        <div class="palpites-bilhete">
                            <div class="bi-side">
                                <b>OPÇÃO</b>
                                <b>COTAÇÃO</b>
                            </div>
                        </div>

                        <hr>

                        @php ($resultado = null)
                        @foreach ($detalhe_bilhete as $db) 
                            @php ($cont++)
                            <div>
                                <div class="palpite">
                                    <div style="display: inline-block; width: 99%; text-align: left;">
                                        <b> Futebol </b> - <b>{{ date('d/m/Y H:i:s', strtotime( $db->data_hora_jogo_bilhete_detalhe ) ) }}</b>
                                    </div>
                                </div>
                            </div>

                            {{-- <div>
                                <div style="display: inline-block; width: 99%; text-align: left;">
                                    Brasil:   Copa do Brasil
                                </div>
                            </div> --}}

                             <div>
                                <b> {{ $db->time_casa_bilhete_detalhe}} x {{ $db->time_visitante_bilhete_detalhe}} </b>
                            </div>

                            <div class="bi-side">
                                <div>
                                    {{ $db->descricao_odd_bilhete_detalhe }}
                                </div>
                                <div>
                                    {{ $db->taxa_odd_bilhete_detalhe }}
                                </div>
                            </div>

                            <div class="bi-side">
                                <div>
                                    <b>Situação:</b>
                                </div>
                                
                                @if ( $db->resultado_jogo == '1' )
                                    <div style="display: inline-block; width: 48%; text-align: right; text-transform: capitalize;">
                                        <span class="btn-vencedor">Vencedor</span>
                                    </div>
                                    
                                @elseif ($db->resultado_jogo == '0')
                                    @php ($resultado = true)
                                    <div style="display: inline-block; width: 48%; text-align: right; text-transform: capitalize;">
                                        <span class="btn-perdedor">Perdedor</span>
                                    </div>
                                @elseif ($db->resultado_jogo == '2')
                                    <div style="display: inline-block; width: 48%; text-align: right; text-transform: capitalize;">
                                        <span class="btn-vencedor"> Não tem aposta </span>
                                    </div>
                                @elseif ($db->resultado_jogo == '3')
                                    <div style="display: inline-block; width: 48%; text-align: right; text-transform: capitalize;">
                                        <span class="btn-aguardando"> Adiado </span>
                                    </div>
                                @else
                                    @php ($resultado = null)
                                    <div style="display: inline-block; width: 48%; text-align: right; text-transform: capitalize;">
                                        <span class="btn-aguardando"> Aguardando </span>
                                    </div>
                                    
                                @endif
                            </div>
                            <hr>
                        @endforeach

                        <div class="resumo-bilhete">
                            <div class="bi-side">
                                <div>
                                    <span style="display: inline-block"><b>Quantidade de Jogos:</b></span>
                                </div>
                                <div>
                                    <span style="display: inline-block">
                                        {{ $cont }}
                                    </span>
                                </div>
                            </div>

                            <div class="bi-side">
                                <div style="display: inline-block; width: 49%; text-align: left;">
                                    <span style="display: inline-block"><b>Cotação:</b></span>
                                </div>
                                <div style="display: inline-block; width: 49%; text-align: right;">
                                    <span style="display: inline-block">
                                        {{ $b->valor_cotacao_aposta_bilhete }}
                                    </span>
                                </div>
                            </div>

                            <div class="bi-side">
                                <div>
                                    <span style="display: inline-block"><b>Total Apostado:</b></span>
                                </div>
                                <div>
                                    <span style="display: inline-block">
                                        {{ $b->valor_aposta_bilhete }}
                                    </span>
                                </div>
                            </div>

                            <div class="bi-side">
                                <div>
                                    <span style="display: inline-block"><b>Possível Retorno:</b></span>
                                </div>
                                <div>
                                    <span style="display: inline-block">
                                        {{ $b->valor_premio_aposta_bilhete }}
                                    </span>
                                </div>
                            </div>
                            <hr>
                            <div style="display: inline-block; width: 99%; text-align: center;">
                                Bilhete
                            </div>
                            <div style="display: inline-block;width: 99%;text-align: center;font-size: 19px;font-weight: bold;">
                                {{ $b->codigo_validacao_bilhete }}
                            </div>
                            <hr>

                            <div style="text-align: center;"> 
                                Eu li e concordo com as regras da estrela247bet.
                                <h4>Boa sorte!</h4>
                            </div>
                        </div>
                        </div>
                            @if ($resultado == true)
                                <span class="btn-perdedor btn-situacao">Perdedor</span>
                            @elseif ( $b->bilhete_premiado == 1)
                                <span class="btn-vencedor btn-situacao">Vencedor</span>
                            @elseif ( $b->bilhete_premiado == null)
                                <span class="btn-aguardando btn-situacao">Aguardando</span>
                            @endif
                        </div>
                    </div>
                @endforeach
            @endif   
        </div>
    </div>

    @if(isset($bilhete))

    
        <div class="print">
            <div id="bilhete">
                Data..: {{ date('d/m/Y H:i:s', strtotime($b->created_at)) }}<br>
                @foreach ($cambista as $c)
                    Login.: {{$c->name}}<br>
                @endforeach
                Nome..: {{ $b->nome_cliente_bilhete }}<br>
                Codigo: {{ $b->codigo_validacao_bilhete }}<br>
                Situacao: Perdedor<br><br>

                Seus palpites:<br>
                @foreach ($detalhe_bilhete as $db)
                    <div class="aposta">
                        <table>
                            <tr>
                                {{-- <td>Brasil:Copa do Brasil<br> --}}
                                    {{ $db->time_casa_bilhete_detalhe}} x {{ $db->time_visitante_bilhete_detalhe}}<br>
                                    {{ date('d/m/Y H:i:s', strtotime( $db->data_hora_jogo_bilhete_detalhe ) ) }}<br>
                                    {{ $db->descricao_odd_bilhete_detalhe }} - {{ $db->taxa_odd_bilhete_detalhe }}<br>
                                    Situacao:
                                    @if($db->resultado_jogo == null)
                                        Aguardando
                                    @elseif ($db->resultado_jogo == 0)
                                        Perdedor
                                    @elseif ($db->resultado_jogo == 1)
                                        Vencedor
                                    @elseif ($db->resultado_jogo == 2)
                                        Não Tem aposta
                                    @endif
                                </td>       
                            </tr>
                        </table>
                        ----------------------------
                        <br>
                        
                    </div>    
                @endforeach
                
                <p>Quantidade de Jogos: {{ $cont }}</p>
                <p>Total Apostado: R$ {{ $b->valor_aposta_bilhete }} </p>
                <p>Possível Retorno: R$ {{ $b->valor_premio_aposta_bilhete }}</p>
                <p><b>Eu li e concordo com as regras e termos da estrela247bet</b></p>
            </div>
        </div>
    @endif

</body>
</html>