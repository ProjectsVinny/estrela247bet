@extends('templates.defaultTemplate')

@section('body')
    <div class="list-group" id="list-tab" role="tablist">
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">

            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="row">
                    <br />
                    <h2 align="center" size>
                            <font size="10">Adicionar saldo</font>
                    </h2>
                </div>
                @if( isset($message) )
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ $message }}</li>
                        </ul>
                    </div>
                @endif
                @if( isset($messageSucesso) )
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ $messageSucesso }}</li>
                        </ul>
                    </div>
                @endif
                <div class="row list-group" id="list-tab" role="tablist">
                    <form class="row g-3" method="post" action="{{ url('admin/saldo/do') }}">

                        <div class="col-md-12">
                            <label for="usuario" class="form-label">Usuário</label>
                            <select name="usuario" id="usuario" class="form-select form-select-sm select_usuario_saldo" required>
                                @if(Auth::user()->id_perfil == 1)
                                    <option disabled selected>Selecione o usuário</option>
                                @endif    
                                @foreach ($usuarios as $u)
                                    <option value="{{ $u->id }}">{{ $u->login }}</option>                                        
                                @endforeach
                            </select>
                        </div>

                        <div class="col-sm-12">
                            <label for="saldo_usuario" class="form-label">Saldo</label>
                            <input min='0' type="number" name="saldo_usuario" id="saldo_usuario"  class="form-control form-control-sm saldo_usuario" onkeypress="return (event.charCode == 8 || event.charCode == 0) ? null : event.charCode >= 48 && event.charCode <= 57" disabled required>
                        </div>

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary">Salvar</button>
                        </div>
                    </form>

                    <br>
                    <br>
                    <br>
                    <br>

                    @if( isset($usuarios))
                        <div class="col-md-12">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead  style="background-color: #BF130D">
                                    <tr>
                                        <th><font color="white">Login</font></th>
                                        <th><font color="white">Saldo</font></th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach ($usuarios as $usuario)
                                        <tr>
                                            <td>{{ $usuario->login }}</td>
                                            <td>R$: {{ $usuario->saldo_apostas }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif

                    <div class="col-md-12">
                        <br><br>
                        <br><br>
                    </div>

                </div>
            </div>
            <div class="col-md-1"></div>            
        </div>

        
        @include('templates._rodape')
    </div>
@endsection

