@extends('templates.defaultTemplate')

@section('body')
    <div class="container">
        
        @include('admin._menu_admin_superior')
            
        <div class="row fundo_branco">

            <div class="col-md-2"></div>
            <div class="col-md-8">
                
                <form action=""  class="form">
                    <div class="mb-3"></div>
                    <div class="mb-3">
                        <h2 align="center"><b>Formulário de cadastro</b></h2>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Nome completo</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="nome">
                    </div>

                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Login</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="login">
                    </div>

                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">E-mail</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="email">
                    </div>

                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Perfil</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="name@example.com">
                    </div>

                    <div class="mb-3" align="center">

                        <button type="reset" class="btn title-odd-atual">Limpar</button> &nbsp;
                        <button class="btn title-odd-atual">Cadastrar</button>
                    </div>

                </form>
            </div>
            <div class="col-md-2"></div>            
        </div>

        @include('templates._rodape')
    </div>
@endsection

