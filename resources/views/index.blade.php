@extends('templates.defaultTemplate')

@section('body')
    <div class="container">
        
        @include('templates._menu_login')
            
        <div class="row">
            <div class="col-md-12 menu_superior">
                <a href="{{url ('/')}}" class="btn title-odd-atual">
                    Apostas
                </a>
                
                <a href="{{url ('consultar_bilhete')}}" class="btn title-odd-atual">
                    conferir bilhete
                </a>
            </div> 
        </div>

        <div class="row fundo_branco">

            <div class="col-md-3 overflow-auto" style="max-height: 750px; max-width: 100%">
                @include('templates.menu_lateral')
            </div>    
                    
            @include( 'templates.carroussel')

            <div class="col-md-3 overflow-auto" class="" style="max-height: 750px; max-width: 100%">
                @include('templates.jogos_selecionados')
            </div>
        </div>

        @include('templates._rodape')
    </div>
@endsection