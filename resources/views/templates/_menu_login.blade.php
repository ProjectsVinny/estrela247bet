<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container-fluid">

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarLoginUsuario" aria-controls="navbarLoginUsuario" aria-expanded="false" aria-label="Toggle navigation">
            <a class="btn btn-danger" id="list-home-list" data-toggle="tab" href="#" role="tab" aria-controls="list-home">Acessar o sistema</a>
        </button>
    

        <div class="collapse navbar-collapse list-group" role="tablist" id="navbarLoginUsuario">
            <ul class="me-auto mb-12 mb-lg-0">
                <form action="{{ route('admin.login.do') }}" method="post">
                    <div class="row">
                        @csrf
                        @if ($errors->all())
                            <script>
                                alert('Usuário ou senha incorretos!');   
                            </script>        
                        @endif
                        <div class="col-md-7">
                            @if (env('APP_ENV') === 'production')
                                <img class="scala_logo" src="{{ asset('public/Images/logo_estrelabet247.png') }}" alt="">
                            @else
                                <img class="scala_logo" src="{{ asset('Images/logo_estrelabet247.png') }}" alt="">
                            @endif
                        </div>
                        <div class="col-md-2">
                            <br/>
                            <input type="text" name="login" id="login" class="form-control" placeholder="login" aria-label="login">
                        </div>
                        <div class="col-md-2">
                            <br/>
                            <input type="password" name="password" id="password"  class="form-control" placeholder="senha" aria-label="senha">
                        </div>
                        
                            <div class="col-md-1">
                                <br/>
                                <button class="btn btn-danger" type="submit">Login</button>
                            </div>
                        
                    </div>
                </form>
            </ul>
        </div>
     </div>
</nav>
