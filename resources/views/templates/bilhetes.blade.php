
@extends('templates.defaultTemplate');

@section('body')
    <div class="container">
        
        @include('templates._menu_login')

        <div class="row">
            <div class="col-md-12 menu_superior">
                    <a href="{{url ('/')}}" class="btn title-odd-atual">
                        Apostas
                    </a>
                    
                    <a href="{{url ('consultar_bilhete')}}" class="btn title-odd-atual">
                        conferir bilhete
                    </a>
                    
            </div> 
        </div>
        <div class="row fundo_branco">

            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="mb-2"></div>
                <div class="mb-3 centerDiv" >
                    <h2 align="center"><b>Consultar Bilhetes</b></h2>
                    <br><br>

                    @if ( $errors->any() )
                    
                        <div class="alert alert-danger">
                            <ul>
                                <li>O campo codigo pré bilhete é obrigatório!</li>
                            </ul>
                        </div>
                    @endif
                        
                    @if ( isset($message) )
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{ $message }}</li>
                            </ul>
                        </div>
                    @endif

                    <form class="row g-3" method="POST" action="{{ url('/consultar_bilhete/do') }}">
                        @csrf
                        <div class="col-auto " align="center">
                            <label for="codigo_bilhete" class="visually-hidden">Código Pré Bilhete: </label>
                            <input type="text" class="form-control form-control-sm inputCodBilhete" id="codigo_bilhete" name="codigo_bilhete" required>
                        </div>
                        <div class="col-auto">
                            <button type="submit" class="btn title-odd-atual mb-3">Pesquisar</button>
                        </div>
                    </form>
                </div>
                @if( isset($bilhete ) && isset($detalheBilhete))
                    <table  class="table table-striped" border="0" cellpadding="1" cellspacing="0" width="100%">
                        <tbody>
                            <tr class="title-odd-atual">
                                <td>Jogos</td>
                                <td>&nbsp;</td>
                                <td align="center">Status</td>
                                <td align="center" class="hit" title="{tip}">Resultado</td>
                            </tr>
                            @php ($cont = 0)
                            @foreach ($detalheBilhete as $db)
                                <tr class="tabelaLinhaImpar4">
                                    <td>
                                        <br><span class="fonte12">
                                            {{ $db->time_casa_bilhete_detalhe }} x {{ $db->time_visitante_bilhete_detalhe }}
                                        </span>
                                        <br><span class="fonte12">Vencedor: {{ $db->descricao_odd_bilhete_detalhe}} - Taxa {{ $db->taxa_odd_bilhete_detalhe }}</span>
                                        <br><span class="fonte12">Data: {{ $db->data_hora_jogo_bilhete_detalhe }}</span>
                                        <br>
                                    </td>
                                    <td>
                                      
                                    </td>
                                    @if ($db->resultado_jogo === 1)
                                    @php ( $cont++ )
                                    <td align="center">Vencedor</td>
                                    @elseif ($db->resultado_jogo === 0)
                                        <td align="center">Perdedor</td>
                                    @elseif ($db->resultado_jogo === 3)
                                        <td align="center">Cancelado</td>
                                    @elseif ($db->resultado_jogo === 2)
                                        <td align="center">Não tem aposta</td>
                                    @else
                                        <td align="center">Aberto</td>
                                    @endif
                                    
                                    <td align="center" class="hit" title="">
                                        resultado
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                    @foreach ($bilhete as $b)
                    <form action="{{url('admin/confirm_bilhete')}}" method="POST">
                        <input type="hidden" value="{{ $b->codigo_validacao_bilhete }}" name="validacao_bilhete" id="validacao_bilhete">
                            <table border="0" cellpadding="4" cellspacing="2" width="100%">
                                <tbody>
                                    <tr>
                                        <td align="right">Código:</td>
                                        <td align="left"><b>{{ $b->codigo_validacao_bilhete }}</b></td>
                                        <td align="right"></td>
                                        <td align="left"><b></b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Cliente: </td>
                                        <td align="left">
                                            <b>
                                                @if (! $b->bilhete_valido )
                                                    <input type="text" class="inputNomeCliente" id="nome_cliente" name="nome_cliente" required>
                                                @else
                                                    {{ $b->nome_cliente_bilhete }}
                                                @endif
                                            </b>
                                        </td>
                                        <td align="right"></td>
                                        <td align="left">
                                            <b>
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">Valor da Aposta: </td>
                                        <td align="left"><b>{{ $b->valor_aposta_bilhete }}</b></td>
                                        <td align="right">N. de Jogos: </td>
                                        <td align="left"><b>{{ $detalheBilhete->count() }}</b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Data - Hora</td>
                                        <td align="left"><b> {{ date('d/m/Y H:i:s' , strtotime($b->created_at) ) }}</b></td>
                                        <td align="right">Valor do Prêmio: </td>
                                        <td align="left"><b>  <span class="formatMoney"> {{ $b->valor_premio_aposta_bilhete }} </span> </b></td>
                                    </tr>
                                    <tr>
                                        <td align="right">Qtd Acertos:</td>
                                        <td align="left"><b>{{ $cont }}</b></td>
                                        <td align="right">Status</td>
                                        <td align="left">
                                            <b>
                                                @if (! $b->bilhete_valido )
                                                    Pré bilhete
                                                @else
                                                    Validado
                                                @endif

                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"></td>
                                        <td align="left"></td>
                                        <td align="right">Vl. Pago: </td>
                                        <td align="left">
                                            <b>
                                                
                                                @if ( $b->bilhete_premiado )
                                                    <span class="formatMoney">
                                                        {{ $b->valor_premio_aposta_bilhete }}
                                                    </span>
                                                @endif
                                            </b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right"></td>
                                        <td align="center" colspan="2">
                                            @if (! $b->bilhete_valido )
                                                <button class="btn title-odd-atual" type="submit">Confirmar Bilhete</button>
                                            @endif
                                        </td>
                                        <td align="right">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    @endforeach
                @endif
            </div>
            <div class="col-md-2"></div>            
        </div>
        
        @include('templates._rodape')
    </div>
@endsection


