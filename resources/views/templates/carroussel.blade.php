<div class="col-md-6 overflow-auto" style="max-height: 750px; max-width: 100%">
    <br>
    @if(Auth::check() !== true)
        <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                {{-- <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button> --}}
            </div>
            <div class="carousel-inner">
                <div class="carousel-item active">
                     @if (env('APP_ENV') === 'production')
                        <img src="{{ asset('public/Images/banner_01.jpg') }}" class="d-block w-100" alt="...">
                     @else
                        <img src="{{ asset('Images/banner_01.jpg') }}" class="d-block w-100" alt="...">
                    @endif
                </div>
                <div class="carousel-item">
                    @if (env('APP_ENV') === 'production')
                        <img src="{{ asset('public/Images/banner_02.jpg') }}" class="d-block w-100" alt="...">
                    @else
                        <img src="{{ asset('Images/banner_02.jpg') }}" class="d-block w-100" alt="...">
                    @endif
                </div>
                {{-- <div class="carousel-item">
                    <img src="..." class="d-block w-100" alt="...">
                </div> --}}
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>
    @endif

    @include('templates.conteudo_central')
</div>