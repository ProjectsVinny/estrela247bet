
<br />    
<table>
    <tr>
        <td colspan="5">
            <div class="input-group input-group-sm mb-3">
                <input type="text" alt="table_games" class="form-control input-search" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-sm" id="pesquisaTime" placeholder="Pesquisar por time">
            </div>
        </td>
    </tr>
</table>
<table class="table table-striped lista" id="table_games" > 
        <thead style="background-color: #BF130D">
            <tr>
                <td width="60%">
                        <font color="white">
                            Casa x Visitante <br>
                            Data - Hora
                        </font>
                </td>
                <td width="10%"><font color="white">Casa</font></td>
                <td width="10%"><font color="white">Empate</font></td>
                <td width="10%"><font color="white">Fora</font></td>
                <td width="10%"><font color="white"> + </font></td>
            </tr>
        </thead>
    @if(isset($data))
        @foreach ($data as $key => $val)
            @foreach ($val as $i)
                <tbody class="tbody_{{ $i->camp_jog_id }}">
                @break
            @endforeach
                <tr>
                    <td colspan="5" class="campeonato_header_td"><h2> {{ $key }} </h2></td>
                </tr>
                @php( 
                    usort($val , function($a,$b){
                        return $a->dt_hr_ini >= $b->dt_hr_ini;
                    })
                ) 

                @foreach ($val as $jogos)
                    <tr>
                        <td>
                            {{ $jogos->casa_time }} X {{ $jogos->visit_time }} <br>
                            {{ date('d/m/Y H:i:s' , strtotime($jogos->dt_hr_ini) ) }}
                        </td>
                        @foreach ($jogos->Odds as $odds)
                            @if ($odds->descricao === 'Casa')
                                <td><a href="javascript:checkOdds({{ $odds->jog_odd_id }} , {{ $jogos->camp_jog_id }})"> <span class="formatMoney"> {{ ( $odds->taxa * env('AUMENTAR_ODD_PORCENTAGEM') ) +  $odds->taxa }} </span> </a> </td>
                            @endif
                            @if ($odds->descricao === 'Empate')
                                <td><a href="javascript:checkOdds({{ $odds->jog_odd_id }} , {{ $jogos->camp_jog_id }})"> <span class="formatMoney"> {{ ( $odds->taxa * env('AUMENTAR_ODD_PORCENTAGEM') ) +  $odds->taxa }} </span> </a> </td>
                            @endif
                            @if ($odds->descricao === 'Fora')
                                <td><a href="javascript:checkOdds({{ $odds->jog_odd_id }} , {{ $jogos->camp_jog_id }})"> <span class="formatMoney"> {{ ( $odds->taxa * env('AUMENTAR_ODD_PORCENTAGEM') ) +  $odds->taxa }} </span> </a> </td>
                            @endif
                            @endforeach
                            
                            <td><a href="#" id="openModal" data-bs-toggle="modal" data-bs-target="#modal" onclick="loadModal({{ $jogos->camp_jog_id }})">+{{ $jogos->qtd_odds }}</a></td>
                    </tr>    
                @endforeach
            </tbody>
        @endforeach
    @endif
</table>


{{-- modal com odds detalhadas --}}

<div class="modal" id="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span class="camp_nome" id="camp_nome"></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <span class="camp_body" id="camp_body"></span>
            </div>
        </div>
    </div>
</div>