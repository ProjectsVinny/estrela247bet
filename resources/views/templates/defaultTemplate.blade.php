<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8" />

        <!-- palavras chaves para pesquisa -->
        <meta name="keywords" content="bet, aposta, jogos, futebol" />

        <meta name="description" content="Site de apostas" />

        <meta name="author" content="Marcus Vinicius Nascimento Silva" />
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

        
        @if (env('APP_ENV') === 'production')

            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}" >
            <link rel="stylesheet" type="text/css" href="{{ asset('public/css/style.css') }}" >

        @else

            <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" >
            <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" >

        @endif

        <title>@yield('title')</title>
    </head>
    <body>
        <div class="container">
            @yield('body')
        </div>
        @if (env('APP_ENV') === 'production')

            {{-- <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script> --}}
            <script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('public/js/jquery-3.6.0.min.js') }}"></script>
            <script src="{{ asset('public/js/inputMask.js') }}"></script>

            <script src="{{ asset('public/js/jquery.dataTables.min.js') }}"></script>
            {{-- <script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script> --}}

        @else

            {{-- <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script> --}}
            <script src="{{ asset('js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('js/jquery-3.6.0.min.js') }}"></script>
            <script src="{{ asset('js/inputMask.js') }}"></script>

            <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
            {{-- <script src="{{ asset('js/dataTables.bootstrap5.min.js') }}"></script> --}}
                
        @endif

        <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap5.min.js"></script>
        {{-- import sweet alert 2 --}}
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        @include('templates.script')
    </body>
</html>
    

