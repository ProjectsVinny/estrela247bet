    &nbsp;
    
    <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarMenuCampeonatos" aria-controls="navbarMenuCampeonatos" aria-expanded="false" aria-label="Toggle navigation">
                <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="tab" href="#" role="tab" aria-controls="list-home" aria-selected="true">Campeonatos</a>
            </button>

            <div class="collapse navbar-collapse list-group" role="tablist" id="navbarMenuCampeonatos">
                 <ul class="me-auto mb-12 mb-lg-0">

                    <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="tab" href="#" role="tab" aria-controls="list-home" aria-selected="true">Campeonatos</a>

                    @if(Auth::check() === true)
                        <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="tab" href="{{ url('admin') }}" role="tab" aria-controls="list-home" aria-selected="true">Todos</a>
                    @else
                        <a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="tab" href="{{ url('/') }}" role="tab" aria-controls="list-home" aria-selected="true">Todos</a>
                    @endif

                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="tab" href="javascript:loadMenuWithData('{{  substr(now() , 0 , 10) }}');" role="tab" aria-controls="list-profile" aria-selected="false">Jogos de Hoje ({{ date( 'd/m/Y', strtotime( now() ) ) }})</a>       
                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="tab" href="javascript:loadMenuWithData('{{date('Y-m-d' , strtotime('+1 days') )}}');" role="tab" aria-controls="list-profile" aria-selected="false">Jogos de Amanhã ({{ date('d/m/Y',strtotime('+1 days') ) }})</a> 
                            
                    @if ( isset($data) )
                    
                        @foreach ($data as $key => $item)
                            @foreach ($item as $i)
                                <a href="javascript:findChamp({{$i->camp_jog_id}})" class="list-group-item list-group-item-action" id="list-profile-list">
                                    <span data-bs-toggle="collapse" data-bs-target="#navbarMenuCampeonatos" aria-controls="navbarMenuCampeonatos">
                                        {{ $key }}
                                    </span>
                                </a>    
                                {{-- <a href="javascript:findChamp({{$i->camp_jog_id}})" class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="tab"  role="tab" aria-controls="list-profile" aria-selected="false">{{ $key }}</a>     --}}
                                @break
                            @endforeach
                        @endforeach
                    @endif
                </ul>
            </div>
            <button onclick="topFunction()" id="myBtn" title="Ir ao topo">Bilhete</button>
        </div>
    </nav>
