<script>
  $(document).ready(function () {
    $('#div_cliente').hide();
    $('#div_status').hide();
    $('.formatMoney').each(function(){
      this.innerHTML = maskMoney(this.innerHTML);
    });

    $('#body-bilhete').load("{{url('ajax/request/checkOdds/')}}" + '/' + null + '/' + null + '/' + 5); 

    $(".input_money").maskMoney({
        thousands: ".",
        decimal: ",",
    });

    $('#tipo_relatorio').change(function (){

        if($(this).val() == 1) {
          $('#div_cliente').hide();
          $('#div_status').hide();
        }

        if($(this).val() == 2) {

        }

        if($(this).val() == 3) {

        }
    });

    $(".input-search").keyup(function(){

      var txtVal = $(".input-search").val();
      var tabela = $(this).attr('alt');
      
      if( $(this).val() != ""){

        
        if(txtVal.length >= 3) {

            $("#"+tabela+" tbody > tr").hide();
            var filter = txtVal.toLowerCase();
            
            var nodes = $("#"+tabela+" tbody tr");

            for (i = 0; i < nodes.length; i++) {
              
                if (nodes[i].innerText.toLowerCase().includes(filter)) {
                  // $(nodes[i]).attr('style','display:block')
                  $(nodes[i]).show();
                } else {
                    $(nodes[i]).attr('style','display:none');
                }
            }
        }

        // $("#"+tabela+" td:contains-ci('" + txtVal + "')").parent("tr").show();
        
      } else {

        $("#"+tabela+" tbody > tr").show();

      }

    })

    $(".select_usuario_saldo").change(function (){
      $('.saldo_usuario').prop("disabled", false);
    }); 
  });


$('#addFavorito').click(function(e) {
    var bookmarkURL = window.location.href;
    var bookmarkTitle = document.title;

    if ('addToHomescreen' in window && addToHomescreen.isCompatible) {
      // Mobile browsers
      addToHomescreen({ autostart: false, startDelay: 0 }).show(true);
    } else if (window.sidebar && window.sidebar.addPanel) {
      // Firefox <=22
      window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
    } else if ((window.sidebar && /Firefox/i.test(navigator.userAgent)) || (window.opera && window.print)) {
      // Firefox 23+ and Opera <=14
      $(this).attr({
        href: bookmarkURL,
        title: bookmarkTitle,
        rel: 'sidebar'
      }).off(e);
      return true;
    } else if (window.external && ('AddFavorite' in window.external)) {
      // IE Favorites
      window.external.AddFavorite(bookmarkURL, bookmarkTitle);
    } else {
      // Other browsers (mainly WebKit & Blink - Safari, Chrome, Opera 15+)
      alert('Press ' + (/Mac/i.test(navigator.userAgent) ? 'Cmd' : 'Ctrl') + '+D to bookmark this page.');
    }

    return false;
  });

function loadModal(value) {
  $('#camp_body').load("{{url('ajax/request/detail')}}" + '/' + value);
  $('#camp_nome').load("{{url('ajax/request/title')}}" + '/' + value);
}

  function checkOdds( value1 , value2 ) {

    if($('#valor_aposta').val() === undefined){
      var value = 2;
    } else {
      var value = $('#valor_aposta').val();
    }

    $('#body-bilhete').load("{{url('ajax/request/checkOdds/')}}" + '/' + value1 + '/' + value2 + '/' + value); 
    // $('#footer-bilhete').load("{{url('ajax/request/footerGameOdds/')}}" + '/' + value); 
  }

  function checkDetailOdds(value1, value2) {

    if($('#valor_aposta').val() === undefined){
      var value = 2;
    } else {
      var value = $('#valor_aposta').val();
    }

    $('#body-bilhete').load("{{url('ajax/request/checkOdds/')}}" + '/' + value1 + '/' + value2 + '/' + value + '/1'); 
    $('.modal').modal('toggle');
  }

  function deleteGame( value1 , value2 ) {

    if($('#valor_aposta').val() === undefined){
      var value = 2;
    } else {
      var value = $('#valor_aposta').val();
    }

    $('#body-bilhete').load("{{url('ajax/request/checkOdds/')}}/" + value1 + '/' + value2 + '/' + value + '/2'); 
  }

function maskMoney(valor) {
  valor = valor.replace(',' , '.');
  var real = valor.split('.')[0].trim();
  var cents = valor.split('.')[1];

  if( cents == undefined ) {
    cents = "00";
  }
  if( cents.trim().length == 1 ) {
    cents = cents.trim() + "0";
  }
  if(cents.trim().length > 2) {
    cents = cents.trim().substr(0, 2);
  }

  return real + "," + cents;
}

function findChamp(code) {
  $('#table_games tbody').hide();
  $('.tbody_'+code).show();
}

function champAll() {
  $('#table_games tbody').hide();
  $('#table_games tbody').show();
}

function cleanTicket() {
  $('#body-bilhete').load("{{url('ajax/request/cleanTicket')}}"); 
}

function savePreBilhete(tipo) {

  var odd_minima = 0;
  @if (Auth::user() !== null)
    @if (Auth::user()->odd_minima !== null)
      odd_minima = {{ Auth::user()->odd_minima }};
    @endif

    @if (Auth::user()->odd_minima_2_jogos !== null)
      odd_minima_2_jogos = {{ Auth::user()->odd_minima_2_jogos }};
    @endif
  @endif
  
  
  if( $('#txt_nome_cliente').length ) {
    if($('#txt_nome_cliente').val() == '') {
      alert("O nome do cliente é obrigatório!");
      return false;
    }
  }
  
  if( $('#valor_aposta').val().split(',')[0].trim() < {{env('VALOR_MINIMO_APOSTA')}}) {
    alert('Valor mínimo de aposta R$ '+{{env('VALOR_MINIMO_APOSTA')}}+',00 ');
    return false;
  }
  if( $('#valor_aposta').val().split(',')[0].trim() > {{env('VALOR_MAXIMO_APOSTA')}}) {
    alert('Valor maximo de aposta R$ '+{{env('VALOR_MAXIMO_APOSTA')}}+',00 ');
    return false;
  }

  if(tipo == 0){
    var message = "Deseja salvar este bilhete agora?";
  } else {
    var message = "Deseja salvar este pré-bilhete agora?";
  }
  if(confirm(message)){

    var link_url;
    if( $('#txt_nome_cliente').val() != undefined){
      link_url = "{{ url('ajax/request/savePreBilhete/') }}"+"/"+$('#valor_aposta').val() + '/' +$('#txt_nome_cliente').val();
    } else {
      link_url = "{{ url('ajax/request/savePreBilhete/') }}"+"/"+$('#valor_aposta').val()
    }

    $.ajax({
        type: "GET",
        url: link_url,
    }).done(function(data) {
      
      if(data === 'sem_saldo'){
          Swal.fire({
            title: 'atenção',
            html: 'Seu usuário não possui saldo suficiente para efetivar jogos, favor entrar em contato com Administador e solicitar que seja inserido mais saldo para sua conta!',
            type: "warning"
          }).then(function() {
              window.location = "/admin";
          });

      }else if(data == 'jogo_iniciado') {
        if(tipo == 1){
          Swal.fire({
            title: 'atenção',
            html: 'O pré-bilhete não pode ser validado pois contém um ou mais jogos em andamento!',
            type: "warning"
          }).then(function() {
              window.location = "/";
          });
        } else {
          Swal.fire({
            title: 'atenção',
            html: 'O bilhete não pode ser validado pois contém um ou mais jogos em andamento!',
            type: "warning"
          }).then(function() {
              window.location = "{{ url('admin') }}";
          });
        }

      }else if(data == 'odd_minima') {
        
        Swal.fire({
        title: 'atenção',
        html: 'A soma das ODDs tem que ser superior a '+ maskMoney( String(odd_minima) ) +'!',
        type: "warning"
        }).then(function() {
            window.location = "/admin";
        });
        
      }else if(data == 'odd_minima_2_jogos') {
        
        Swal.fire({
        title: 'atenção',
        html: 'A soma das ODDs tem que ser superior a '+ maskMoney( String(odd_minima_2_jogos) ) +'!',
        type: "warning"
        }).then(function() {
            window.location = "/admin";
        });
        
      }else if(data == 'sem_jogos') {
        
        Swal.fire({
        title: 'atenção',
        html: 'O bilhete deve conter pelo menos um jogo selecionado!',
        type: "warning"
        }).then(function() {
            window.location = "/admin";
        });
        
      }else if(tipo == 1) {
        Swal.fire({
            title: 'Pré-Bilhete gerado com sucesso!',
            html: 'codigo do pré-bilhete: <b>'+ data + '</b>',
            type: "success"
        }).then(function() {
            window.location = "/";
        });
      } else {
        Swal.fire({
            title: 'Bilhete gerado com sucesso! adicionar imprimir',
            html: 'codigo do bilhete: <b>'+ data + '</b>',
            type: "success"
        }).then(function() {
            window.location = "{{ url('admin/find_bilhete_detail/') }}/" + data;
        });
      }
    });
  }
}

function attVal() {
  
  var cotacao = parseFloat($('.cotacao_atual').html().replace(',' , '.'));
  var aposta = parseFloat($('#valor_aposta').val().replace(',' , '.'));
  var total = parseFloat(0);


  if($('#valor_aposta').val().split(',')[0].trim() < 20) {

    if(parseInt(cotacao) > 0 && parseInt(aposta) > 0) {
      total = aposta * cotacao;
    }
  
    if( total > $('#valor_aposta').val().split(',')[0].trim()+'000' ) {
      total = $('#valor_aposta').val().split(',')[0].trim()+'000';
    }

  } else {

    if(parseInt(cotacao) > 0 && parseInt(aposta) > 0) {
      total = aposta * cotacao;
    }
  
    if(total > 20000) {
      total = 20000;
    }
  }

  $('.premio_aposta').html(maskMoney(String(total)));
  
}

function removerItem( value ) {
  $(document).load("{{url('admin/ajax/request/removeItem/')}}" + '/' + value);
}


function loadMenuWithData(data) {
  window.location = "{{ url('getJogos') }}"+ "/" +data;
}
function sendLinkWhatsApp(code) {
  var url = "{{ urlencode( url('print_bilhete/') ) }}/"+code;
}

function confirmExBilhete(cod) {
    if(confirm("Deseja realmente excluir este bilhete?")){
        window.location = "{{ url('admin/cancelar_bilhete') }}"+ "/" +cod;
    }
}

$(document).ready(function() {
    $('#example').DataTable();
} );

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  // document.getElementById("myBtn").style.display = "block";
}

function topFunction() {
    document.getElementById("jogos_bilhete").scrollIntoView();
}

$('document').ready(function(){

  $(document).on('keypress', 'input.only-number', function(e) {
      var $this = $(this);
      var key = (window.event)?event.keyCode:e.which;
      var dataAcceptDot = $this.data('accept-dot');
      var dataAcceptComma = $this.data('accept-comma');
      var acceptDot = (typeof dataAcceptDot !== 'undefined' && (dataAcceptDot == true || dataAcceptDot == 1)?true:false);
      var acceptComma = (typeof dataAcceptComma !== 'undefined' && (dataAcceptComma == true || dataAcceptComma == 1)?true:false);

      if((key > 47 && key < 58)
        || (key == 46 && acceptDot)
        || (key == 44 && acceptComma)) {
        return true;
      } else {
        return (key == 8 || key == 0)?true:false;
      }
    });

})

</script>