<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\{
    ConsultarApiTheOdds,
    ConsultarApiBet365,
    ConsultarSporteBetNet,
    PreBilhete,
    AuthController,
    UsuarioController,
    BilheteController,
    DetalheBilheteController,
    ConsultarBetsApi,
    CaixaController
};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// rotas de consultas as APIs

//API theOdds
// Route::get('/' , [ConsultarApiTheOdds::class, 'get'])->name('get.events');

// API betsapi
Route::get('/' , [ConsultarSporteBetNet::class, 'get'])->name('get.events');
Route::get('/getJogos/{value}' , [ConsultarSporteBetNet::class, 'loadMenuDataFilter'])->name('get.getJogos');
Route::get('/login' , [ConsultarSporteBetNet::class, 'get'])->name('get.events');
Route::post('/consultar' , [PreBilhete::class, 'getOdd'])->name('consultar.odds');
Route::get('/consultar_bilhete', [BilheteController::class , 'consultarBilheteExterno'])->name('consultar_bilhete');
Route::post('/consultar_bilhete/do', [BilheteController::class , 'getConsultarBilheteExterno'])->name('consultar_bilhete.do');

Route::get('/print_bilhete/{value}', [BilheteController::class , 'downloadPDF'])->name('print_bilhete');

Route::get('ajax/request/title/{valor}', [PreBilhete::class, 'getOddDetailTitle'])->name('ajax.request.getOddDetailTitle');
Route::get('ajax/request/detail/{valor}', [PreBilhete::class, 'getOddDetail'])->name('ajax.request.getOddDetail');

Route::get('ajax/request/checkOdds/{jogo_id}/{oddId}/{value?}/{value1?}/', [PreBilhete::class, 'getCheckOdds'])->name('ajax.checkOdds');
Route::get('ajax/request/deleteGameOdds/{jogo_id}', [PreBilhete::class, 'getDeleteGameOdds'])->name('ajax.getDeleteGameOdds');
Route::get('ajax/request/footerGameOdds/{valor?}', [PreBilhete::class, 'getResultFooterTable'])->name('ajax.getResultFooterTable');

Route::get('ajax/request/cleanTicket/', [PreBilhete::class, 'getDeleteticket'])->name('ajax.getDeleteticket');

Route::get('ajax/request/savePreBilhete/{valor}/{value2?}', [PreBilhete::class, 'getSavePreBilhete'])->name('ajax.getSavePreBilhete');

Route::get('ajax/gerarPdf', [PdfController::class, 'getGerarPDF']) ->name('ajax.gerarPdf');

// parte administrativa
Route::get('admin/logout', [AuthController::class , 'getLogout'])->name('admin.logout');
Route::post('admin/login/do', [AuthController::class , 'getLogin'])->name('admin.login.do');



Route::get('admin', [AuthController::class , 'dashboard'])->name('admin')->middleware('auth');
Route::get('admin/usuarios', [UsuarioController::class , 'getListaUsuarios'])->name('admin.usuarios')->middleware('auth');
Route::get('admin/consultar_bilhete', [BilheteController::class , 'get'])->name('admin.consultar_bilhete')->middleware('auth');
Route::post('admin/consultar_bilhete/do', [BilheteController::class , 'getfiltroBilhetes'])->name('admin.consultar_bilhete.do')->middleware('auth');
Route::get('admin/tabelas' , [TabelasController::class, 'getTables'])->name('admin.tabelas')->middleware('auth');
Route::get('admin/box' , [CaixaController::class, 'getCaixa'])->name('admin.box')->middleware('auth');
Route::post('admin/find_bilhete', [BilheteController::class , 'getStore'])->name('admin.find_bilhete')->middleware('auth');
Route::get('admin/find_bilhete_detail/{codigoBilhete}', [BilheteController::class , 'getStoreDetail'])->name('admin.find_bilhete_detail')->middleware('auth');

Route::get('admin/ajax/request/removeItem/{value}', [DetalheBilheteController::class , 'getRemoveItem'])->name('admin.ajax.request.removeItem')->middleware('auth');
Route::post('admin/confirm_bilhete', [BilheteController::class , 'getConfirm'])->name('admin.confirm_bilhete')->middleware('auth');
Route::post('admin/cadastro/do', [UsuarioController::class , 'getCadastroUsuario'])->name('admin.cadastro.do')->middleware('auth');
Route::get('admin/cadastro', [UsuarioController::class , 'getTelaCadastro'])->name('admin.cadastro')->middleware('auth');

Route::get('admin/delete/{value}', [UsuarioController::class , 'getDeleteUsuario'])->name('admin.delete')->middleware('auth');
Route::get('admin/atualizar/{value}', [UsuarioController::class , 'getEditarUsuario'])->name('admin.atualizar')->middleware('auth');
Route::post('admin/editar/do', [UsuarioController::class , 'getSalvarEditarUsuario'])->name('admin.editar.do')->middleware('auth');
Route::get('admin/caixa', [CaixaController::class , 'get'])->name('admin.caixa')->middleware('auth');
Route::post('admin/caixa/do', [CaixaController::class , 'getStore'])->name('admin.caixa.do')->middleware('auth');
Route::get('admin/saldo', [UsuarioController::class , 'getSaldo'])->name('admin.saldo')->middleware('auth');
Route::post('admin/saldo/do', [UsuarioController::class , 'salvarSaldo'])->name('admin.saldo.do')->middleware('auth');

Route::get('admin/pass', [UsuarioController::class , 'getAlterarSenha'])->name('admin.pass')->middleware('auth');
Route::post('admin/pass/do', [UsuarioController::class , 'salvarSenhaNova'])->name('admin.pass.do')->middleware('auth');

Route::get('admin/cancelar_bilhete/{value}', [BilheteController::class , 'cancelarBilhete'])->name('admin.cancelar_bilhete.do')->middleware('auth');

require __DIR__.'/auth.php';





